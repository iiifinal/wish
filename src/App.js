import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import './App.less';

import routerArr from './router/index'

import PageBox from './component/pageBox/pageBox';



export default function App() {
  return (
    <Router>
      <PageBox>
        <Switch>
          {routerArr.map((item)=><Route key={item.key} path={item.path} component={item.component} />)}
          <Redirect to={"/home"} />
        </Switch>
      </PageBox>
    </Router>
  );
}
