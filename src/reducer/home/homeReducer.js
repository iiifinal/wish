import types from '../../action/types'


const defaultState = {
    account: {},
}

export default function onAction(state = defaultState, action) {
    switch (action.type) {
        case types.ACCOUNT:
            return {
                ...state,
                account: action.account
            }

        default:
            return state

    }

}
