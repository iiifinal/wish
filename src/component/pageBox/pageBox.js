import { useEffect } from "react";
import styles from './pageBox.module.scss'

import { useLocation } from "react-router-dom";
import Header from "../header/header";
import Footer from "../footer/footer";
import router from "../../router";
export default function PageBox({ children }) {


    const pathname = useLocation().pathname;

    let index=router.findIndex(item=>item.path==pathname)

    let result=index>-1?router[index].showFooter:true
   
    
    return <div className={styles.pageBox}>
        <Header/>
        {children && children}
        {result&&<Footer/>}
    </div>
}