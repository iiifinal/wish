import types from '../types'

export function getWalletInfo(payload) {
    return {
        type: types.ACCOUNT,
        account: payload
    }
}
