import { useEffect, useState, useRef, useReducer } from "react"
import { Button, Input, message, Spin } from "antd";
import axios from "axios";
import PlayGameView from '../../component/playGame/playGame'
import styles from './home.module.scss';
import Actions from '../../action/index';
import { connect } from "react-redux";
import { loadStdlib } from '@reach-sh/stdlib';
import { RandomArrayHandle, GetFormatAddress, Ascii2native } from '../../util/baseUtil';
import { RoleArray, EffectCardArray, EquipmentCardArray, LimitCardArray } from '../../util/playCardMock';
import { MapRoleCardInfo, ReadTypeCardInfo } from '../../util/readCardMap';
// import * as Test from '../../rshUtil/test.main.mjs'

import * as BattleRecord from '../../rshUtil/battleRecord.main.mjs'

const { Search, TextArea } = Input;
const stdlib = loadStdlib(process.env);
//代币的缩写
const { standardUnit } = stdlib

//获取地址的代币数量
//who参数是地址对象传入
const getBalanceHandle = async (who) => {
    //balaceOf查询得到是hash余额，然后formatCurrency转换成数字语言
    if (Object.keys(who).length) {
        return stdlib.formatCurrency(await stdlib.balanceOf(who), 4)
    } else {
        return ''
    }
}
//传入reach的随机函数
const random = () => {
    return stdlib.hasRandom.random();
}
//卡组数据（暂时模拟累计18张卡）
const DeckData = [...EffectCardArray, ...EquipmentCardArray, ...LimitCardArray]
DeckData.forEach(k => {
    let card_id = k.type_id + k.word_id + k.level_id + k.id + k.effect_id
    k.card_id = parseFloat(card_id)
})
//转换成整数
const ResetFormatNumber = (num) => {
    let format = stdlib.formatCurrency(num)
    //0.000000000000000001
    return format>1?format:parseInt(format * 1000000000000000000)

}

const tellWhoWin=(sumA,sumA_s,sumB,sumB_s)=>{
    if (sumA == sumB && sumA_s == sumB_s) {
        //进入平局处理(暂时设定是A赢)
        return 'A'
    } else {
        if (sumA_s > sumB_s) {
            //判断是B赢了
            return 'B'
        } else {
            if (sumA_s == sumB_s) {
                if (sumA_s > 0) {
                  return   sumA > sumB ? 'B' : 'A'
                } else {
                    // 都是正
                    return  sumA > sumB ? 'A' : 'B'
                   
                }
            } else {
                return 'A'
            }
        }
    }
}



function Home(props) {
    // Founder创建者 Joiner加入者
    const [role, setRole] = useState('')
    //选择角色处理，先判断有没有登陆
    const setRoleHandle = async (value) => {
        if (Object.keys(props.account).length == 0) {
            const account = await stdlib.getDefaultAccount()
            props.getWalletInfo(account)
        }
        setRole(value)
    }




    useEffect(() => {
        // axios.get('/douyu/api/room/list?page=1&type=yz').then(res => {
        //     console.log(res)
        // }).catch(err => {
        //     console.log(err)
        // })
    }, [])

    return (<div className={styles.home}>
        {!role && <div className={styles.title}><span>{'NFT卡牌对战游戏案例'}</span></div>}

        {!role && <div className={styles.buttonView}>
            <Button style={{
                marginRight: 20
            }} onClick={() => setRoleHandle('Founder')} type={'primary'}>{'创建游戏'}</Button>
            <Button onClick={() => setRoleHandle('Joiner')}>{'加入游戏'}</Button>
        </div>
        }
        {role ? role == 'Founder' ? <FounderView account={props.account}></FounderView> : <JoinerView account={props.account}></JoinerView> : null}

    </div>)
}


export default connect(state => ({ ...state.home }), { ...Actions })(Home);

//初始化数据
const initialState = {
    //回合
    roundCurrent: 0,
    //开组
    deckCard: [],
    //手卡
    handCard: [],
    //效果区域（可盖牌显示效果卡，装备卡）
    hideCard: [],
    //使用历史
    historyCard: [],
    //等待状态
    waitSwitch: false,
    //提示玩家现在是什么样的等待状态
    waitInfo: '',
    //等待状态下，另外一个玩家的出卡情况
    waitUseCard: [],
    //等待状态下，另外一个玩家的预言的真相
    waitSaidTruth: null,
    //非自己回合使用卡数据
    noMyselfReply: [],
    //抽卡动作处理（一回合一次,true已经完成抽卡动作）
    drawActionSwitch: false,
    //我的积分
    myFraction: 0,
    //对方的积分
    otherFraction: 0,
    //倒计数回合
    countDownNum: 0,
    //等待状态下的出卡结果开关
    waitActionSwitch: false,
    //判断是不是自己回合
    isMyself: false
}

//根据动作是执行相应的数据
function RoundAcReducer(state, action) {
    switch (action.type) {
        case 'initial':
            return {
                ...state,
                ...action.value
            };
        case 'start':
            return {
                ...state,
                ...action.value
            };
        case 'draw':
            return {
                ...state,
                ...action.value
            };
        case 'hide':
            return {
                ...state,
                ...action.value
            };
        case 'use':
            return {
                ...state,
                ...action.value
            };
        case 'wait':
            return {
                ...state,
                ...action.value
            };
        case 'end':
            return {
                ...state,
                ...action.value
            };
        default:
            return state
    }
}

//合约发起者的前端视图结构
function FounderView({ account }) {

    const [gameData, dispatch] = useReducer(RoundAcReducer, initialState);
    // const { roundCurrent, deckCard, handCard, hideCard, historyCard, waitSwitch,drawActionSwitch,myFraction,otherFraction,countDownNum} = gameData;

    //设置隐藏盖牌的执行ref
    const setHideRoleRef = useRef(null)
    //抽卡的执行等待
    const drawCardRef = useRef(null)



    //使用卡执行等待
    const useCardRef = useRef(null)
    //对方使用卡反击等待
    const waitUseCardRef = useRef(null)
    //反击状态下判断隐藏卡的数据
    const myHideCardRef = useRef(null)
    //发起合约的相关数据
    const [founderData, setFounderData] = useState({
        //当前测试网络代币数量
        balanceNumber: 0,
        //测试网购买代币视图
        buytokenShow: true,
        //支付视图操作
        payTokenShow: false,
        //合约地址
        contractAddress: '',
        //等待状态
        gameStartStatus: false,
        //开始游戏
        goPlayGameSwitch: false
    })
    //购买的代币数量
    const [buyToken, setBuyToken] = useState('')
    //为合约支付的代币
    const [payToken, setPayToken] = useState('')

    //选择的UUID
    const [selectUUID, setSelectUUID] = useState({})
    //待提交的效果卡，装备卡，限制卡，没有视图显示
    const [currentUseCardArr, setCurrentUseCardArr] = useState([])
    //玩家的预判
    const [currentJudge, setCurrentJudge] = useState(null)
    //查询账户余额
    useEffect(async () => {
        const balanceNumber = await getBalanceHandle(account)
        setFounderData({
            ...founderData,
            balanceNumber,
            // goPlayGameSwitch: true
        })
        // setSelectUUID(RoleArray[0])
        // setInitialHand()
    }, [])


    //购买处理逻辑
    const onBuyTokenHandle = async (value) => {
        if (!value) {
            message.info('创建游戏的代币的数量不能为0')
            return
        }
        //获取当前区块链网络
        const faucet = await stdlib.getFaucet()
        // 模拟购买步骤（这里是测试网络地址转移到自己的钱包，所以直接发起交易）
        await stdlib.transfer(faucet, account, stdlib.parseCurrency(value));
        //交易处理后重新查询账户余额
        const balanceNumber = await getBalanceHandle(account)
        setFounderData({
            ...founderData,
            balanceNumber,
            buytokenShow: false,
            payTokenShow: true,
        })
        message.info(`你成功购买了${value}${standardUnit}`, 1)
    }

    //支付合约处理逻辑
    const payTokenHandle = async (value) => {
        if (!value || founderData.balanceNumber < value) {
            message.info('输入参与游戏的代币数量不能为0或者超过当前拥有的数量', 1)
            return
        }
        //depoloy与后端的类进行链接处理
        const linkAccount = account.deploy(BattleRecord)
        // console.log(linkAccount)
        // //转换成hash交易代币数量
        const wager = stdlib.parseCurrency(value)

        let config = {
            random,
            ...stdlib.hasConsoleLogger,
            wager: wager,
            tellGameStartStatus,
            getHideRoleHandle,
            waitTimeoutHandle,
            payTimeoutHandle,
            setInitialHand,
            tellEndRoundStatus,
            currentDrawCardAction,
            currentUseCardAction,
            waitUseCardAction,
            tellwaitplayerUseCardResult,
            tellWhoWinHandle,
        }
        // console.log(config)
        BattleRecord.Oneself(linkAccount, config)
        // //获取建立后的合约地址
        const newinfo = await linkAccount.getInfo()
        // console.log(newinfo)
        const contractAddress = JSON.stringify(newinfo, null, 2)
        // console.log(contractAddress)
        setFounderData({
            ...founderData,
            contractAddress,
            payTokenShow: false,

        })
        message.info('合约部署成功', 1)

    }




    const tellWhoWinHandle=async(RA,RSA,RB,RSB)=>{
        const _RA=stdlib.formatCurrency(RA)
        const _RSA=stdlib.formatCurrency(RSA)
        const _RB=stdlib.formatCurrency(RB)
        const _RSB=stdlib.formatCurrency(RSB)
        const result=tellWhoWin(_RA,_RSA,_RB,_RSB) 
        if(result=='A'){
            message.success(`对战结束，是你自己获得了胜利！`,3)
        }else{
            message.error(`对战结束，是你对方获得了胜利！`,3)
        }
    }

    //告诉玩家游戏等待状态结束，准备开始
    const tellGameStartStatus = (status) => {
        setFounderData({
            ...founderData,
            gameStartStatus: status,
            payTokenShow: false,
        })
        message.info('等待3秒后游戏开始', 1)
        setTimeout(() => {
            setFounderData({
                ...founderData,
                goPlayGameSwitch: status,
                payTokenShow: false,

            })
        }, 3000)
    }



    const payTimeoutHandle = () => {
        console.log('等待玩家支付过程中超时了')
    }

    const waitTimeoutHandle = () => {
        console.log('游戏超时了')
    }
    //执行洗牌处理
    const setInitialHand = () => {
        const copyDeckData = JSON.parse(JSON.stringify(DeckData))
        let deckCard = RandomArrayHandle(copyDeckData)
        console.log(`我洗牌后得顺序是`, deckCard)
        let handCard = deckCard.splice(0, 5)
        dispatch({ type: 'initial', value: { deckCard, handCard } })
        return stdlib.parseCurrency(1)
    }

    //抽卡处理函数，传递到reach
    const currentDrawCardAction = async () => {
        await new Promise(resolve => {
            drawCardRef.current = resolve
        }).then((data) => {
            // console.log('我进入抽卡同步前端视图了',data)
            let newDeckCard = data.deckCard
            let currentCard = newDeckCard.splice(0, 1)
            let newHandCard = [...data.handCard, currentCard[0]]
            // console.log(newDeckCard)
            // console.log(newHandCard)
            dispatch({
                type: 'draw', value: {
                    deckCard: newDeckCard,
                    handCard: newHandCard,
                    drawActionSwitch: true
                }
            })
        })
    }
    //抽卡处理
    const drawCardHandle = () => {
        console.log('我要执行抽卡处理啦')
        if (!gameData.isMyself) {
            message.info('非自己回合不允许抽卡操作', 1)
            return
        }
        if (gameData.drawActionSwitch) {
            message.info('本回合内你已经抽卡了', 1)
            return
        }
        //注意：传递过去到reach的函数，如果对前端进行视图更新，可通过promise的reslove返回更新的数据，在then更新
        drawCardRef.current && drawCardRef.current(gameData)
    }
    //埋伏卡处理
    const ambushItemHandle = (item) => {
        if (!gameData.isMyself) {
            message.info('非自己回合不允许埋伏卡操作', 1)
            return
        }
        if (!gameData.drawActionSwitch) {
            message.info('正处于抽卡状态，请点击卡组进行抽卡', 1)
            return
        }
        if (currentUseCardArr.length) {
            message.info('已进入使用卡牌阶段,无法进行盖牌处理', 1)
            return
        }
        if (gameData.hideCard.length === 3) {
            message.info('埋伏卡区最多只能放置3张埋伏卡', 1)
            return
        }
        let newhandCard = gameData.handCard.filter(k => k.uuid !== item.uuid)
        let newhideCard = [...gameData.hideCard, item]

        dispatch({
            type: 'hide', value: {
                hideCard: newhideCard,
                handCard: newhandCard,
            }
        })
    }

    //使用卡处理
    const useItemHandle = (item, target, num) => {
        //进入自己回合的处理逻辑
        if (gameData.isMyself) {
            if (!gameData.drawActionSwitch) {
                message.info('正处于抽卡状态，请点击卡组进行抽卡', 1)
                return
            }
            if (currentUseCardArr.length == 3) {
                message.info('每个回合最多使用3张卡', 1)
                return
            }
            if(currentUseCardArr.find(k=>k.type=='limit')){
                message.info('每个回合最多只能使用一张限制卡',1)
                return
            }
            let newCurrentUseCardArr = [...currentUseCardArr, item]
            setCurrentUseCardArr(newCurrentUseCardArr)
            //效果区使用卡牌
            let newhistoryCard = [...gameData.historyCard, item]
            //判断是从哪里区域使用卡的，BattleZoneView是埋伏区域
            if (target == 'BattleZoneView') {
                let newhideCard = gameData.hideCard.filter(k => k.uuid !== item.uuid)
                dispatch({
                    type: 'use', value: {
                        hideCard: newhideCard,
                        historyCard: newhistoryCard
                    }
                })
            } else {
                //否则则是手卡发动
                let newhandCard = gameData.handCard.filter(k => k.uuid !== item.uuid)
                dispatch({
                    type: 'use', value: {
                        handCard: newhandCard,
                        historyCard: newhistoryCard
                    }
                })

            }
        } else {

            if (target == 'BattleZoneView' && gameData.waitActionSwitch) {
                let newhistoryCard = [...gameData.historyCard, item]

                let str = `${gameData.waitUseCard[num].type_id}-${gameData.waitUseCard[num].word_id}-${gameData.waitUseCard[num].level_id}-${gameData.waitUseCard[num].id}`
                let useStr = `${item.type_id}-${item.word_id}-${item.level_id}-${item.id}`
                console.log('判断两个id是', str, useStr)
                if (str !== useStr) {
                    message.info('非同系列对应的的效果卡，不符合条件无法反击', 1)
                    return
                } else {
                    if (item.effect_id == gameData.waitUseCard[num].effect_id) {
                        message.info('相同效果的卡，不符合条件', 1)
                        return
                    } else {
                        let newhideCard = gameData.hideCard.filter(k => k.uuid !== item.uuid)
                        let copyNoMyselfReply = gameData.noMyselfReply
                        item.flagNum = num
                        copyNoMyselfReply[num] = item
                        console.log('准备更新用卡后的前端视图', item)
                        dispatch({
                            type: 'wait', value: {
                                noMyselfReply: copyNoMyselfReply,
                                hideCard: newhideCard,
                                historyCard: newhistoryCard
                            }
                        })
                    }
                }
            }

        }
    }

    //

    //回合结束前端准备提交数据
    const currentUseCardAction = async () => {
        const _updateArr = await new Promise(resolve => {
            useCardRef.current = resolve
        })

        console.log(_updateArr)
        return _updateArr
    }

    //结束本回合确认
    const endThisRound = () => {
        if (!gameData.drawActionSwitch) {
            message.info('正处于抽卡状态，请点击卡组进行抽卡', 1)
            return
        }
        if (!currentJudge) {
            message.info('请选择一个预测真相结果然后结束本回合', 1)
            return
        }

        let flagArr = []
        let arr = [0, 1, 2]
        arr.forEach(k => {
            //判断是是否有卡，并且不是限制卡
            if (currentUseCardArr[k]&&currentUseCardArr[k].type!=='limit') {
                let flagNum = ReadTypeCardInfo(currentUseCardArr[k].type, currentUseCardArr[k].uuid)
                console.log(`对应的卡槽使用数据是`, currentUseCardArr[k].card_id, Math.abs(flagNum), flagNum > 0 ? 0 : 1)
                // flagArr = [...flagArr, stdlib.parseCurrency(currentUseCardArr[k].card_id), stdlib.parseCurrency(Math.abs(flagNum)), flagNum > 0 ? zeroFormat : stdlib.parseCurrency(1)]
                flagArr = [...flagArr, currentUseCardArr[k].card_id, Math.abs(flagNum), flagNum > 0 ? 0 : 1]

            } else {
                flagArr = [...flagArr, 0, 0, 0]

            }
        })
        //判断是否有限制卡
        let limitItem=currentUseCardArr.find(k=>k.type=='limit')
        if(limitItem){
            let num=ReadTypeCardInfo(limitItem.type, limitItem.uuid)
            flagArr = [...flagArr, limitItem.card_id, stdlib.parseCurrency(Math.abs(num)), num>0?0:1]
        }else{
            flagArr = [...flagArr, 0, 0, 0]
        }
        // console.log(`我要往reach提交数据了`);
        const updateArr = [...flagArr, stdlib.parseCurrency(currentJudge)]

        console.log(updateArr)
        //提交完后清空这次候选的提交数据
        setCurrentUseCardArr([])
        useCardRef.current && useCardRef.current(updateArr)
        dispatch({
            type: 'end', value: {
                waitSwitch: true,
                waitInfo: '正在等待对方的反击操作'
            }
        })
        message.info('你的操作步骤已结束，等待对方回应本次操作结果', 1)
        //更新隐藏区的数据
        myHideCardRef.current = gameData.hideCard

    }

    const tellwaitplayerUseCardResult = (CU1, CU2, CU3,limit) => {
        console.log('对方玩家对自己使用卡的操作结果')
        console.log(CU1, CU2, CU3,limit)
        // return ['1',0,'1',0,'1',0,'1',0,1]
    }

    //等待回合，按顺序复制备份对手使用卡
    const copywaitUseCardSortRef = useRef(null)

    // 通知等待中玩家对方使用卡，然后反击操作的结果
    const waitUseCardAction = async (CU1, CU2, CU3,limitA,Truth) => {
        console.log('通知对方我使用卡的处理函数')
        let restArr = [CU1, CU2, CU3,limitA]
        let flagUseArr = []
        restArr.forEach(k => {
            flagUseArr.push(stdlib.formatCurrency(k))
        })
        copywaitUseCardSortRef.current = flagUseArr
        let formatWaitUseCard = []

        flagUseArr.forEach(item => {
            let flagItem = DeckData.find(k => k.card_id == item)
            if (flagItem) {
                formatWaitUseCard.push(flagItem)
            }
        })


        console.log(`我找到映射数组`, formatWaitUseCard, myHideCardRef.current)
        let zero = stdlib.parseCurrency(0)
        //判断到对方有出卡，自己也有埋伏卡
        if (formatWaitUseCard.length && myHideCardRef.current && myHideCardRef.current.length) {
            let noMyselfReply = []
            //预置空白的对应卡槽
            formatWaitUseCard.forEach(k => {
                noMyselfReply.push({})
            })

            dispatch({
                type: 'wait', value: {
                    waitSwitch: false,
                    waitActionSwitch: true,
                    waitSaidTruth: stdlib.formatCurrency(Truth),
                    waitUseCard: formatWaitUseCard,
                    noMyselfReply,
                }
            })
            console.log('前端打开反击的操作了')

            const ReplyArr = await new Promise(resolve => {
                waitUseCardRef.current = resolve
            }).then(arr => {
                console.log('我在promise看到的反击数组是', arr)
                dispatch({
                    type: 'wait', value: {
                        waitSwitch: true,
                        waitActionSwitch: false,
                        waitInfo: '你已经回应了对方的效果卡操作，正在等待共识网络数据同步中..'
                    }
                })
                return arr
            })

            console.log('反击卡最终效果是', ReplyArr)

            return ReplyArr
        } else {
            return [0, 0, 0, 0, 0, 0, 0, 0, 0,0, 0, 0]
            // return [zero, zero, zero, zero, zero, zero, zero, zero, zero]
        }
    }





    //等待状态下，确认上传反击卡的数据处理
    const confirmUpNoReplyArr = () => {
        let flagArr = []
   
        if (copywaitUseCardSortRef.current) {
            //这里 copywaitUseCardSortRef.current保存的是用卡的cardid
            copywaitUseCardSortRef.current.forEach((k, i) => {
                if (k > 0) {
                    if (gameData.noMyselfReply.length) {
                        //判断卡槽位置
                        let flag = gameData.noMyselfReply.find(v => v.flagNum == i)
                        if (flag&&flag.type!=='limit') {
                            let activeNum = ReadTypeCardInfo(flag.type, flag.uuid)
                            flagArr = [...flagArr, flag.card_id, Math.abs(activeNum), activeNum > 0 ? 0 : 1]
                        } else {
                            flagArr = [...flagArr, 0, 0, 0]
                        }
                    } else {
                        flagArr = [...flagArr, 0, 0, 0]
                    }
                } else {
                    console.log(`我进入判断按顺序对应卡的逻辑了，没有卡的逻辑，设0`)
                    // flagArr = [...flagArr, zero, zero, zero]

                    flagArr = [...flagArr, 0, 0, 0]
                }
            })
            let limitItem=copywaitUseCardSortRef.current.find(b=>b.type=='limit')
            if(limitItem){
                let limitNum=ReadTypeCardInfo(limitItem.type, limitItem.uuid)
                flagArr = [...flagArr, limitItem.card_id, Math.abs(limitNum), limitNum > 0 ? 0 : 1]
            }else{
                flagArr = [...flagArr,0, 0, 0]  
            }

        } else {
            // flagArr = [zero, zero, zero, zero, zero, zero, zero, zero, zero]

            flagArr = [0, 0, 0, 0, 0, 0, 0, 0, 0,0, 0, 0]
        }
        console.log('准备确认反击的上传数据', flagArr)
        waitUseCardRef.current && waitUseCardRef.current(flagArr)
    }


    //告诉玩家现在第几回合了
    const tellEndRoundStatus = async (fa, fsa, fb, fsb, round, count) => {
        console.log(fa, fsa, fb, fsb, round, count)
        //    //我的积分
        //    myFraction:0,
        //    //对方的积分
        //    otherFraction:0,
        //    //倒计数回合
        //    countDownNum:0

        await new Promise(resolve => {
            let _fa = ResetFormatNumber(fa)
            let _fsa = ResetFormatNumber(fsa)
            let _fb = ResetFormatNumber(fb)
            let _fsb = ResetFormatNumber(fsb)
            let _round = ResetFormatNumber(round)
            let _count = ResetFormatNumber(count)
            // console.log(JSON.parse(JSON.stringify(gameData)))
            // console.log(xxx.current)
            resolve([_fa, _fsa, _fb, _fsb, _round, _count])

        }).then(data => {
            const [_fa, _fsa, _fb, _fsb, _round, _count] = data
            // console.log(_fa, _fsa, _fb, _fsb, _round, _count)
            console.log(`ab分数是`, _fa, _fsa, _fb, _fsb)
            console.log(`当前回合是${_round}`, `当前倒计时是${_count}`)

            dispatch({
                type: 'start', value: {
                    isMyself: _round % 2 > 0 ? true : false,
                    waitSwitch: _round % 2 > 0 ? false : true,
                    waitInfo: _round % 2 > 0 ? '' : '正处于对手的回合中',
                    roundCurrent: _round,
                    myFraction: _fsa > 0 ? -_fa : _fa,
                    otherFraction: _fsb > 0 ? -_fb : _fb,
                    countDownNum: _count,
                    drawActionSwitch: false,
                }
            })

        })
    }

    //传递reach的前端执行函数，用来获取隐藏角色卡
    const getHideRoleHandle = async () => {
        const hideUUID = await new Promise(resolve => {
            setHideRoleRef.current = resolve
        })
        console.log(`我拿到了A的uuid`, hideUUID)

        return stdlib.parseCurrency(hideUUID)
    }

    //执行等待状态
    const confirmHideCard = (item) => {
        if (!setHideRoleRef.current) {
            message.info('等待共识网络状态中', 1)
            return
        }
        setHideRoleRef.current && setHideRoleRef.current(MapRoleCardInfo(item.uuid))
        setSelectUUID(item)
    }



    //读取操作视图
    const _ReadViewHandle = (status) => {
        if (status) {
            return <PlayGameView
                RoleArray={RoleArray}
                confirmHideCard={confirmHideCard}
                uuid={selectUUID}
                waitSwitch={gameData.waitSwitch}
                waitActionSwitch={gameData.waitActionSwitch}
                waitUseCard={gameData.waitUseCard}
                waitSaidTruth={gameData.waitSaidTruth}
                waitInfo={gameData.waitInfo}
                deckCard={gameData.deckCard}
                handCard={gameData.handCard}
                hideCard={gameData.hideCard}
                historyCard={gameData.historyCard}
                roundCurrent={gameData.roundCurrent}
                myFraction={gameData.myFraction}
                otherFraction={gameData.otherFraction}
                countDownNum={gameData.countDownNum}
                deckCardAction={drawCardHandle}
                ambushCardAction={ambushItemHandle}
                currentUseCardArrAcion={useItemHandle}
                setCurrentJudge={setCurrentJudge}
                currentJudge={currentJudge}
                endThisRound={endThisRound}
                confirmUpNoReplyArr={confirmUpNoReplyArr}

            />
        } else {
            return <div className={styles.FounderViewBox}>
                <InfoView account={account} balance={founderData.balanceNumber} unit={standardUnit} />

                {founderData.buytokenShow && <div className={styles.actionButton}>
                    <Input onChange={(e) => {
                        setBuyToken(e.target.value)
                    }} placeholder={'请输入购买的测试代币数量'} />
                    <div className={styles.buttonCon}>
                        <Button style={{ marginRight: 10 }} onClick={() => onBuyTokenHandle(buyToken)}>{'确定'}</Button>
                        <Button onClick={() => {
                            setFounderData({
                                ...founderData,
                                buytokenShow: false,
                                payTokenShow: true
                            })
                        }}>{'跳过'}</Button>
                    </div>
                </div>}

                {founderData.payTokenShow && <div className={styles.actionButton}>
                    <Input onChange={(e) => {
                        setPayToken(e.target.value)
                    }} placeholder={'请输入要为游戏支付筹码的代币数量'} />
                    <div className={styles.buttonCon}>
                        <Button style={{ marginRight: 10 }} onClick={() => payTokenHandle(payToken)}>{'确定'}</Button>
                    </div>
                </div>}



                {founderData.contractAddress && <div className={styles.contractAddress}>
                    {!founderData.gameStartStatus ? <span className={styles.contractAddressSpan}>{founderData.contractAddress}</span> : null}
                    {!founderData.gameStartStatus ? <Spin tip={'等待其他玩家加入中'} /> : <span>{'对战游戏即将开始'}</span>}
                </div>}

            </div>
        }
    }


    return <div className={styles.FounderView}>

        {_ReadViewHandle(founderData.goPlayGameSwitch)}

    </div>
}

//参与合约游戏的加入者的前端视图结构
function JoinerView({ account }) {

    const [gameData, dispatch] = useReducer(RoundAcReducer, initialState);


    const confirmPayRef = useRef(null)
    //设置隐藏盖牌的执行ref
    const setHideRoleRef = useRef(null)

    //抽卡的执行等待
    const drawCardRef = useRef(null)
    //使用卡执行等待
    const useCardRef = useRef(null)
    //使用卡执行等待
    const waitUseCardRef = useRef(null)

    const myHideCardRef = useRef(null)

    //支付相关的数据
    const [JoinerData, setJoinerData] = useState({
        //当前测试网络代币数量
        balanceNumber: 0,
        //参与合约游戏的JSON
        contractAddress: '',
        //输入合约的前端视图开关
        inputContractShow: false,
        //购买测试代币的视图
        buytokenShow: true,
        //同意合约需要支付的代币视图开关
        confirmTokenShow: false,
        //确认支付代币的数量
        confirmToken: 0,
        //游戏等待前端视图开关
        openGameStartView: false,
        //等待状态
        gameStartStatus: false,
        //游戏开始
        goPlayGameSwitch: false

    })
    //购买的代币数量
    const [buyToken, setBuyToken] = useState('')


    //选择的UUID
    const [selectUUID, setSelectUUID] = useState({})
    //待提交的效果卡，装备卡，限制卡，没有视图显示
    const [currentUseCardArr, setCurrentUseCardArr] = useState([])
    //玩家的预判
    const [currentJudge, setCurrentJudge] = useState(null)

    //查询账户余额
    useEffect(async () => {
        const balanceNumber = await getBalanceHandle(account)
        setJoinerData({
            ...JoinerData,
            balanceNumber,
            // goPlayGameSwitch: true
        })
        // setSelectUUID(RoleArray[0])
        // setInitialHand()
    }, [])

    //购买处理逻辑
    const onBuyTokenHandle = async (value) => {
        if (!value) {
            message.info('创建游戏的代币的数量不能为0')
            return
        }
        //获取当前区块链网络
        const faucet = await stdlib.getFaucet()
        // 模拟购买步骤（这里是测试网络地址转移到自己的钱包，所以直接发起交易）
        await stdlib.transfer(faucet, account, stdlib.parseCurrency(value));
        //交易处理后重新查询账户余额
        const balanceNumber = await getBalanceHandle(account)
        setJoinerData({
            ...JoinerData,
            balanceNumber,
            buytokenShow: false,
            inputContractShow: true,
        })
        message.info(`你成功购买了${value}${standardUnit}`, 1)
    }

    //链接合约，并将相关的执行函数传过去给合约执行
    const linkContractHandle = (str) => {
        if (!str) {
            message.info('输入的合约不能为空', 1)
            return
        }
        const newAccount = account.attach(BattleRecord, JSON.parse(str));
        let config = {
            random,
            ...stdlib.hasConsoleLogger,
            getTokenNumberHandle,
            tellGameStartStatus,
            getHideRoleHandle,
            waitTimeoutHandle,
            payTimeoutHandle,
            setInitialHand,
            tellEndRoundStatus,
            currentDrawCardAction,
            currentUseCardAction,
            waitUseCardAction,
            tellwaitplayerUseCardResult,
            tellWhoWinHandle,
        }
        BattleRecord.Opponent(newAccount, config)
    }


   
    const tellWhoWinHandle=async(RA,RSA,RB,RSB)=>{
        const _RA=stdlib.formatCurrency(RA)
        const _RSA=stdlib.formatCurrency(RSA)
        const _RB=stdlib.formatCurrency(RB)
        const _RSB=stdlib.formatCurrency(RSB)
        const result=tellWhoWin(_RA,_RSA,_RB,_RSB) 
        if(result=='B'){
            message.success(`对战结束，是你自己获得了胜利！`,3)
        }else{
            message.error(`对战结束，是对方获得了胜利！`,3)
        }
    }

    //获取合约需要支付的代币数量，并对支付状态进行处理
    const getTokenNumberHandle = async (wagerUInt) => {
        const wager = stdlib.formatCurrency(wagerUInt)
        await new Promise(resolve => {
            //打开支付前端页面
            setJoinerData({
                ...JoinerData,
                confirmToken: wager,
                inputContractShow: false,
                confirmTokenShow: true,
                inputContractShow: false
            })
            confirmPayRef.current = resolve
        }).then(() => {
            setJoinerData({
                ...JoinerData,
                confirmTokenShow: false,
                openGameStartView: true,
                inputContractShow: false,
            })
        })
    }

    //执行确认支付
    const confirmPayTokenHandle = () => {
        confirmPayRef.current && confirmPayRef.current()
    }

    //告诉玩家游戏等待状态结束，准备开始
    const tellGameStartStatus = (status) => {
        setJoinerData({
            ...JoinerData,
            gameStartStatus: status,
            inputContractShow: false,
        })
        message.info('等待3秒后游戏开始', 1)
        setTimeout(() => {
            setJoinerData({
                ...JoinerData,
                goPlayGameSwitch: status,
                inputContractShow: false,
            })
        }, 3000)
    }





    const payTimeoutHandle = () => {
        console.log('等待玩家支付过程中超时了')
    }

    const waitTimeoutHandle = () => {
        console.log('游戏超时了')
    }
    //执行洗牌处理
    const setInitialHand = () => {
        const copyDeckData = JSON.parse(JSON.stringify(DeckData))
        let deckCard = RandomArrayHandle(copyDeckData)
        console.log(`我洗牌后得顺序是`, deckCard)
        let handCard = deckCard.splice(0, 5)
        dispatch({ type: 'initial', value: { deckCard, handCard } })
        return stdlib.parseCurrency(1)

    }

    //抽卡处理函数，传递到reach
    const currentDrawCardAction = async () => {
        await new Promise(resolve => {
            drawCardRef.current = resolve
        }).then((data) => {
            // console.log('我进入抽卡同步前端视图了',data)
            let newDeckCard = data.deckCard
            let currentCard = newDeckCard.splice(0, 1)
            let newHandCard = [...data.handCard, currentCard[0]]
            // console.log(newDeckCard)
            // console.log(newHandCard)
            dispatch({
                type: 'draw', value: {
                    deckCard: newDeckCard,
                    handCard: newHandCard,
                    drawActionSwitch: true
                }
            })
        })
    }
    //抽卡处理
    const drawCardHandle = () => {
        console.log('我要执行抽卡处理啦')
        if (!gameData.isMyself) {
            message.info('非自己回合不允许抽卡操作', 1)
            return
        }
        if (gameData.drawActionSwitch) {
            message.info('本回合内你已经抽卡了', 1)
            return
        }
        //注意：传递过去到reach的函数，如果对前端进行视图更新，可通过promise的reslove返回更新的数据，在then更新
        drawCardRef.current && drawCardRef.current(gameData)
    }
    //埋伏卡处理
    const ambushItemHandle = (item) => {
        if (!gameData.isMyself) {
            message.info('非自己回合不允许埋伏卡操作', 1)
            return
        }
        if (!gameData.drawActionSwitch) {
            message.info('正处于抽卡状态，请点击卡组进行抽卡', 1)
            return
        }

        if (currentUseCardArr.length) {
            message.info('已进入使用卡牌阶段,无法进行盖牌处理', 1)
            return
        }

        if (gameData.hideCard.length === 3) {
            message.info('埋伏卡区最多只能放置3张埋伏卡', 1)
            return
        }
        let newhandCard = gameData.handCard.filter(k => k.uuid !== item.uuid)
        let newhideCard = [...gameData.hideCard, item]

        dispatch({
            type: 'hide', value: {
                hideCard: newhideCard,
                handCard: newhandCard,
            }
        })
    }

    //使用卡处理
    const useItemHandle = (item, target, num) => {
        //进入自己回合的处理逻辑
        if (gameData.isMyself) {
            if (!gameData.drawActionSwitch) {
                message.info('正处于抽卡状态，请点击卡组进行抽卡', 1)
                return
            }
            if (currentUseCardArr.length == 3) {
                message.info('每个回合最多使用3张卡', 1)
                return
            }
            if (currentUseCardArr.find(k=>k.type=='limit')) {
                message.info('每个回合最多只能使用1张限制卡', 1)
                return
            }

            let newCurrentUseCardArr = [...currentUseCardArr, item]
            setCurrentUseCardArr(newCurrentUseCardArr)
            //效果区使用卡牌
            let newhistoryCard = [...gameData.historyCard, item]
            //判断是从哪里区域使用卡的，BattleZoneView是埋伏区域
            if (target == 'BattleZoneView') {
                let newhideCard = gameData.hideCard.filter(k => k.uuid !== item.uuid)
                dispatch({
                    type: 'use', value: {
                        hideCard: newhideCard,
                        historyCard: newhistoryCard
                    }
                })
            } else {
                //否则则是手卡发动
                let newhandCard = gameData.handCard.filter(k => k.uuid !== item.uuid)
                dispatch({
                    type: 'use', value: {
                        handCard: newhandCard,
                        historyCard: newhistoryCard
                    }
                })

            }

        } else {

            if (target == 'BattleZoneView' && gameData.waitActionSwitch) {
                let newhistoryCard = [...gameData.historyCard, item]


                let str = `${gameData.waitUseCard[num].type_id}-${gameData.waitUseCard[num].word_id}-${gameData.waitUseCard[num].level_id}-${gameData.waitUseCard[num].id}`
                let useStr = `${item.type_id}-${item.word_id}-${item.level_id}-${item.id}`
                console.log('判断两个id是', str, useStr)
                if (str !== useStr) {
                    message.info('非同系列对应的的效果卡，不符合条件无法反击', 1)
                    return
                } else {
                    if (item.effect_id == gameData.waitUseCard[num].effect_id) {
                        message.info('相同效果的卡，不符合条件', 1)
                        return
                    } else {
                        let newhideCard = gameData.hideCard.filter(k => k.uuid !== item.uuid)
                        let copyNoMyselfReply = gameData.noMyselfReply
                        item.flagNum = num
                        copyNoMyselfReply[num] = item
                        console.log('准备更新用卡后的前端视图', item)
                        dispatch({
                            type: 'wait', value: {
                                noMyselfReply: copyNoMyselfReply,
                                hideCard: newhideCard,
                                historyCard: newhistoryCard
                            }
                        })
                    }
                }
            }

        }
    }

    //

    //回合结束前端准备提交数据
    const currentUseCardAction = async () => {
        const _updateArr = await new Promise(resolve => {
            useCardRef.current = resolve
        })

        console.log(_updateArr)
        return _updateArr
    }

    //结束本回合确认
    const endThisRound = () => {
        if (!gameData.drawActionSwitch) {
            message.info('正处于抽卡状态，请点击卡组进行抽卡', 1)
            return
        }

        if (!currentJudge) {
            message.info('请选择一个预测真相结果然后结束本回合', 1)
            return
        }

        let flagArr = []
        let arr = [0, 1, 2]
        arr.forEach(k => {
            //判断是是否有卡，并且不是限制卡
            if (currentUseCardArr[k]&&currentUseCardArr[k].type!=='limit') {
                let flagNum = ReadTypeCardInfo(currentUseCardArr[k].type, currentUseCardArr[k].uuid)
                console.log(`对应的卡槽使用数据是`, currentUseCardArr[k].card_id, Math.abs(flagNum), flagNum > 0 ? 0 : 1)
                // flagArr = [...flagArr, stdlib.parseCurrency(currentUseCardArr[k].card_id), stdlib.parseCurrency(Math.abs(flagNum)), flagNum > 0 ? zeroFormat : stdlib.parseCurrency(1)]
                flagArr = [...flagArr, currentUseCardArr[k].card_id, Math.abs(flagNum), flagNum > 0 ? 0 : 1]

            } else {
                flagArr = [...flagArr, 0, 0, 0]

            }
        })
        //判断是否有限制卡
        let limitItem=currentUseCardArr.find(k=>k.type=='limit')
        if(limitItem){
            let num=ReadTypeCardInfo(limitItem.type, limitItem.uuid)
            flagArr = [...flagArr, limitItem.card_id, stdlib.parseCurrency(Math.abs(num)), num>0?0:1]
        }else{
            flagArr = [...flagArr, 0, 0, 0]
        }
        // console.log(`我要往reach提交数据了`);
        const updateArr = [...flagArr, stdlib.parseCurrency(currentJudge)]
        setCurrentUseCardArr([])
        useCardRef.current && useCardRef.current(updateArr)
        dispatch({
            type: 'end', value: {
                waitSwitch: true,
                waitInfo: '正在等待对方的反击操作'
            }
        })
        message.info('你的操作步骤已结束，等待对方回应本次操作结果', 1)

        myHideCardRef.current = gameData.hideCard
    }

    const tellwaitplayerUseCardResult = (CU1, CU2, CU3) => {
        console.log('对方玩家对自己使用卡的操作结果')
        console.log(CU1, CU2, CU3)
        // return ['1',0,'1',0,'1',0,'1',0,1]
    }

   //等待回合，按顺序复制备份对手使用卡
   const copywaitUseCardSortRef = useRef(null)

   // 通知等待中玩家对方使用卡，然后反击操作的结果
   const waitUseCardAction = async (CU1, CU2, CU3,limitA,Truth) => {
       console.log('通知对方我使用卡的处理函数')
       let restArr = [CU1, CU2, CU3,limitA]
       let flagUseArr = []
       restArr.forEach(k => {
           flagUseArr.push(stdlib.formatCurrency(k))
       })
       copywaitUseCardSortRef.current = flagUseArr
       let formatWaitUseCard = []

       flagUseArr.forEach(item => {
           let flagItem = DeckData.find(k => k.card_id == item)
           if (flagItem) {
               formatWaitUseCard.push(flagItem)
           }
       })


       console.log(`我找到映射数组`, formatWaitUseCard, myHideCardRef.current)
       let zero = stdlib.parseCurrency(0)
       //判断到对方有出卡，自己也有埋伏卡
       if (formatWaitUseCard.length && myHideCardRef.current && myHideCardRef.current.length) {
           let noMyselfReply = []
           //预置空白的对应卡槽
           formatWaitUseCard.forEach(k => {
               noMyselfReply.push({})
           })

           dispatch({
               type: 'wait', value: {
                   waitSwitch: false,
                   waitActionSwitch: true,
                   waitSaidTruth: stdlib.formatCurrency(Truth),
                   waitUseCard: formatWaitUseCard,
                   noMyselfReply,
               }
           })
           console.log('前端打开反击的操作了')

           const ReplyArr = await new Promise(resolve => {
               waitUseCardRef.current = resolve
           }).then(arr => {
               console.log('我在promise看到的反击数组是', arr)
               dispatch({
                   type: 'wait', value: {
                       waitSwitch: true,
                       waitActionSwitch: false,
                       waitInfo: '你已经回应了对方的效果卡操作，正在等待共识网络数据同步中..'
                   }
               })
               return arr
           })

           console.log('反击卡最终效果是', ReplyArr)

           return ReplyArr
       } else {
           return [0, 0, 0, 0, 0, 0, 0, 0, 0,0, 0, 0]
           // return [zero, zero, zero, zero, zero, zero, zero, zero, zero]
       }
   }





   //等待状态下，确认上传反击卡的数据处理
   const confirmUpNoReplyArr = () => {
       let flagArr = []
  
       if (copywaitUseCardSortRef.current) {
           //这里 copywaitUseCardSortRef.current保存的是用卡的cardid
           copywaitUseCardSortRef.current.forEach((k, i) => {
               if (k > 0) {
                   if (gameData.noMyselfReply.length) {
                       //判断卡槽位置
                       let flag = gameData.noMyselfReply.find(v => v.flagNum == i)
                       if (flag&&flag.type!=='limit') {
                           let activeNum = ReadTypeCardInfo(flag.type, flag.uuid)
                           flagArr = [...flagArr, flag.card_id, Math.abs(activeNum), activeNum > 0 ? 0 : 1]
                       } else {
                           flagArr = [...flagArr, 0, 0, 0]
                       }
                   } else {
                       flagArr = [...flagArr, 0, 0, 0]
                   }
               } else {
                   console.log(`我进入判断按顺序对应卡的逻辑了，没有卡的逻辑，设0`)
                   // flagArr = [...flagArr, zero, zero, zero]

                   flagArr = [...flagArr, 0, 0, 0]
               }
           })
           let limitItem=copywaitUseCardSortRef.current.find(b=>b.type=='limit')
           if(limitItem){
               let limitNum=ReadTypeCardInfo(limitItem.type, limitItem.uuid)
               flagArr = [...flagArr, limitItem.card_id, Math.abs(limitNum), limitNum > 0 ? 0 : 1]
           }else{
               flagArr = [...flagArr,0, 0, 0]  
           }

       } else {
           // flagArr = [zero, zero, zero, zero, zero, zero, zero, zero, zero]

           flagArr = [0, 0, 0, 0, 0, 0, 0, 0, 0,0, 0, 0]
       }
       console.log('准备确认反击的上传数据', flagArr)
       waitUseCardRef.current && waitUseCardRef.current(flagArr)
   }



    //告诉玩家现在第几回合了
    const tellEndRoundStatus = async (fa, fsa, fb, fsb, round, count) => {
        console.log(fa, fsa, fb, fsb, round, count)

        await new Promise(resolve => {
            let _fa = ResetFormatNumber(fa)
            let _fsa = ResetFormatNumber(fsa)
            let _fb = ResetFormatNumber(fb)
            let _fsb = ResetFormatNumber(fsb)
            let _round = ResetFormatNumber(round)
            let _count = ResetFormatNumber(count)
            // console.log(JSON.parse(JSON.stringify(gameData)))
            // console.log(xxx.current)
            resolve([_fa, _fsa, _fb, _fsb, _round, _count])

        }).then(data => {
            const [_fa, _fsa, _fb, _fsb, _round, _count] = data
            // console.log(_fa, _fsa, _fb, _fsb, _round, _count)
            console.log(`ab分数是`, _fa, _fsa, _fb, _fsb)
            console.log(`当前回合是${_round}`, `当前倒计时是${_count}`)

            dispatch({
                type: 'start', value: {
                    isMyself: _round % 2 > 0 ? false : true,
                    waitSwitch: _round % 2 > 0 ? true : false,
                    waitInfo: _round % 2 > 0 ? '正处于对手的回合中' : '',
                    roundCurrent: _round,
                    otherFraction: _fsa > 0 ? -_fa : _fa,
                    myFraction: _fsb > 0 ? -_fb : _fb,
                    countDownNum: _count,
                    drawActionSwitch: false,
                }
            })

        })
    }


    //传递reach的前端执行函数，用来获取隐藏角色卡
    const getHideRoleHandle = async () => {
        const hideUUID = await new Promise(resolve => {
            setHideRoleRef.current = resolve
        })
        console.log(`我拿到了A的uuid`, hideUUID)
        return stdlib.parseCurrency(hideUUID)
    }

    //执行等待状态
    const confirmHideCard = (item) => {
        if (!setHideRoleRef.current) {
            message.info('等待共识网络状态中', 1)
            return
        }
        setHideRoleRef.current && setHideRoleRef.current(MapRoleCardInfo(item.uuid))
        setSelectUUID(item)
    }

    //读取操作视图
    const _ReadViewHandle = (status) => {
        if (status) {
            return <PlayGameView
                RoleArray={RoleArray}
                confirmHideCard={confirmHideCard}
                uuid={selectUUID}
                waitSwitch={gameData.waitSwitch}
                waitActionSwitch={gameData.waitActionSwitch}
                waitUseCard={gameData.waitUseCard}
                waitSaidTruth={gameData.waitSaidTruth}
                waitInfo={gameData.waitInfo}
                deckCard={gameData.deckCard}
                handCard={gameData.handCard}
                hideCard={gameData.hideCard}
                historyCard={gameData.historyCard}
                roundCurrent={gameData.roundCurrent}
                myFraction={gameData.myFraction}
                otherFraction={gameData.otherFraction}
                countDownNum={gameData.countDownNum}
                deckCardAction={drawCardHandle}
                ambushCardAction={ambushItemHandle}
                currentUseCardArrAcion={useItemHandle}
                setCurrentJudge={setCurrentJudge}
                currentJudge={currentJudge}
                endThisRound={endThisRound}
                confirmUpNoReplyArr={confirmUpNoReplyArr}

            />
        } else {
            return <div className={styles.JoinerViewBox}>
                <InfoView account={account} balance={JoinerData.balanceNumber} unit={standardUnit} />


                {JoinerData.buytokenShow && <div className={styles.actionButton}>
                    <Input onChange={(e) => {
                        setBuyToken(e.target.value)
                    }} placeholder={'请输入购买的测试代币数量'} />
                    <div className={styles.buttonCon}>
                        <Button type={'primary'} style={{ marginRight: 10 }} onClick={() => onBuyTokenHandle(buyToken)}>{'确定'}</Button>
                        <Button onClick={() => {
                            setJoinerData({
                                ...JoinerData,
                                buytokenShow: false,
                                inputContractShow: true
                            })
                        }}>{'跳过'}</Button>
                    </div>
                </div>}

                {JoinerData.inputContractShow && <div className={styles.inputContract}>
                    <TextArea
                        value={JoinerData.contractAddress}
                        onChange={(e) => {
                            setJoinerData({
                                ...JoinerData,
                                contractAddress: e.target.value,
                            })
                        }}
                        onPressEnter={(e) => {
                            linkContractHandle(e.target.value)
                        }}
                        autoSize={{ minRows: 7, maxRows: 20 }}
                        placeholder={'请输入合约的地址JSON数据格式'}

                    />
                    <div className={styles.inputContractButton}>
                        <Button style={{ marginRight: 10 }} onClick={() => {
                            setJoinerData({
                                ...JoinerData,
                                contractAddress: '',
                            })
                        }}>{'清空'}</Button>
                        <Button type={'primary'} onClick={() => {
                            linkContractHandle(JoinerData.contractAddress)
                        }}>{'确定'}</Button>
                    </div>
                </div>}



                {JoinerData.confirmTokenShow && <div className={styles.actionButton}>
                    <span>{`你需要支付${JoinerData.confirmToken}个${standardUnit}参与这个合约游戏，同意请点击确认！`}</span>
                    <div className={styles.buttonCon}>
                        <Button style={{ marginRight: 10 }} onClick={() => {
                            setJoinerData({
                                ...JoinerData,
                                buytokenShow: false,
                                inputContractShow: true
                            })
                        }}>{'退出'}</Button>
                        <Button type={'primary'} onClick={() => confirmPayTokenHandle(JoinerData.confirmToken)}>{'确认'}</Button>

                    </div>
                </div>}
                {JoinerData.openGameStartView ? JoinerData.gameStartStatus ? <div className={styles.gameStart}><span>{'游戏即将开始'}</span></div> : <div className={styles.gameStart}><Spin tip={'等待确认支付状态'} /></div> : null}
            </div>
        }
    }

    return <div className={styles.JoinerView}>
        {_ReadViewHandle(JoinerData.goPlayGameSwitch)}
    </div>
}


function InfoView({ account, balance, unit }) {
    return <div className={styles.accountInfo}>
        <span className={styles.promptInfo}>{`${GetFormatAddress(account)} 的账号余额 : ${balance} ${unit}`}</span>
    </div>
}