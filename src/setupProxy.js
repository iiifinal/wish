const { createProxyMiddleware } = require('http-proxy-middleware')


module.exports = function (app) {
    app.use(createProxyMiddleware('/douyu', {
        target: 'https://m.douyu.com',
        changeOrigin: true,
        secure: true,
        ws: true, // proxy websockets
        pathRewrite:{
            '^/douyu':''
        }
    }));
};