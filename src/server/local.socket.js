const app = require('express')();
// const cors = require('cors')
// const bodyParser = require('body-parser')

// app.use(bodyParser.json());// 添加json解析
// app.use(bodyParser.urlencoded({ extended: false }))
// app.use(cors())

const server = require('http').createServer(app);
const io = require('socket.io')(server,{
    cors:{
        origin:'*',
    }
});

io.listen(process.env.PORT || 9004, process.env.IP, () => {
    console.log('listening on port 9004!')
})

io.on('connection', socket =>{
    console.log('connection made successfully')
    socket.on('message',payload => {
        console.log('Message received on server: ', payload)
        io.emit('message',payload)
    })
})


