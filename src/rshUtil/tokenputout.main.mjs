// Automatically generated with Reach 0.1.2
/* eslint-disable */
export const _version = '0.1.2';


export function getExports(s) {
  const stdlib = s.reachStdlib;
  return {
    };
  };

export function _getViews(s, viewlib) {
  const stdlib = s.reachStdlib;
  
  return {
    infos: {
      },
    views: {
      }
    };
  
  };

export function _getMaps(s) {
  const stdlib = s.reachStdlib;
  const ctc0 = stdlib.T_Tuple([]);
  return {
    mapDataTy: ctc0
    };
  };

export async function Airdropper(ctc, interact) {
  if (ctc.sendrecv === undefined) {
    return Promise.reject(new Error(`The backend for Airdropper expects to receive a contract as its first argument.`));}
  if (typeof(interact) !== 'object') {
    return Promise.reject(new Error(`The backend for Airdropper expects to receive an interact object as its second argument.`));}
  const stdlib = ctc.stdlib;
  const ctc0 = stdlib.T_UInt;
  const ctc1 = stdlib.T_Tuple([ctc0]);
  const ctc2 = stdlib.T_Digest;
  const ctc3 = stdlib.T_Tuple([ctc0, ctc2, ctc0, ctc0]);
  const ctc4 = stdlib.T_Tuple([ctc0, ctc2, ctc0]);
  const ctc5 = stdlib.T_Tuple([ctc0, ctc0]);
  
  
  const v20 = await ctc.creationTime();
  const v18 = stdlib.protect(ctc0, interact.amt, 'for Airdropper\'s interact field amt');
  const v19 = stdlib.protect(ctc0, interact.pass, 'for Airdropper\'s interact field pass');
  
  const v24 = stdlib.digest(ctc1, [v19]);
  
  const txn1 = await (ctc.sendrecv(1, 2, stdlib.checkedBigNumberify('./src/rsh/tokenputout.rsh:62:20:dot', stdlib.UInt_max, 0), [ctc0, ctc2, ctc0], [v20, v24, v18], [v18, []], [ctc2, ctc0], true, true, false, (async (txn1) => {
    const sim_r = { txns: [], mapRefs: [], mapsPrev: [], mapsNext: [] };
    
    sim_r.prevSt = stdlib.digest(ctc5, [stdlib.checkedBigNumberify('./src/rsh/tokenputout.rsh:62:20:dot', stdlib.UInt_max, 0), v20]);
    sim_r.prevSt_noPrevTime = stdlib.digest(ctc1, [stdlib.checkedBigNumberify('./src/rsh/tokenputout.rsh:62:20:dot', stdlib.UInt_max, 0)]);
    const [v26, v27] = txn1.data;
    const v30 = txn1.time;
    const v25 = txn1.from;
    
    sim_r.txns.push({
      amt: v27,
      kind: 'to',
      tok: undefined
      });
    sim_r.nextSt = stdlib.digest(ctc3, [stdlib.checkedBigNumberify('./src/rsh/tokenputout.rsh:after expr stmt', stdlib.UInt_max, 1), v26, v27, v30]);
    sim_r.nextSt_noTime = stdlib.digest(ctc4, [stdlib.checkedBigNumberify('./src/rsh/tokenputout.rsh:after expr stmt', stdlib.UInt_max, 1), v26, v27]);
    sim_r.view = [ctc1, [stdlib.checkedBigNumberify('./src/rsh/tokenputout.rsh:after expr stmt', stdlib.UInt_max, 0)]];
    sim_r.isHalt = false;
    
    return sim_r;
    })));
  const [v26, v27] = txn1.data;
  const v30 = txn1.time;
  const v25 = txn1.from;
  ;
  const txn2 = await (ctc.recv(2, 1, [ctc0], false, false));
  const [v38] = txn2.data;
  const v40 = txn2.time;
  const v37 = txn2.from;
  ;
  const v41 = stdlib.digest(ctc1, [v38]);
  const v42 = stdlib.digestEq(v26, v41);
  stdlib.assert(v42, {
    at: './src/rsh/tokenputout.rsh:70:16:application',
    fs: [],
    msg: null,
    who: 'Airdropper'
    });
  ;
  return;
  
  
  };
export async function Recipient(ctc, interact) {
  if (ctc.sendrecv === undefined) {
    return Promise.reject(new Error(`The backend for Recipient expects to receive a contract as its first argument.`));}
  if (typeof(interact) !== 'object') {
    return Promise.reject(new Error(`The backend for Recipient expects to receive an interact object as its second argument.`));}
  const stdlib = ctc.stdlib;
  const ctc0 = stdlib.T_Digest;
  const ctc1 = stdlib.T_UInt;
  const ctc2 = stdlib.T_Tuple([ctc1]);
  const ctc3 = stdlib.T_Tuple([]);
  const ctc4 = stdlib.T_Tuple([ctc1, ctc0, ctc1, ctc1]);
  const ctc5 = stdlib.T_Tuple([ctc1, ctc0, ctc1]);
  
  
  const v20 = await ctc.creationTime();
  const txn1 = await (ctc.recv(1, 2, [ctc0, ctc1], false, false));
  const [v26, v27] = txn1.data;
  const v30 = txn1.time;
  const v25 = txn1.from;
  ;
  const v34 = stdlib.protect(ctc1, await interact.getPass(), {
    at: './src/rsh/tokenputout.rsh:66:53:application',
    fs: ['at ./src/rsh/tokenputout.rsh:65:23:application call to [unknown function] (defined at: ./src/rsh/tokenputout.rsh:65:27:function exp)'],
    msg: 'getPass',
    who: 'Recipient'
    });
  const v35 = stdlib.digest(ctc2, [v34]);
  const v36 = stdlib.digestEq(v26, v35);
  stdlib.assert(v36, {
    at: './src/rsh/tokenputout.rsh:67:19:application',
    fs: ['at ./src/rsh/tokenputout.rsh:65:23:application call to [unknown function] (defined at: ./src/rsh/tokenputout.rsh:65:27:function exp)'],
    msg: null,
    who: 'Recipient'
    });
  
  const txn2 = await (ctc.sendrecv(2, 1, stdlib.checkedBigNumberify('./src/rsh/tokenputout.rsh:69:19:dot', stdlib.UInt_max, 2), [ctc0, ctc1, ctc1, ctc1], [v26, v27, v30, v34], [stdlib.checkedBigNumberify('./src/rsh/tokenputout.rsh:decimal', stdlib.UInt_max, 0), []], [ctc1], true, true, false, (async (txn2) => {
    const sim_r = { txns: [], mapRefs: [], mapsPrev: [], mapsNext: [] };
    
    sim_r.prevSt = stdlib.digest(ctc4, [stdlib.checkedBigNumberify('./src/rsh/tokenputout.rsh:69:19:dot', stdlib.UInt_max, 1), v26, v27, v30]);
    sim_r.prevSt_noPrevTime = stdlib.digest(ctc5, [stdlib.checkedBigNumberify('./src/rsh/tokenputout.rsh:69:19:dot', stdlib.UInt_max, 1), v26, v27]);
    const [v38] = txn2.data;
    const v40 = txn2.time;
    const v37 = txn2.from;
    
    sim_r.txns.push({
      amt: stdlib.checkedBigNumberify('./src/rsh/tokenputout.rsh:decimal', stdlib.UInt_max, 0),
      kind: 'to',
      tok: undefined
      });
    const v41 = stdlib.digest(ctc2, [v38]);
    const v42 = stdlib.digestEq(v26, v41);
    stdlib.assert(v42, {
      at: './src/rsh/tokenputout.rsh:70:16:application',
      fs: [],
      msg: null,
      who: 'Recipient'
      });
    sim_r.txns.push({
      amt: v27,
      kind: 'from',
      to: v37,
      tok: undefined
      });
    sim_r.txns.push({
      kind: 'halt',
      tok: undefined
      })
    sim_r.nextSt = stdlib.digest(ctc3, []);
    sim_r.nextSt_noTime = stdlib.digest(ctc3, []);
    sim_r.view = [ctc3, []];
    sim_r.isHalt = true;
    
    return sim_r;
    })));
  const [v38] = txn2.data;
  const v40 = txn2.time;
  const v37 = txn2.from;
  ;
  const v41 = stdlib.digest(ctc2, [v38]);
  const v42 = stdlib.digestEq(v26, v41);
  stdlib.assert(v42, {
    at: './src/rsh/tokenputout.rsh:70:16:application',
    fs: [],
    msg: null,
    who: 'Recipient'
    });
  ;
  return;
  
  
  };

const _ALGO = {
  appApproval: `#pragma version 3
txn RekeyTo
global ZeroAddress
==
assert
txn OnCompletion
int OptIn
==
bz normal
global GroupSize
int 1
==
assert
b done
normal:
gtxna 0 ApplicationArgs 8
store 5
// Check that everyone's here
global GroupSize
int 3
>=
assert
// Check txnAppl (us)
txn GroupIndex
int 0
==
assert
// Check txnFromHandler
int 0
gtxn 2 Sender
byte "{{m1}}"
==
||
gtxn 2 Sender
byte "{{m2}}"
==
||
assert
byte base64(cw==)
app_global_get
gtxna 0 ApplicationArgs 0
==
assert
byte base64(cw==)
gtxna 0 ApplicationArgs 1
app_global_put
byte base64(bA==)
app_global_get
gtxna 0 ApplicationArgs 5
btoi
==
assert
byte base64(bA==)
global Round
app_global_put
int 0
txn NumAccounts
==
assert
byte base64(aA==)
gtxna 0 ApplicationArgs 3
btoi
app_global_put
byte base64(aA==)
app_global_get
bnz halted
txn OnCompletion
int NoOp
==
assert
b done
halted:
txn OnCompletion
int DeleteApplication
==
assert
done:
int 1
return
`,
  appApproval0: `#pragma version 3
// Check that we're an App
txn TypeEnum
int appl
==
assert
txn RekeyTo
global ZeroAddress
==
assert
txn Sender
byte "{{Deployer}}"
==
assert
txn ApplicationID
bz init
global GroupSize
int 2
==
assert
gtxn 1 TypeEnum
int pay
==
assert
gtxn 1 Amount
int 100000
==
assert
// We don't check the receiver, because we don't know it yet, because the escrow account embeds our id
// We don't check the sender, because we don't care... anyone is allowed to fund it. We'll give it back to the deployer, though.
txn OnCompletion
int UpdateApplication
==
assert
byte base64(cw==)
// compute state in HM_Set 0
int 0
itob
keccak256
app_global_put
byte base64(bA==)
global Round
app_global_put
byte base64(aA==)
int 0
app_global_put
b done
init:
global GroupSize
int 1
==
assert
txn OnCompletion
int NoOp
==
assert
done:
int 1
return
`,
  appClear: `#pragma version 3
// We're alone
global GroupSize
int 1
==
assert
// We're halted
byte base64(aA==)
app_global_get
int 1
==
assert
done:
int 1
return
`,
  ctc: `#pragma version 3
// Check size
global GroupSize
int 3
>=
assert
// Check txnAppl
gtxn 0 TypeEnum
int appl
==
assert
gtxn 0 ApplicationID
byte "{{ApplicationID}}"
btoi
==
assert
// Don't check anything else, because app does
// Check us
txn TypeEnum
int pay
==
int axfer
dup2
==
||
assert
txn RekeyTo
global ZeroAddress
==
assert
txn GroupIndex
int 3
>=
assert
done:
int 1
return
`,
  mapArgSize: 165,
  mapDataKeys: 0,
  mapDataSize: 0,
  mapRecordSize: 33,
  stepargs: [null, {
    count: 9,
    size: 286
    }, {
    count: 9,
    size: 294
    }],
  steps: [null, `#pragma version 3
gtxna 0 ApplicationArgs 1
store 0
gtxna 0 ApplicationArgs 2
store 1
gtxna 0 ApplicationArgs 3
store 2
gtxna 0 ApplicationArgs 4
store 3
gtxna 0 ApplicationArgs 5
store 4
gtxna 0 ApplicationArgs 8
store 5
int 0
store 6
gtxna 0 ApplicationArgs 7
dup
substring 0 32
store 255
dup
substring 32 40
btoi
store 254
pop
// Handler 1
// Check txnAppl
gtxn 0 TypeEnum
int appl
==
assert
gtxn 0 ApplicationID
byte "{{ApplicationID}}"
btoi
==
assert
gtxn 0 NumAppArgs
int 9
==
assert
// Check txnToHandler
gtxn 1 TypeEnum
int pay
==
assert
gtxn 1 Receiver
txn Sender
==
assert
gtxn 1 Amount
gtxn 2 Fee
int 100000
+
==
assert
// Check txnFromHandler (us)
txn GroupIndex
int 2
==
assert
txn TypeEnum
int pay
==
assert
txn Amount
int 0
==
assert
txn Receiver
gtxn 1 Sender
==
assert
// compute state in HM_Check 0
int 0
itob
keccak256
gtxna 0 ApplicationArgs 0
==
assert
txn CloseRemainderTo
gtxn 1 Sender
==
assert
// Run body
// "CheckPay"
// "./src/rsh/tokenputout.rsh:62:20:dot"
// "[]"
gtxn 3 TypeEnum
int pay
==
assert
gtxn 3 Receiver
byte "{{ContractAddr}}"
==
assert
gtxn 3 Amount
load 3
btoi
-
load 254
==
assert
// We don't care who the sender is... this means that you can get other people to pay for you if you want.
byte base64()
load 1
==
assert
// compute state in HM_Set 1
int 1
itob
load 255
concat
load 254
itob
concat
keccak256
load 0
==
assert
load 2
btoi
int 0
==
assert
// Check GroupSize
global GroupSize
int 4
==
assert
load 3
btoi
int 0
==
assert
// Check time limits
checkAccts:
gtxn 0 NumAccounts
load 6
==
assert
done:
int 1
return
`, `#pragma version 3
gtxna 0 ApplicationArgs 1
store 0
gtxna 0 ApplicationArgs 2
store 1
gtxna 0 ApplicationArgs 3
store 2
gtxna 0 ApplicationArgs 4
store 3
gtxna 0 ApplicationArgs 5
store 4
gtxna 0 ApplicationArgs 8
store 5
int 0
store 6
gtxna 0 ApplicationArgs 6
dup
substring 0 32
store 255
dup
substring 32 40
btoi
store 254
pop
gtxna 0 ApplicationArgs 7
dup
substring 0 8
btoi
store 253
pop
// Handler 2
// Check txnAppl
gtxn 0 TypeEnum
int appl
==
assert
gtxn 0 ApplicationID
byte "{{ApplicationID}}"
btoi
==
assert
gtxn 0 NumAppArgs
int 9
==
assert
// Check txnToHandler
gtxn 1 TypeEnum
int pay
==
assert
gtxn 1 Receiver
txn Sender
==
assert
gtxn 1 Amount
gtxn 2 Fee
int 100000
+
==
assert
// Check txnFromHandler (us)
txn GroupIndex
int 2
==
assert
txn TypeEnum
int pay
==
assert
txn Amount
int 0
==
assert
txn Receiver
gtxn 1 Sender
==
assert
// compute state in HM_Check 1
int 1
itob
load 255
concat
load 254
itob
concat
keccak256
gtxna 0 ApplicationArgs 0
==
assert
txn CloseRemainderTo
gtxn 1 Sender
==
assert
// Run body
// "CheckPay"
// "./src/rsh/tokenputout.rsh:69:19:dot"
// "[]"
gtxn 3 TypeEnum
int pay
==
assert
gtxn 3 Receiver
byte "{{ContractAddr}}"
==
assert
gtxn 3 Amount
load 3
btoi
==
assert
// We don't care who the sender is... this means that you can get other people to pay for you if you want.
// Nothing
// "./src/rsh/tokenputout.rsh:70:16:application"
// "[]"
load 255
load 253
itob
keccak256
==
assert
gtxn 4 TypeEnum
int pay
==
assert
gtxn 4 Receiver
gtxn 0 Sender
==
assert
gtxn 4 Amount
load 254
==
assert
gtxn 4 Sender
byte "{{ContractAddr}}"
==
assert
byte base64()
load 1
==
assert
gtxn 5 TypeEnum
int pay
==
assert
// We don't check the receiver
gtxn 5 Amount
int 0
==
assert
gtxn 5 Sender
byte "{{ContractAddr}}"
==
assert
gtxn 5 CloseRemainderTo
byte "{{Deployer}}"
==
assert
load 2
btoi
int 1
==
assert
// Check GroupSize
global GroupSize
int 6
==
assert
load 3
btoi
gtxn 4 Fee
gtxn 5 Fee
+
==
assert
// Check time limits
checkAccts:
gtxn 0 NumAccounts
load 6
==
assert
done:
int 1
return
`],
  unsupported: [],
  version: 1,
  viewKeys: 0,
  viewSize: 0
  };
const _ETH = {
  ABI: `[
  {
    "inputs": [],
    "stateMutability": "payable",
    "type": "constructor"
  },
  {
    "inputs": [
      {
        "internalType": "uint256",
        "name": "msg",
        "type": "uint256"
      }
    ],
    "name": "ReachError",
    "type": "error"
  },
  {
    "anonymous": false,
    "inputs": [],
    "name": "e0",
    "type": "event"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "components": [
          {
            "components": [
              {
                "internalType": "uint256",
                "name": "v20",
                "type": "uint256"
              }
            ],
            "internalType": "struct T0",
            "name": "svs",
            "type": "tuple"
          },
          {
            "components": [
              {
                "internalType": "uint256",
                "name": "v26",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v27",
                "type": "uint256"
              }
            ],
            "internalType": "struct T2",
            "name": "msg",
            "type": "tuple"
          }
        ],
        "indexed": false,
        "internalType": "struct T3",
        "name": "_a",
        "type": "tuple"
      }
    ],
    "name": "e1",
    "type": "event"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "components": [
          {
            "components": [
              {
                "internalType": "uint256",
                "name": "v26",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v27",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v30",
                "type": "uint256"
              }
            ],
            "internalType": "struct T1",
            "name": "svs",
            "type": "tuple"
          },
          {
            "components": [
              {
                "internalType": "uint256",
                "name": "v38",
                "type": "uint256"
              }
            ],
            "internalType": "struct T4",
            "name": "msg",
            "type": "tuple"
          }
        ],
        "indexed": false,
        "internalType": "struct T5",
        "name": "_a",
        "type": "tuple"
      }
    ],
    "name": "e2",
    "type": "event"
  },
  {
    "inputs": [
      {
        "components": [
          {
            "components": [
              {
                "internalType": "uint256",
                "name": "v20",
                "type": "uint256"
              }
            ],
            "internalType": "struct T0",
            "name": "svs",
            "type": "tuple"
          },
          {
            "components": [
              {
                "internalType": "uint256",
                "name": "v26",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v27",
                "type": "uint256"
              }
            ],
            "internalType": "struct T2",
            "name": "msg",
            "type": "tuple"
          }
        ],
        "internalType": "struct T3",
        "name": "_a",
        "type": "tuple"
      }
    ],
    "name": "m1",
    "outputs": [],
    "stateMutability": "payable",
    "type": "function"
  },
  {
    "inputs": [
      {
        "components": [
          {
            "components": [
              {
                "internalType": "uint256",
                "name": "v26",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v27",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v30",
                "type": "uint256"
              }
            ],
            "internalType": "struct T1",
            "name": "svs",
            "type": "tuple"
          },
          {
            "components": [
              {
                "internalType": "uint256",
                "name": "v38",
                "type": "uint256"
              }
            ],
            "internalType": "struct T4",
            "name": "msg",
            "type": "tuple"
          }
        ],
        "internalType": "struct T5",
        "name": "_a",
        "type": "tuple"
      }
    ],
    "name": "m2",
    "outputs": [],
    "stateMutability": "payable",
    "type": "function"
  },
  {
    "stateMutability": "payable",
    "type": "receive"
  }
]`,
  Bytecode: `0x608060408190527f49ff028a829527a47ec6839c7147b484eccf5a2a94853eddac09cef44d9d4e9e90600090a16040805160208082018352438252825180820184526000808252925181528351808301849052905181850152835180820385018152606090910190935282519201919091209055610336806100826000396000f3fe60806040526004361061002d5760003560e01c806310f48adc1461003957806352c8aab61461004e57600080fd5b3661003457005b600080fd5b61004c6100473660046102a7565b610061565b005b61004c61005c3660046102bf565b610175565b60408051600060208201528235918101919091526100a0906060016040516020818303038152906040528051906020012060001c60005414600861027e565b600080556100b534604083013514600761027e565b604080518235815260208084013590820152828201358183015290517f50407a4b90da10f3360d0261cffec8fb082a93699e2f91827b46a545fd3694e99181900360600190a161011f60405180606001604052806000815260200160008152602001600081525090565b602082810135825260409283013582820190815243848401908152845160018185015293518486015290516060840152516080808401919091528351808403909101815260a09092019092528051910120600055565b6040516101b19061018d9060019084906020016102d1565b6040516020818303038152906040528051906020012060001c60005414600b61027e565b600080556101c13415600961027e565b60408051606083013560208201526101f6910160408051601f198184030181529190528051602090910120823514600a61027e565b6040513390602083013580156108fc02916000818181858888f19350505050158015610226573d6000803e3d6000fd5b5060408051823581526020808401359082015282820135818301526060808401359082015290517f0f737e484d8af9204b9f39ae7d9877dfeb9f61794bbbfb35bee8d8dbf75303759181900360800190a16000805533ff5b816102a35760405163100960cb60e01b81526004810182905260240160405180910390fd5b5050565b6000606082840312156102b957600080fd5b50919050565b6000608082840312156102b957600080fd5b828152608081016102f960208301848035825260208082013590830152604090810135910152565b939250505056fea264697066735822122074e9e7ed4007643e63d9985cace9ab7789d587f69bff84a84d135440189e7d5864736f6c63430008050033`,
  BytecodeLen: 952,
  Which: `oD`,
  deployMode: `DM_constructor`,
  views: {
    }
  };

export const _Connectors = {
  ALGO: _ALGO,
  ETH: _ETH
  };

