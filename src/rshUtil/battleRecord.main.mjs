// Automatically generated with Reach 0.1.2
/* eslint-disable */
export const _version = '0.1.2';


export function getExports(s) {
  const stdlib = s.reachStdlib;
  return {
    };
  };

export function _getViews(s, viewlib) {
  const stdlib = s.reachStdlib;
  
  return {
    infos: {
      },
    views: {
      }
    };
  
  };

export function _getMaps(s) {
  const stdlib = s.reachStdlib;
  const ctc0 = stdlib.T_Tuple([]);
  return {
    mapDataTy: ctc0
    };
  };

export async function Oneself(ctc, interact) {
  if (ctc.sendrecv === undefined) {
    return Promise.reject(new Error(`The backend for Oneself expects to receive a contract as its first argument.`));}
  if (typeof(interact) !== 'object') {
    return Promise.reject(new Error(`The backend for Oneself expects to receive an interact object as its second argument.`));}
  const stdlib = ctc.stdlib;
  const ctc0 = stdlib.T_UInt;
  const ctc1 = stdlib.T_Null;
  const ctc2 = stdlib.T_Digest;
  const ctc3 = stdlib.T_Tuple([ctc0, ctc0]);
  const ctc4 = stdlib.T_Tuple([ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0]);
  const ctc5 = stdlib.T_Tuple([ctc0]);
  const ctc6 = stdlib.T_Address;
  const ctc7 = stdlib.T_Tuple([ctc0, ctc6, ctc0, ctc6, ctc2, ctc0, ctc2, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0]);
  const ctc8 = stdlib.T_Tuple([ctc0, ctc6, ctc0, ctc6, ctc2, ctc0, ctc2, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0]);
  const ctc9 = stdlib.T_Tuple([ctc0, ctc6, ctc0, ctc6, ctc2, ctc2, ctc0, ctc0, ctc0, ctc0, ctc0]);
  const ctc10 = stdlib.T_Tuple([ctc0, ctc6, ctc0, ctc6, ctc2, ctc2, ctc0, ctc0, ctc0, ctc0]);
  const ctc11 = stdlib.T_Tuple([ctc0, ctc6, ctc0, ctc6, ctc2, ctc0, ctc2, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0]);
  const ctc12 = stdlib.T_Tuple([ctc0, ctc6, ctc0, ctc6, ctc2, ctc0, ctc2, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0]);
  const ctc13 = stdlib.T_Tuple([ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0]);
  const ctc14 = stdlib.T_Tuple([ctc0, ctc6, ctc0, ctc6, ctc2, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0]);
  const ctc15 = stdlib.T_Tuple([ctc0, ctc6, ctc0, ctc6, ctc2, ctc0, ctc0, ctc0, ctc0, ctc0]);
  const ctc16 = stdlib.T_Tuple([ctc0, ctc6, ctc0, ctc6, ctc0, ctc2, ctc0, ctc0]);
  const ctc17 = stdlib.T_Tuple([ctc0, ctc6, ctc0, ctc6, ctc0, ctc2, ctc0]);
  const ctc18 = stdlib.T_Tuple([]);
  const ctc19 = stdlib.T_Tuple([ctc0, ctc6, ctc0, ctc6, ctc0, ctc0]);
  const ctc20 = stdlib.T_Tuple([ctc0, ctc6, ctc0, ctc6, ctc0]);
  const ctc21 = stdlib.T_Tuple([ctc0, ctc6, ctc0, ctc0]);
  const ctc22 = stdlib.T_Tuple([ctc0, ctc6, ctc0]);
  
  
  const v19 = await ctc.creationTime();
  const v18 = stdlib.protect(ctc0, interact.wager, 'for Oneself\'s interact field wager');
  
  const txn1 = await (ctc.sendrecv(1, 1, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:233:11:dot', stdlib.UInt_max, 0), [ctc0, ctc0], [v19, v18], [v18, []], [ctc0], true, true, false, (async (txn1) => {
    const sim_r = { txns: [], mapRefs: [], mapsPrev: [], mapsNext: [] };
    
    sim_r.prevSt = stdlib.digest(ctc3, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:233:11:dot', stdlib.UInt_max, 0), v19]);
    sim_r.prevSt_noPrevTime = stdlib.digest(ctc5, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:233:11:dot', stdlib.UInt_max, 0)]);
    const [v24] = txn1.data;
    const v27 = txn1.time;
    const v23 = txn1.from;
    
    sim_r.txns.push({
      amt: v24,
      kind: 'to',
      tok: undefined
      });
    sim_r.nextSt = stdlib.digest(ctc21, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:235:17:after expr stmt semicolon', stdlib.UInt_max, 1), v23, v24, v27]);
    sim_r.nextSt_noTime = stdlib.digest(ctc22, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:235:17:after expr stmt semicolon', stdlib.UInt_max, 1), v23, v24]);
    sim_r.view = [ctc5, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:235:17:after expr stmt semicolon', stdlib.UInt_max, 0)]];
    sim_r.isHalt = false;
    
    return sim_r;
    })));
  const [v24] = txn1.data;
  const v27 = txn1.time;
  const v23 = txn1.from;
  ;
  const txn2 = await (ctc.recv(2, 0, [], false, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:54:22:decimal', stdlib.UInt_max, 10)));
  if (txn2.didTimeout) {
    const txn3 = await (ctc.sendrecv(3, 0, stdlib.checkedBigNumberify('reach standard library:209:7:dot', stdlib.UInt_max, 2), [ctc6, ctc0, ctc0], [v23, v24, v27], [stdlib.checkedBigNumberify('reach standard library:decimal', stdlib.UInt_max, 0), []], [], true, true, false, (async (txn3) => {
      const sim_r = { txns: [], mapRefs: [], mapsPrev: [], mapsNext: [] };
      
      sim_r.prevSt = stdlib.digest(ctc21, [stdlib.checkedBigNumberify('reach standard library:209:7:dot', stdlib.UInt_max, 1), v23, v24, v27]);
      sim_r.prevSt_noPrevTime = stdlib.digest(ctc22, [stdlib.checkedBigNumberify('reach standard library:209:7:dot', stdlib.UInt_max, 1), v23, v24]);
      const [] = txn3.data;
      const v977 = txn3.time;
      const v974 = txn3.from;
      
      sim_r.txns.push({
        amt: stdlib.checkedBigNumberify('reach standard library:decimal', stdlib.UInt_max, 0),
        kind: 'to',
        tok: undefined
        });
      const v976 = stdlib.addressEq(v23, v974);
      stdlib.assert(v976, {
        at: 'reach standard library:209:7:dot',
        fs: ['at ./src/rsh/battleRecord.rsh:242:57:application call to "closeTo" (defined at: reach standard library:207:8:function exp)'],
        msg: 'sender correct',
        who: 'Oneself'
        });
      sim_r.txns.push({
        amt: v24,
        kind: 'from',
        to: v23,
        tok: undefined
        });
      sim_r.txns.push({
        kind: 'halt',
        tok: undefined
        })
      sim_r.nextSt = stdlib.digest(ctc18, []);
      sim_r.nextSt_noTime = stdlib.digest(ctc18, []);
      sim_r.view = [ctc18, []];
      sim_r.isHalt = true;
      
      return sim_r;
      })));
    const [] = txn3.data;
    const v977 = txn3.time;
    const v974 = txn3.from;
    ;
    const v976 = stdlib.addressEq(v23, v974);
    stdlib.assert(v976, {
      at: 'reach standard library:209:7:dot',
      fs: ['at ./src/rsh/battleRecord.rsh:242:57:application call to "closeTo" (defined at: reach standard library:207:8:function exp)'],
      msg: 'sender correct',
      who: 'Oneself'
      });
    ;
    stdlib.protect(ctc1, await interact.payTimeoutHandle(), {
      at: './src/rsh/battleRecord.rsh:223:42:application',
      fs: ['at ./src/rsh/battleRecord.rsh:222:17:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:222:29:function exp)', 'at reach standard library:212:8:application call to "after" (defined at: ./src/rsh/battleRecord.rsh:221:37:function exp)', 'at ./src/rsh/battleRecord.rsh:242:57:application call to "closeTo" (defined at: reach standard library:207:8:function exp)'],
      msg: 'payTimeoutHandle',
      who: 'Oneself'
      });
    
    return;
    }
  else {
    const [] = txn2.data;
    const v34 = txn2.time;
    const v31 = txn2.from;
    const v33 = stdlib.add(v24, v24);
    ;
    stdlib.protect(ctc1, await interact.tellGameStartStatus(true), {
      at: './src/rsh/battleRecord.rsh:247:41:application',
      fs: ['at ./src/rsh/battleRecord.rsh:246:13:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:246:25:function exp)'],
      msg: 'tellGameStartStatus',
      who: 'Oneself'
      });
    
    const txn3 = await (ctc.recv(4, 2, [ctc2, ctc0], false, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:57:23:decimal', stdlib.UInt_max, 5)));
    if (txn3.didTimeout) {
      const txn4 = await (ctc.sendrecv(5, 0, stdlib.checkedBigNumberify('reach standard library:209:7:dot', stdlib.UInt_max, 4), [ctc6, ctc0, ctc6, ctc0, ctc0], [v23, v24, v31, v33, v34], [stdlib.checkedBigNumberify('reach standard library:decimal', stdlib.UInt_max, 0), []], [], true, true, false, (async (txn4) => {
        const sim_r = { txns: [], mapRefs: [], mapsPrev: [], mapsNext: [] };
        
        sim_r.prevSt = stdlib.digest(ctc19, [stdlib.checkedBigNumberify('reach standard library:209:7:dot', stdlib.UInt_max, 3), v23, v24, v31, v33, v34]);
        sim_r.prevSt_noPrevTime = stdlib.digest(ctc20, [stdlib.checkedBigNumberify('reach standard library:209:7:dot', stdlib.UInt_max, 3), v23, v24, v31, v33]);
        const [] = txn4.data;
        const v958 = txn4.time;
        const v955 = txn4.from;
        
        sim_r.txns.push({
          amt: stdlib.checkedBigNumberify('reach standard library:decimal', stdlib.UInt_max, 0),
          kind: 'to',
          tok: undefined
          });
        const v957 = stdlib.addressEq(v23, v955);
        stdlib.assert(v957, {
          at: 'reach standard library:209:7:dot',
          fs: ['at ./src/rsh/battleRecord.rsh:266:72:application call to "closeTo" (defined at: reach standard library:207:8:function exp)'],
          msg: 'sender correct',
          who: 'Oneself'
          });
        sim_r.txns.push({
          amt: v33,
          kind: 'from',
          to: v23,
          tok: undefined
          });
        sim_r.txns.push({
          kind: 'halt',
          tok: undefined
          })
        sim_r.nextSt = stdlib.digest(ctc18, []);
        sim_r.nextSt_noTime = stdlib.digest(ctc18, []);
        sim_r.view = [ctc18, []];
        sim_r.isHalt = true;
        
        return sim_r;
        })));
      const [] = txn4.data;
      const v958 = txn4.time;
      const v955 = txn4.from;
      ;
      const v957 = stdlib.addressEq(v23, v955);
      stdlib.assert(v957, {
        at: 'reach standard library:209:7:dot',
        fs: ['at ./src/rsh/battleRecord.rsh:266:72:application call to "closeTo" (defined at: reach standard library:207:8:function exp)'],
        msg: 'sender correct',
        who: 'Oneself'
        });
      ;
      stdlib.protect(ctc1, await interact.waitTimeoutHandle(), {
        at: './src/rsh/battleRecord.rsh:252:43:application',
        fs: ['at ./src/rsh/battleRecord.rsh:251:17:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:251:29:function exp)', 'at reach standard library:212:8:application call to "after" (defined at: ./src/rsh/battleRecord.rsh:250:38:function exp)', 'at ./src/rsh/battleRecord.rsh:266:72:application call to "closeTo" (defined at: reach standard library:207:8:function exp)'],
        msg: 'waitTimeoutHandle',
        who: 'Oneself'
        });
      
      return;
      }
    else {
      const [v51, v52] = txn3.data;
      const v55 = txn3.time;
      const v50 = txn3.from;
      ;
      const v54 = stdlib.addressEq(v31, v50);
      stdlib.assert(v54, {
        at: './src/rsh/battleRecord.rsh:266:11:dot',
        fs: [],
        msg: 'sender correct',
        who: 'Oneself'
        });
      const v65 = stdlib.protect(ctc0, await interact.getHideRoleHandle(), {
        at: './src/rsh/battleRecord.rsh:272:61:application',
        fs: ['at ./src/rsh/battleRecord.rsh:270:15:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:270:19:function exp)'],
        msg: 'getHideRoleHandle',
        who: 'Oneself'
        });
      const v67 = stdlib.protect(ctc0, await interact.random(), {
        at: 'reach standard library:60:31:application',
        fs: ['at ./src/rsh/battleRecord.rsh:273:54:application call to "makeCommitment" (defined at: reach standard library:59:8:function exp)', 'at ./src/rsh/battleRecord.rsh:270:15:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:270:19:function exp)'],
        msg: 'random',
        who: 'Oneself'
        });
      const v68 = stdlib.digest(ctc3, [v67, v65]);
      const v70 = stdlib.protect(ctc0, await interact.setInitialHand(), {
        at: './src/rsh/battleRecord.rsh:275:62:application',
        fs: ['at ./src/rsh/battleRecord.rsh:270:15:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:270:19:function exp)'],
        msg: 'setInitialHand',
        who: 'Oneself'
        });
      
      const txn4 = await (ctc.sendrecv(6, 2, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:277:11:dot', stdlib.UInt_max, 6), [ctc6, ctc0, ctc6, ctc0, ctc2, ctc0, ctc0, ctc2, ctc0], [v23, v24, v31, v33, v51, v52, v55, v68, v70], [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:decimal', stdlib.UInt_max, 0), []], [ctc2, ctc0], true, true, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:57:23:decimal', stdlib.UInt_max, 5), (async (txn4) => {
        const sim_r = { txns: [], mapRefs: [], mapsPrev: [], mapsNext: [] };
        
        sim_r.prevSt = stdlib.digest(ctc16, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:277:11:dot', stdlib.UInt_max, 5), v23, v24, v31, v33, v51, v52, v55]);
        sim_r.prevSt_noPrevTime = stdlib.digest(ctc17, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:277:11:dot', stdlib.UInt_max, 5), v23, v24, v31, v33, v51, v52]);
        const [v72, v73] = txn4.data;
        const v76 = txn4.time;
        const v71 = txn4.from;
        
        sim_r.txns.push({
          amt: stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:decimal', stdlib.UInt_max, 0),
          kind: 'to',
          tok: undefined
          });
        const v75 = stdlib.addressEq(v23, v71);
        stdlib.assert(v75, {
          at: './src/rsh/battleRecord.rsh:277:11:dot',
          fs: [],
          msg: 'sender correct',
          who: 'Oneself'
          });
        
        const v83 = stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:294:162:decimal', stdlib.UInt_max, 0);
        const v84 = stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:294:165:decimal', stdlib.UInt_max, 0);
        const v85 = stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:294:168:decimal', stdlib.UInt_max, 0);
        const v86 = stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:294:171:decimal', stdlib.UInt_max, 0);
        const v89 = stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:294:156:decimal', stdlib.UInt_max, 0);
        const v90 = stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:294:177:decimal', stdlib.UInt_max, 0);
        const v91 = stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:294:159:decimal', stdlib.UInt_max, 1);
        const v92 = stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:294:174:decimal', stdlib.UInt_max, 2);
        const v93 = stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:294:153:decimal', stdlib.UInt_max, 1);
        const v94 = stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:294:186:decimal', stdlib.UInt_max, 50);
        const v992 = v76;
        
        if ((() => {
          const v101 = stdlib.gt(v93, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:298:25:decimal', stdlib.UInt_max, 0));
          const v103 = stdlib.eq(v89, v73);
          const v105 = v103 ? false : true;
          const v108 = v101 ? v105 : false;
          const v110 = stdlib.eq(v89, v52);
          const v112 = v110 ? false : true;
          const v115 = v108 ? v112 : false;
          
          return v115;})()) {
          
          const v123 = stdlib.mod(v91, v92);
          const v125 = stdlib.eq(v123, v90);
          if (v125) {
            sim_r.nextSt = stdlib.digest(ctc7, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:379:25:after expr stmt semicolon', stdlib.UInt_max, 16), v23, v24, v31, v51, v52, v72, v73, v83, v84, v85, v86, v89, v90, v91, v92, v93, v94, v992]);
            sim_r.nextSt_noTime = stdlib.digest(ctc8, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:379:25:after expr stmt semicolon', stdlib.UInt_max, 16), v23, v24, v31, v51, v52, v72, v73, v83, v84, v85, v86, v89, v90, v91, v92, v93, v94]);
            sim_r.view = [ctc5, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:379:25:after expr stmt semicolon', stdlib.UInt_max, 0)]];
            sim_r.isHalt = false;
            }
          else {
            sim_r.nextSt = stdlib.digest(ctc7, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:311:25:after expr stmt semicolon', stdlib.UInt_max, 18), v23, v24, v31, v51, v52, v72, v73, v83, v84, v85, v86, v89, v90, v91, v92, v93, v94, v992]);
            sim_r.nextSt_noTime = stdlib.digest(ctc8, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:311:25:after expr stmt semicolon', stdlib.UInt_max, 18), v23, v24, v31, v51, v52, v72, v73, v83, v84, v85, v86, v89, v90, v91, v92, v93, v94]);
            sim_r.view = [ctc5, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:311:25:after expr stmt semicolon', stdlib.UInt_max, 0)]];
            sim_r.isHalt = false;
            }}
        else {
          sim_r.nextSt = stdlib.digest(ctc9, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:448:17:after expr stmt semicolon', stdlib.UInt_max, 9), v23, v24, v31, v51, v72, v83, v84, v85, v86, v992]);
          sim_r.nextSt_noTime = stdlib.digest(ctc10, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:448:17:after expr stmt semicolon', stdlib.UInt_max, 9), v23, v24, v31, v51, v72, v83, v84, v85, v86]);
          sim_r.view = [ctc5, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:448:17:after expr stmt semicolon', stdlib.UInt_max, 0)]];
          sim_r.isHalt = false;
          }
        return sim_r;
        })));
      if (txn4.didTimeout) {
        const txn5 = await (ctc.recv(7, 0, [], false, false));
        const [] = txn5.data;
        const v939 = txn5.time;
        const v936 = txn5.from;
        ;
        const v938 = stdlib.addressEq(v31, v936);
        stdlib.assert(v938, {
          at: 'reach standard library:209:7:dot',
          fs: ['at ./src/rsh/battleRecord.rsh:277:72:application call to "closeTo" (defined at: reach standard library:207:8:function exp)'],
          msg: 'sender correct',
          who: 'Oneself'
          });
        ;
        stdlib.protect(ctc1, await interact.waitTimeoutHandle(), {
          at: './src/rsh/battleRecord.rsh:252:43:application',
          fs: ['at ./src/rsh/battleRecord.rsh:251:17:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:251:29:function exp)', 'at reach standard library:212:8:application call to "after" (defined at: ./src/rsh/battleRecord.rsh:250:38:function exp)', 'at ./src/rsh/battleRecord.rsh:277:72:application call to "closeTo" (defined at: reach standard library:207:8:function exp)'],
          msg: 'waitTimeoutHandle',
          who: 'Oneself'
          });
        
        return;
        }
      else {
        const [v72, v73] = txn4.data;
        const v76 = txn4.time;
        const v71 = txn4.from;
        ;
        const v75 = stdlib.addressEq(v23, v71);
        stdlib.assert(v75, {
          at: './src/rsh/battleRecord.rsh:277:11:dot',
          fs: [],
          msg: 'sender correct',
          who: 'Oneself'
          });
        stdlib.protect(ctc1, await interact.log('A提交了隐藏数据'), {
          at: './src/rsh/battleRecord.rsh:282:23:application',
          fs: ['at ./src/rsh/battleRecord.rsh:282:23:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:282:23:function exp)', 'at ./src/rsh/battleRecord.rsh:282:23:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:282:23:function exp)'],
          msg: 'log',
          who: 'Oneself'
          });
        
        let v83 = stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:294:162:decimal', stdlib.UInt_max, 0);
        let v84 = stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:294:165:decimal', stdlib.UInt_max, 0);
        let v85 = stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:294:168:decimal', stdlib.UInt_max, 0);
        let v86 = stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:294:171:decimal', stdlib.UInt_max, 0);
        let v89 = stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:294:156:decimal', stdlib.UInt_max, 0);
        let v90 = stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:294:177:decimal', stdlib.UInt_max, 0);
        let v91 = stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:294:159:decimal', stdlib.UInt_max, 1);
        let v92 = stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:294:174:decimal', stdlib.UInt_max, 2);
        let v93 = stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:294:153:decimal', stdlib.UInt_max, 1);
        let v94 = stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:294:186:decimal', stdlib.UInt_max, 50);
        let v992 = v76;
        
        while ((() => {
          const v101 = stdlib.gt(v93, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:298:25:decimal', stdlib.UInt_max, 0));
          const v103 = stdlib.eq(v89, v73);
          const v105 = v103 ? false : true;
          const v108 = v101 ? v105 : false;
          const v110 = stdlib.eq(v89, v52);
          const v112 = v110 ? false : true;
          const v115 = v108 ? v112 : false;
          
          return v115;})()) {
          stdlib.protect(ctc1, await interact.tellEndRoundStatus(v83, v84, v85, v86, v91, v89), {
            at: './src/rsh/battleRecord.rsh:302:44:application',
            fs: ['at ./src/rsh/battleRecord.rsh:301:17:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:301:29:function exp)'],
            msg: 'tellEndRoundStatus',
            who: 'Oneself'
            });
          
          const v123 = stdlib.mod(v91, v92);
          const v125 = stdlib.eq(v123, v90);
          if (v125) {
            const txn5 = await (ctc.recv(12, 13, [ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0], false, false));
            const [v494, v495, v496, v497, v498, v499, v500, v501, v502, v503, v504, v505, v506] = txn5.data;
            const v509 = txn5.time;
            const v493 = txn5.from;
            ;
            const v508 = stdlib.addressEq(v31, v493);
            stdlib.assert(v508, {
              at: './src/rsh/battleRecord.rsh:402:19:dot',
              fs: [],
              msg: 'sender correct',
              who: 'Oneself'
              });
            const v513 = stdlib.protect(ctc4, await interact.waitUseCardAction(v494, v497, v500, v503, v506), {
              at: './src/rsh/battleRecord.rsh:408:196:application',
              fs: ['at ./src/rsh/battleRecord.rsh:406:23:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:406:27:function exp)'],
              msg: 'waitUseCardAction',
              who: 'Oneself'
              });
            const v514 = v513[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:408:27:array', stdlib.UInt_max, 0)];
            const v515 = v513[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:408:27:array', stdlib.UInt_max, 1)];
            const v516 = v513[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:408:27:array', stdlib.UInt_max, 2)];
            const v517 = v513[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:408:27:array', stdlib.UInt_max, 3)];
            const v518 = v513[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:408:27:array', stdlib.UInt_max, 4)];
            const v519 = v513[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:408:27:array', stdlib.UInt_max, 5)];
            const v520 = v513[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:408:27:array', stdlib.UInt_max, 6)];
            const v521 = v513[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:408:27:array', stdlib.UInt_max, 7)];
            const v522 = v513[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:408:27:array', stdlib.UInt_max, 8)];
            const v523 = v513[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:408:27:array', stdlib.UInt_max, 9)];
            const v524 = v513[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:408:27:array', stdlib.UInt_max, 10)];
            const v525 = v513[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:408:27:array', stdlib.UInt_max, 11)];
            
            const txn6 = await (ctc.sendrecv(13, 12, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:429:19:dot', stdlib.UInt_max, 24), [ctc6, ctc0, ctc6, ctc2, ctc0, ctc2, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0], [v23, v24, v31, v51, v52, v72, v73, v83, v84, v85, v86, v89, v90, v91, v92, v93, v94, v495, v496, v498, v499, v501, v502, v504, v509, v514, v515, v516, v517, v518, v519, v520, v521, v522, v523, v524, v525], [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:decimal', stdlib.UInt_max, 0), []], [ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0], true, true, false, (async (txn6) => {
              const sim_r = { txns: [], mapRefs: [], mapsPrev: [], mapsNext: [] };
              
              sim_r.prevSt = stdlib.digest(ctc11, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:429:19:dot', stdlib.UInt_max, 17), v23, v24, v31, v51, v52, v72, v73, v83, v84, v85, v86, v89, v90, v91, v92, v93, v94, v495, v496, v498, v499, v501, v502, v504, v509]);
              sim_r.prevSt_noPrevTime = stdlib.digest(ctc12, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:429:19:dot', stdlib.UInt_max, 17), v23, v24, v31, v51, v52, v72, v73, v83, v84, v85, v86, v89, v90, v91, v92, v93, v94, v495, v496, v498, v499, v501, v502, v504]);
              const [v527, v528, v529, v530, v531, v532, v533, v534, v535, v536, v537, v538] = txn6.data;
              const v541 = txn6.time;
              const v526 = txn6.from;
              
              sim_r.txns.push({
                amt: stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:decimal', stdlib.UInt_max, 0),
                kind: 'to',
                tok: undefined
                });
              const v540 = stdlib.addressEq(v23, v526);
              stdlib.assert(v540, {
                at: './src/rsh/battleRecord.rsh:429:19:dot',
                fs: [],
                msg: 'sender correct',
                who: 'Oneself'
                });
              let v548;
              const v549 = stdlib.lt(stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:67:9:decimal', stdlib.UInt_max, 0), v495);
              const v550 = stdlib.lt(v495, v94);
              const v551 = v549 ? v550 : false;
              if (v551) {
                const v619 = [v85, v86, v83, v84];
                v548 = v619;
                }
              else {
                const v559 = stdlib.eq(v495, v528);
                if (v559) {
                  const v620 = [v85, v86, v83, v84];
                  v548 = v620;
                  }
                else {
                  const v567 = stdlib.gt(v496, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:80:57:decimal', stdlib.UInt_max, 0));
                  if (v567) {
                    const v575 = stdlib.gt(v84, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:85:83:decimal', stdlib.UInt_max, 0));
                    if (v575) {
                      const v583 = stdlib.add(v83, v495);
                      const v621 = [v85, v86, v583, v84];
                      v548 = v621;
                      }
                    else {
                      const v585 = stdlib.gt(v83, v495);
                      if (v585) {
                        const v593 = stdlib.sub(v83, v495);
                        const v622 = [v85, v86, v593, v84];
                        v548 = v622;
                        }
                      else {
                        const v595 = stdlib.sub(v495, v83);
                        const v623 = [v85, v86, v595, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:96:59:decimal', stdlib.UInt_max, 1)];
                        v548 = v623;
                        }
                      }
                    }
                  else {
                    const v597 = stdlib.gt(v86, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:100:81:decimal', stdlib.UInt_max, 0));
                    if (v597) {
                      const v605 = stdlib.gt(v85, v495);
                      if (v605) {
                        const v613 = stdlib.sub(v85, v495);
                        const v624 = [v613, v86, v83, v84];
                        v548 = v624;
                        }
                      else {
                        const v615 = stdlib.sub(v495, v85);
                        const v625 = [v615, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:110:45:decimal', stdlib.UInt_max, 0), v83, v84];
                        v548 = v625;
                        }
                      }
                    else {
                      const v617 = stdlib.add(v85, v495);
                      const v626 = [v617, v86, v83, v84];
                      v548 = v626;
                      }
                    }
                  }
                }
              const v627 = v548[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:435:23:array', stdlib.UInt_max, 0)];
              const v628 = v548[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:435:23:array', stdlib.UInt_max, 1)];
              const v629 = v548[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:435:23:array', stdlib.UInt_max, 2)];
              const v630 = v548[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:435:23:array', stdlib.UInt_max, 3)];
              let v631;
              const v632 = stdlib.lt(stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:67:9:decimal', stdlib.UInt_max, 0), v498);
              const v633 = stdlib.lt(v498, v94);
              const v634 = v632 ? v633 : false;
              if (v634) {
                const v702 = [v627, v628, v629, v630];
                v631 = v702;
                }
              else {
                const v642 = stdlib.eq(v498, v531);
                if (v642) {
                  const v703 = [v627, v628, v629, v630];
                  v631 = v703;
                  }
                else {
                  const v650 = stdlib.gt(v499, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:80:57:decimal', stdlib.UInt_max, 0));
                  if (v650) {
                    const v658 = stdlib.gt(v630, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:85:83:decimal', stdlib.UInt_max, 0));
                    if (v658) {
                      const v666 = stdlib.add(v629, v498);
                      const v704 = [v627, v628, v666, v630];
                      v631 = v704;
                      }
                    else {
                      const v668 = stdlib.gt(v629, v498);
                      if (v668) {
                        const v676 = stdlib.sub(v629, v498);
                        const v705 = [v627, v628, v676, v630];
                        v631 = v705;
                        }
                      else {
                        const v678 = stdlib.sub(v498, v629);
                        const v706 = [v627, v628, v678, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:96:59:decimal', stdlib.UInt_max, 1)];
                        v631 = v706;
                        }
                      }
                    }
                  else {
                    const v680 = stdlib.gt(v628, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:100:81:decimal', stdlib.UInt_max, 0));
                    if (v680) {
                      const v688 = stdlib.gt(v627, v498);
                      if (v688) {
                        const v696 = stdlib.sub(v627, v498);
                        const v707 = [v696, v628, v629, v630];
                        v631 = v707;
                        }
                      else {
                        const v698 = stdlib.sub(v498, v627);
                        const v708 = [v698, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:110:45:decimal', stdlib.UInt_max, 0), v629, v630];
                        v631 = v708;
                        }
                      }
                    else {
                      const v700 = stdlib.add(v627, v498);
                      const v709 = [v700, v628, v629, v630];
                      v631 = v709;
                      }
                    }
                  }
                }
              const v710 = v631[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:436:23:array', stdlib.UInt_max, 0)];
              const v711 = v631[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:436:23:array', stdlib.UInt_max, 1)];
              const v712 = v631[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:436:23:array', stdlib.UInt_max, 2)];
              const v713 = v631[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:436:23:array', stdlib.UInt_max, 3)];
              let v714;
              const v715 = stdlib.lt(stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:67:9:decimal', stdlib.UInt_max, 0), v501);
              const v716 = stdlib.lt(v501, v94);
              const v717 = v715 ? v716 : false;
              if (v717) {
                const v785 = [v710, v711, v712, v713];
                v714 = v785;
                }
              else {
                const v725 = stdlib.eq(v501, v534);
                if (v725) {
                  const v786 = [v710, v711, v712, v713];
                  v714 = v786;
                  }
                else {
                  const v733 = stdlib.gt(v502, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:80:57:decimal', stdlib.UInt_max, 0));
                  if (v733) {
                    const v741 = stdlib.gt(v713, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:85:83:decimal', stdlib.UInt_max, 0));
                    if (v741) {
                      const v749 = stdlib.add(v712, v501);
                      const v787 = [v710, v711, v749, v713];
                      v714 = v787;
                      }
                    else {
                      const v751 = stdlib.gt(v712, v501);
                      if (v751) {
                        const v759 = stdlib.sub(v712, v501);
                        const v788 = [v710, v711, v759, v713];
                        v714 = v788;
                        }
                      else {
                        const v761 = stdlib.sub(v501, v712);
                        const v789 = [v710, v711, v761, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:96:59:decimal', stdlib.UInt_max, 1)];
                        v714 = v789;
                        }
                      }
                    }
                  else {
                    const v763 = stdlib.gt(v711, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:100:81:decimal', stdlib.UInt_max, 0));
                    if (v763) {
                      const v771 = stdlib.gt(v710, v501);
                      if (v771) {
                        const v779 = stdlib.sub(v710, v501);
                        const v790 = [v779, v711, v712, v713];
                        v714 = v790;
                        }
                      else {
                        const v781 = stdlib.sub(v501, v710);
                        const v791 = [v781, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:110:45:decimal', stdlib.UInt_max, 0), v712, v713];
                        v714 = v791;
                        }
                      }
                    else {
                      const v783 = stdlib.add(v710, v501);
                      const v792 = [v783, v711, v712, v713];
                      v714 = v792;
                      }
                    }
                  }
                }
              const v793 = v714[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:437:23:array', stdlib.UInt_max, 0)];
              const v794 = v714[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:437:23:array', stdlib.UInt_max, 1)];
              const v795 = v714[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:437:23:array', stdlib.UInt_max, 2)];
              const v796 = v714[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:437:23:array', stdlib.UInt_max, 3)];
              let v797;
              const v798 = stdlib.gt(v504, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:129:18:decimal', stdlib.UInt_max, 0));
              if (v798) {
                const v799 = stdlib.add(v504, v537);
                const v800 = stdlib.gt(v89, v52);
                if (v800) {
                  const v801 = stdlib.sub(v89, v52);
                  const v802 = stdlib.add(v801, v799);
                  const v810 = [v802];
                  v797 = v810;
                  }
                else {
                  const v804 = stdlib.add(v89, v799);
                  const v811 = [v804];
                  v797 = v811;
                  }
                }
              else {
                const v806 = stdlib.gt(v89, v52);
                if (v806) {
                  const v807 = stdlib.sub(v89, v52);
                  const v812 = [v807];
                  v797 = v812;
                  }
                else {
                  const v813 = [v89];
                  v797 = v813;
                  }
                }
              const v814 = v797[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:439:23:array', stdlib.UInt_max, 0)];
              const v815 = stdlib.add(v91, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:442:145:decimal', stdlib.UInt_max, 1));
              const cv83 = v795;
              const cv84 = v796;
              const cv85 = v793;
              const cv86 = v794;
              const cv89 = v814;
              const cv90 = v90;
              const cv91 = v815;
              const cv92 = v92;
              const cv93 = v93;
              const cv94 = v94;
              const cv992 = v541;
              
              (() => {
                const v83 = cv83;
                const v84 = cv84;
                const v85 = cv85;
                const v86 = cv86;
                const v89 = cv89;
                const v90 = cv90;
                const v91 = cv91;
                const v92 = cv92;
                const v93 = cv93;
                const v94 = cv94;
                const v992 = cv992;
                
                if ((() => {
                  const v101 = stdlib.gt(v93, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:298:25:decimal', stdlib.UInt_max, 0));
                  const v103 = stdlib.eq(v89, v73);
                  const v105 = v103 ? false : true;
                  const v108 = v101 ? v105 : false;
                  const v110 = stdlib.eq(v89, v52);
                  const v112 = v110 ? false : true;
                  const v115 = v108 ? v112 : false;
                  
                  return v115;})()) {
                  
                  const v123 = stdlib.mod(v91, v92);
                  const v125 = stdlib.eq(v123, v90);
                  if (v125) {
                    sim_r.nextSt = stdlib.digest(ctc7, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:379:25:after expr stmt semicolon', stdlib.UInt_max, 16), v23, v24, v31, v51, v52, v72, v73, v83, v84, v85, v86, v89, v90, v91, v92, v93, v94, v992]);
                    sim_r.nextSt_noTime = stdlib.digest(ctc8, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:379:25:after expr stmt semicolon', stdlib.UInt_max, 16), v23, v24, v31, v51, v52, v72, v73, v83, v84, v85, v86, v89, v90, v91, v92, v93, v94]);
                    sim_r.view = [ctc5, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:379:25:after expr stmt semicolon', stdlib.UInt_max, 0)]];
                    sim_r.isHalt = false;
                    }
                  else {
                    sim_r.nextSt = stdlib.digest(ctc7, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:311:25:after expr stmt semicolon', stdlib.UInt_max, 18), v23, v24, v31, v51, v52, v72, v73, v83, v84, v85, v86, v89, v90, v91, v92, v93, v94, v992]);
                    sim_r.nextSt_noTime = stdlib.digest(ctc8, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:311:25:after expr stmt semicolon', stdlib.UInt_max, 18), v23, v24, v31, v51, v52, v72, v73, v83, v84, v85, v86, v89, v90, v91, v92, v93, v94]);
                    sim_r.view = [ctc5, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:311:25:after expr stmt semicolon', stdlib.UInt_max, 0)]];
                    sim_r.isHalt = false;
                    }}
                else {
                  sim_r.nextSt = stdlib.digest(ctc9, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:448:17:after expr stmt semicolon', stdlib.UInt_max, 9), v23, v24, v31, v51, v72, v83, v84, v85, v86, v992]);
                  sim_r.nextSt_noTime = stdlib.digest(ctc10, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:448:17:after expr stmt semicolon', stdlib.UInt_max, 9), v23, v24, v31, v51, v72, v83, v84, v85, v86]);
                  sim_r.view = [ctc5, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:448:17:after expr stmt semicolon', stdlib.UInt_max, 0)]];
                  sim_r.isHalt = false;
                  }})();
              return sim_r;
              })));
            const [v527, v528, v529, v530, v531, v532, v533, v534, v535, v536, v537, v538] = txn6.data;
            const v541 = txn6.time;
            const v526 = txn6.from;
            ;
            const v540 = stdlib.addressEq(v23, v526);
            stdlib.assert(v540, {
              at: './src/rsh/battleRecord.rsh:429:19:dot',
              fs: [],
              msg: 'sender correct',
              who: 'Oneself'
              });
            let v548;
            const v549 = stdlib.lt(stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:67:9:decimal', stdlib.UInt_max, 0), v495);
            const v550 = stdlib.lt(v495, v94);
            const v551 = v549 ? v550 : false;
            if (v551) {
              const v619 = [v85, v86, v83, v84];
              v548 = v619;
              }
            else {
              const v559 = stdlib.eq(v495, v528);
              if (v559) {
                const v620 = [v85, v86, v83, v84];
                v548 = v620;
                }
              else {
                const v567 = stdlib.gt(v496, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:80:57:decimal', stdlib.UInt_max, 0));
                if (v567) {
                  const v575 = stdlib.gt(v84, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:85:83:decimal', stdlib.UInt_max, 0));
                  if (v575) {
                    const v583 = stdlib.add(v83, v495);
                    const v621 = [v85, v86, v583, v84];
                    v548 = v621;
                    }
                  else {
                    const v585 = stdlib.gt(v83, v495);
                    if (v585) {
                      const v593 = stdlib.sub(v83, v495);
                      const v622 = [v85, v86, v593, v84];
                      v548 = v622;
                      }
                    else {
                      const v595 = stdlib.sub(v495, v83);
                      const v623 = [v85, v86, v595, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:96:59:decimal', stdlib.UInt_max, 1)];
                      v548 = v623;
                      }
                    }
                  }
                else {
                  const v597 = stdlib.gt(v86, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:100:81:decimal', stdlib.UInt_max, 0));
                  if (v597) {
                    const v605 = stdlib.gt(v85, v495);
                    if (v605) {
                      const v613 = stdlib.sub(v85, v495);
                      const v624 = [v613, v86, v83, v84];
                      v548 = v624;
                      }
                    else {
                      const v615 = stdlib.sub(v495, v85);
                      const v625 = [v615, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:110:45:decimal', stdlib.UInt_max, 0), v83, v84];
                      v548 = v625;
                      }
                    }
                  else {
                    const v617 = stdlib.add(v85, v495);
                    const v626 = [v617, v86, v83, v84];
                    v548 = v626;
                    }
                  }
                }
              }
            const v627 = v548[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:435:23:array', stdlib.UInt_max, 0)];
            const v628 = v548[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:435:23:array', stdlib.UInt_max, 1)];
            const v629 = v548[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:435:23:array', stdlib.UInt_max, 2)];
            const v630 = v548[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:435:23:array', stdlib.UInt_max, 3)];
            let v631;
            const v632 = stdlib.lt(stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:67:9:decimal', stdlib.UInt_max, 0), v498);
            const v633 = stdlib.lt(v498, v94);
            const v634 = v632 ? v633 : false;
            if (v634) {
              const v702 = [v627, v628, v629, v630];
              v631 = v702;
              }
            else {
              const v642 = stdlib.eq(v498, v531);
              if (v642) {
                const v703 = [v627, v628, v629, v630];
                v631 = v703;
                }
              else {
                const v650 = stdlib.gt(v499, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:80:57:decimal', stdlib.UInt_max, 0));
                if (v650) {
                  const v658 = stdlib.gt(v630, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:85:83:decimal', stdlib.UInt_max, 0));
                  if (v658) {
                    const v666 = stdlib.add(v629, v498);
                    const v704 = [v627, v628, v666, v630];
                    v631 = v704;
                    }
                  else {
                    const v668 = stdlib.gt(v629, v498);
                    if (v668) {
                      const v676 = stdlib.sub(v629, v498);
                      const v705 = [v627, v628, v676, v630];
                      v631 = v705;
                      }
                    else {
                      const v678 = stdlib.sub(v498, v629);
                      const v706 = [v627, v628, v678, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:96:59:decimal', stdlib.UInt_max, 1)];
                      v631 = v706;
                      }
                    }
                  }
                else {
                  const v680 = stdlib.gt(v628, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:100:81:decimal', stdlib.UInt_max, 0));
                  if (v680) {
                    const v688 = stdlib.gt(v627, v498);
                    if (v688) {
                      const v696 = stdlib.sub(v627, v498);
                      const v707 = [v696, v628, v629, v630];
                      v631 = v707;
                      }
                    else {
                      const v698 = stdlib.sub(v498, v627);
                      const v708 = [v698, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:110:45:decimal', stdlib.UInt_max, 0), v629, v630];
                      v631 = v708;
                      }
                    }
                  else {
                    const v700 = stdlib.add(v627, v498);
                    const v709 = [v700, v628, v629, v630];
                    v631 = v709;
                    }
                  }
                }
              }
            const v710 = v631[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:436:23:array', stdlib.UInt_max, 0)];
            const v711 = v631[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:436:23:array', stdlib.UInt_max, 1)];
            const v712 = v631[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:436:23:array', stdlib.UInt_max, 2)];
            const v713 = v631[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:436:23:array', stdlib.UInt_max, 3)];
            let v714;
            const v715 = stdlib.lt(stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:67:9:decimal', stdlib.UInt_max, 0), v501);
            const v716 = stdlib.lt(v501, v94);
            const v717 = v715 ? v716 : false;
            if (v717) {
              const v785 = [v710, v711, v712, v713];
              v714 = v785;
              }
            else {
              const v725 = stdlib.eq(v501, v534);
              if (v725) {
                const v786 = [v710, v711, v712, v713];
                v714 = v786;
                }
              else {
                const v733 = stdlib.gt(v502, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:80:57:decimal', stdlib.UInt_max, 0));
                if (v733) {
                  const v741 = stdlib.gt(v713, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:85:83:decimal', stdlib.UInt_max, 0));
                  if (v741) {
                    const v749 = stdlib.add(v712, v501);
                    const v787 = [v710, v711, v749, v713];
                    v714 = v787;
                    }
                  else {
                    const v751 = stdlib.gt(v712, v501);
                    if (v751) {
                      const v759 = stdlib.sub(v712, v501);
                      const v788 = [v710, v711, v759, v713];
                      v714 = v788;
                      }
                    else {
                      const v761 = stdlib.sub(v501, v712);
                      const v789 = [v710, v711, v761, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:96:59:decimal', stdlib.UInt_max, 1)];
                      v714 = v789;
                      }
                    }
                  }
                else {
                  const v763 = stdlib.gt(v711, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:100:81:decimal', stdlib.UInt_max, 0));
                  if (v763) {
                    const v771 = stdlib.gt(v710, v501);
                    if (v771) {
                      const v779 = stdlib.sub(v710, v501);
                      const v790 = [v779, v711, v712, v713];
                      v714 = v790;
                      }
                    else {
                      const v781 = stdlib.sub(v501, v710);
                      const v791 = [v781, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:110:45:decimal', stdlib.UInt_max, 0), v712, v713];
                      v714 = v791;
                      }
                    }
                  else {
                    const v783 = stdlib.add(v710, v501);
                    const v792 = [v783, v711, v712, v713];
                    v714 = v792;
                    }
                  }
                }
              }
            const v793 = v714[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:437:23:array', stdlib.UInt_max, 0)];
            const v794 = v714[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:437:23:array', stdlib.UInt_max, 1)];
            const v795 = v714[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:437:23:array', stdlib.UInt_max, 2)];
            const v796 = v714[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:437:23:array', stdlib.UInt_max, 3)];
            let v797;
            const v798 = stdlib.gt(v504, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:129:18:decimal', stdlib.UInt_max, 0));
            if (v798) {
              const v799 = stdlib.add(v504, v537);
              const v800 = stdlib.gt(v89, v52);
              if (v800) {
                const v801 = stdlib.sub(v89, v52);
                const v802 = stdlib.add(v801, v799);
                const v810 = [v802];
                v797 = v810;
                }
              else {
                const v804 = stdlib.add(v89, v799);
                const v811 = [v804];
                v797 = v811;
                }
              }
            else {
              const v806 = stdlib.gt(v89, v52);
              if (v806) {
                const v807 = stdlib.sub(v89, v52);
                const v812 = [v807];
                v797 = v812;
                }
              else {
                const v813 = [v89];
                v797 = v813;
                }
              }
            const v814 = v797[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:439:23:array', stdlib.UInt_max, 0)];
            const v815 = stdlib.add(v91, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:442:145:decimal', stdlib.UInt_max, 1));
            const cv83 = v795;
            const cv84 = v796;
            const cv85 = v793;
            const cv86 = v794;
            const cv89 = v814;
            const cv90 = v90;
            const cv91 = v815;
            const cv92 = v92;
            const cv93 = v93;
            const cv94 = v94;
            const cv992 = v541;
            
            v83 = cv83;
            v84 = cv84;
            v85 = cv85;
            v86 = cv86;
            v89 = cv89;
            v90 = cv90;
            v91 = cv91;
            v92 = cv92;
            v93 = cv93;
            v94 = cv94;
            v992 = cv992;
            
            continue;
            
            }
          else {
            stdlib.protect(ctc1, await interact.currentDrawCardAction(), {
              at: './src/rsh/battleRecord.rsh:314:51:application',
              fs: ['at ./src/rsh/battleRecord.rsh:312:23:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:312:27:function exp)'],
              msg: 'currentDrawCardAction',
              who: 'Oneself'
              });
            const v133 = stdlib.protect(ctc13, await interact.currentUseCardAction(), {
              at: './src/rsh/battleRecord.rsh:315:212:application',
              fs: ['at ./src/rsh/battleRecord.rsh:312:23:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:312:27:function exp)'],
              msg: 'currentUseCardAction',
              who: 'Oneself'
              });
            const v134 = v133[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:315:27:array', stdlib.UInt_max, 0)];
            const v135 = v133[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:315:27:array', stdlib.UInt_max, 1)];
            const v136 = v133[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:315:27:array', stdlib.UInt_max, 2)];
            const v137 = v133[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:315:27:array', stdlib.UInt_max, 3)];
            const v138 = v133[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:315:27:array', stdlib.UInt_max, 4)];
            const v139 = v133[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:315:27:array', stdlib.UInt_max, 5)];
            const v140 = v133[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:315:27:array', stdlib.UInt_max, 6)];
            const v141 = v133[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:315:27:array', stdlib.UInt_max, 7)];
            const v142 = v133[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:315:27:array', stdlib.UInt_max, 8)];
            const v143 = v133[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:315:27:array', stdlib.UInt_max, 9)];
            const v144 = v133[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:315:27:array', stdlib.UInt_max, 10)];
            const v145 = v133[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:315:27:array', stdlib.UInt_max, 11)];
            const v146 = v133[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:315:27:array', stdlib.UInt_max, 12)];
            
            const txn5 = await (ctc.sendrecv(14, 13, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:335:19:dot', stdlib.UInt_max, 17), [ctc6, ctc0, ctc6, ctc2, ctc0, ctc2, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0], [v23, v24, v31, v51, v52, v72, v73, v83, v84, v85, v86, v89, v90, v91, v92, v93, v94, v992, v134, v135, v136, v137, v138, v139, v140, v141, v142, v143, v144, v145, v146], [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:decimal', stdlib.UInt_max, 0), []], [ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0], true, true, false, (async (txn5) => {
              const sim_r = { txns: [], mapRefs: [], mapsPrev: [], mapsNext: [] };
              
              sim_r.prevSt = stdlib.digest(ctc7, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:335:19:dot', stdlib.UInt_max, 18), v23, v24, v31, v51, v52, v72, v73, v83, v84, v85, v86, v89, v90, v91, v92, v93, v94, v992]);
              sim_r.prevSt_noPrevTime = stdlib.digest(ctc8, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:335:19:dot', stdlib.UInt_max, 18), v23, v24, v31, v51, v52, v72, v73, v83, v84, v85, v86, v89, v90, v91, v92, v93, v94]);
              const [v148, v149, v150, v151, v152, v153, v154, v155, v156, v157, v158, v159, v160] = txn5.data;
              const v163 = txn5.time;
              const v147 = txn5.from;
              
              sim_r.txns.push({
                amt: stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:decimal', stdlib.UInt_max, 0),
                kind: 'to',
                tok: undefined
                });
              const v162 = stdlib.addressEq(v23, v147);
              stdlib.assert(v162, {
                at: './src/rsh/battleRecord.rsh:335:19:dot',
                fs: [],
                msg: 'sender correct',
                who: 'Oneself'
                });
              
              sim_r.nextSt = stdlib.digest(ctc11, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:337:25:after expr stmt semicolon', stdlib.UInt_max, 19), v23, v24, v31, v51, v52, v72, v73, v83, v84, v85, v86, v89, v90, v91, v92, v93, v94, v149, v150, v152, v153, v155, v156, v158, v163]);
              sim_r.nextSt_noTime = stdlib.digest(ctc12, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:337:25:after expr stmt semicolon', stdlib.UInt_max, 19), v23, v24, v31, v51, v52, v72, v73, v83, v84, v85, v86, v89, v90, v91, v92, v93, v94, v149, v150, v152, v153, v155, v156, v158]);
              sim_r.view = [ctc5, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:337:25:after expr stmt semicolon', stdlib.UInt_max, 0)]];
              sim_r.isHalt = false;
              
              return sim_r;
              })));
            const [v148, v149, v150, v151, v152, v153, v154, v155, v156, v157, v158, v159, v160] = txn5.data;
            const v163 = txn5.time;
            const v147 = txn5.from;
            ;
            const v162 = stdlib.addressEq(v23, v147);
            stdlib.assert(v162, {
              at: './src/rsh/battleRecord.rsh:335:19:dot',
              fs: [],
              msg: 'sender correct',
              who: 'Oneself'
              });
            stdlib.protect(ctc1, await interact.log('A公布使用卡的情况了'), {
              at: './src/rsh/battleRecord.rsh:336:31:application',
              fs: ['at ./src/rsh/battleRecord.rsh:336:31:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:336:31:function exp)', 'at ./src/rsh/battleRecord.rsh:336:31:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:336:31:function exp)'],
              msg: 'log',
              who: 'Oneself'
              });
            
            const txn6 = await (ctc.recv(15, 12, [ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0], false, false));
            const [v187, v188, v189, v190, v191, v192, v193, v194, v195, v196, v197, v198] = txn6.data;
            const v201 = txn6.time;
            const v186 = txn6.from;
            ;
            const v200 = stdlib.addressEq(v31, v186);
            stdlib.assert(v200, {
              at: './src/rsh/battleRecord.rsh:361:19:dot',
              fs: [],
              msg: 'sender correct',
              who: 'Oneself'
              });
            stdlib.protect(ctc1, await interact.tellwaitplayerUseCardResult(v187, v190, v193, v196), {
              at: './src/rsh/battleRecord.rsh:364:55:application',
              fs: ['at ./src/rsh/battleRecord.rsh:364:55:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:364:55:function exp)', 'at ./src/rsh/battleRecord.rsh:364:55:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:364:55:function exp)'],
              msg: 'tellwaitplayerUseCardResult',
              who: 'Oneself'
              });
            
            let v208;
            const v209 = stdlib.lt(stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:67:9:decimal', stdlib.UInt_max, 0), v149);
            const v210 = stdlib.lt(v149, v94);
            const v211 = v209 ? v210 : false;
            if (v211) {
              stdlib.protect(ctc1, await interact.log('我在判断效果卡函数进入判断限制卡逻辑了'), {
                at: './src/rsh/battleRecord.rsh:69:25:application',
                fs: ['at ./src/rsh/battleRecord.rsh:69:25:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:69:25:function exp)', 'at ./src/rsh/battleRecord.rsh:69:25:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:69:25:function exp)', 'at ./src/rsh/battleRecord.rsh:366:60:application call to "isEffectCard" (defined at: ./src/rsh/battleRecord.rsh:64:74:function exp)'],
                msg: 'log',
                who: 'Oneself'
                });
              
              const v279 = [v83, v84, v85, v86];
              v208 = v279;
              }
            else {
              const v219 = stdlib.eq(v149, v188);
              if (v219) {
                stdlib.protect(ctc1, await interact.log('我在判断效果卡函数判断a1=b1'), {
                  at: './src/rsh/battleRecord.rsh:75:29:application',
                  fs: ['at ./src/rsh/battleRecord.rsh:75:29:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:75:29:function exp)', 'at ./src/rsh/battleRecord.rsh:75:29:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:75:29:function exp)', 'at ./src/rsh/battleRecord.rsh:366:60:application call to "isEffectCard" (defined at: ./src/rsh/battleRecord.rsh:64:74:function exp)'],
                  msg: 'log',
                  who: 'Oneself'
                  });
                
                const v280 = [v83, v84, v85, v86];
                v208 = v280;
                }
              else {
                const v227 = stdlib.gt(v150, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:80:57:decimal', stdlib.UInt_max, 0));
                stdlib.protect(ctc1, await interact.log('我在判断效果卡函数判断A2是否大于0', v227), {
                  at: './src/rsh/battleRecord.rsh:80:29:application',
                  fs: ['at ./src/rsh/battleRecord.rsh:80:29:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:80:29:function exp)', 'at ./src/rsh/battleRecord.rsh:80:29:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:80:29:function exp)', 'at ./src/rsh/battleRecord.rsh:366:60:application call to "isEffectCard" (defined at: ./src/rsh/battleRecord.rsh:64:74:function exp)'],
                  msg: 'log',
                  who: 'Oneself'
                  });
                
                if (v227) {
                  const v235 = stdlib.gt(v86, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:85:83:decimal', stdlib.UInt_max, 0));
                  stdlib.protect(ctc1, await interact.log('我在判断效果卡函数判断是debuff卡的情况下判断numb的状态是否大于0', v235), {
                    at: './src/rsh/battleRecord.rsh:85:33:application',
                    fs: ['at ./src/rsh/battleRecord.rsh:85:33:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:85:33:function exp)', 'at ./src/rsh/battleRecord.rsh:85:33:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:85:33:function exp)', 'at ./src/rsh/battleRecord.rsh:366:60:application call to "isEffectCard" (defined at: ./src/rsh/battleRecord.rsh:64:74:function exp)'],
                    msg: 'log',
                    who: 'Oneself'
                    });
                  
                  if (v235) {
                    const v243 = stdlib.add(v85, v149);
                    const v281 = [v83, v84, v243, v86];
                    v208 = v281;
                    }
                  else {
                    const v245 = stdlib.gt(v85, v149);
                    stdlib.protect(ctc1, await interact.log('我在判断效果卡函数判断是debuff卡的情况下判断到b的分数处于正分状态下开始判断够不够扣除分数,', v245), {
                      at: './src/rsh/battleRecord.rsh:90:37:application',
                      fs: ['at ./src/rsh/battleRecord.rsh:90:37:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:90:37:function exp)', 'at ./src/rsh/battleRecord.rsh:90:37:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:90:37:function exp)', 'at ./src/rsh/battleRecord.rsh:366:60:application call to "isEffectCard" (defined at: ./src/rsh/battleRecord.rsh:64:74:function exp)'],
                      msg: 'log',
                      who: 'Oneself'
                      });
                    
                    if (v245) {
                      const v253 = stdlib.sub(v85, v149);
                      const v282 = [v83, v84, v253, v86];
                      v208 = v282;
                      }
                    else {
                      const v255 = stdlib.sub(v149, v85);
                      const v283 = [v83, v84, v255, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:96:59:decimal', stdlib.UInt_max, 1)];
                      v208 = v283;
                      }
                    }
                  }
                else {
                  const v257 = stdlib.gt(v84, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:100:81:decimal', stdlib.UInt_max, 0));
                  stdlib.protect(ctc1, await interact.log('我在判断效果卡函数判断是buff卡的情况下判断numa的状态是否大于0', v257), {
                    at: './src/rsh/battleRecord.rsh:100:33:application',
                    fs: ['at ./src/rsh/battleRecord.rsh:100:33:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:100:33:function exp)', 'at ./src/rsh/battleRecord.rsh:100:33:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:100:33:function exp)', 'at ./src/rsh/battleRecord.rsh:366:60:application call to "isEffectCard" (defined at: ./src/rsh/battleRecord.rsh:64:74:function exp)'],
                    msg: 'log',
                    who: 'Oneself'
                    });
                  
                  if (v257) {
                    const v265 = stdlib.gt(v83, v149);
                    stdlib.protect(ctc1, await interact.log('我在判断效果卡函数判断是buff卡的情况下a的负分状态下，判断够不够扣除分数', v265), {
                      at: './src/rsh/battleRecord.rsh:105:37:application',
                      fs: ['at ./src/rsh/battleRecord.rsh:105:37:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:105:37:function exp)', 'at ./src/rsh/battleRecord.rsh:105:37:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:105:37:function exp)', 'at ./src/rsh/battleRecord.rsh:366:60:application call to "isEffectCard" (defined at: ./src/rsh/battleRecord.rsh:64:74:function exp)'],
                      msg: 'log',
                      who: 'Oneself'
                      });
                    
                    if (v265) {
                      const v273 = stdlib.sub(v83, v149);
                      const v284 = [v273, v84, v85, v86];
                      v208 = v284;
                      }
                    else {
                      const v275 = stdlib.sub(v149, v83);
                      const v285 = [v275, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:110:45:decimal', stdlib.UInt_max, 0), v85, v86];
                      v208 = v285;
                      }
                    }
                  else {
                    const v277 = stdlib.add(v83, v149);
                    const v286 = [v277, v84, v85, v86];
                    v208 = v286;
                    }
                  }
                }
              }
            const v287 = v208[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:366:23:array', stdlib.UInt_max, 0)];
            const v288 = v208[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:366:23:array', stdlib.UInt_max, 1)];
            const v289 = v208[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:366:23:array', stdlib.UInt_max, 2)];
            const v290 = v208[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:366:23:array', stdlib.UInt_max, 3)];
            let v291;
            const v292 = stdlib.lt(stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:67:9:decimal', stdlib.UInt_max, 0), v152);
            const v293 = stdlib.lt(v152, v94);
            const v294 = v292 ? v293 : false;
            if (v294) {
              stdlib.protect(ctc1, await interact.log('我在判断效果卡函数进入判断限制卡逻辑了'), {
                at: './src/rsh/battleRecord.rsh:69:25:application',
                fs: ['at ./src/rsh/battleRecord.rsh:69:25:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:69:25:function exp)', 'at ./src/rsh/battleRecord.rsh:69:25:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:69:25:function exp)', 'at ./src/rsh/battleRecord.rsh:368:60:application call to "isEffectCard" (defined at: ./src/rsh/battleRecord.rsh:64:74:function exp)'],
                msg: 'log',
                who: 'Oneself'
                });
              
              const v362 = [v287, v288, v289, v290];
              v291 = v362;
              }
            else {
              const v302 = stdlib.eq(v152, v191);
              if (v302) {
                stdlib.protect(ctc1, await interact.log('我在判断效果卡函数判断a1=b1'), {
                  at: './src/rsh/battleRecord.rsh:75:29:application',
                  fs: ['at ./src/rsh/battleRecord.rsh:75:29:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:75:29:function exp)', 'at ./src/rsh/battleRecord.rsh:75:29:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:75:29:function exp)', 'at ./src/rsh/battleRecord.rsh:368:60:application call to "isEffectCard" (defined at: ./src/rsh/battleRecord.rsh:64:74:function exp)'],
                  msg: 'log',
                  who: 'Oneself'
                  });
                
                const v363 = [v287, v288, v289, v290];
                v291 = v363;
                }
              else {
                const v310 = stdlib.gt(v153, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:80:57:decimal', stdlib.UInt_max, 0));
                stdlib.protect(ctc1, await interact.log('我在判断效果卡函数判断A2是否大于0', v310), {
                  at: './src/rsh/battleRecord.rsh:80:29:application',
                  fs: ['at ./src/rsh/battleRecord.rsh:80:29:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:80:29:function exp)', 'at ./src/rsh/battleRecord.rsh:80:29:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:80:29:function exp)', 'at ./src/rsh/battleRecord.rsh:368:60:application call to "isEffectCard" (defined at: ./src/rsh/battleRecord.rsh:64:74:function exp)'],
                  msg: 'log',
                  who: 'Oneself'
                  });
                
                if (v310) {
                  const v318 = stdlib.gt(v290, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:85:83:decimal', stdlib.UInt_max, 0));
                  stdlib.protect(ctc1, await interact.log('我在判断效果卡函数判断是debuff卡的情况下判断numb的状态是否大于0', v318), {
                    at: './src/rsh/battleRecord.rsh:85:33:application',
                    fs: ['at ./src/rsh/battleRecord.rsh:85:33:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:85:33:function exp)', 'at ./src/rsh/battleRecord.rsh:85:33:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:85:33:function exp)', 'at ./src/rsh/battleRecord.rsh:368:60:application call to "isEffectCard" (defined at: ./src/rsh/battleRecord.rsh:64:74:function exp)'],
                    msg: 'log',
                    who: 'Oneself'
                    });
                  
                  if (v318) {
                    const v326 = stdlib.add(v289, v152);
                    const v364 = [v287, v288, v326, v290];
                    v291 = v364;
                    }
                  else {
                    const v328 = stdlib.gt(v289, v152);
                    stdlib.protect(ctc1, await interact.log('我在判断效果卡函数判断是debuff卡的情况下判断到b的分数处于正分状态下开始判断够不够扣除分数,', v328), {
                      at: './src/rsh/battleRecord.rsh:90:37:application',
                      fs: ['at ./src/rsh/battleRecord.rsh:90:37:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:90:37:function exp)', 'at ./src/rsh/battleRecord.rsh:90:37:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:90:37:function exp)', 'at ./src/rsh/battleRecord.rsh:368:60:application call to "isEffectCard" (defined at: ./src/rsh/battleRecord.rsh:64:74:function exp)'],
                      msg: 'log',
                      who: 'Oneself'
                      });
                    
                    if (v328) {
                      const v336 = stdlib.sub(v289, v152);
                      const v365 = [v287, v288, v336, v290];
                      v291 = v365;
                      }
                    else {
                      const v338 = stdlib.sub(v152, v289);
                      const v366 = [v287, v288, v338, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:96:59:decimal', stdlib.UInt_max, 1)];
                      v291 = v366;
                      }
                    }
                  }
                else {
                  const v340 = stdlib.gt(v288, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:100:81:decimal', stdlib.UInt_max, 0));
                  stdlib.protect(ctc1, await interact.log('我在判断效果卡函数判断是buff卡的情况下判断numa的状态是否大于0', v340), {
                    at: './src/rsh/battleRecord.rsh:100:33:application',
                    fs: ['at ./src/rsh/battleRecord.rsh:100:33:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:100:33:function exp)', 'at ./src/rsh/battleRecord.rsh:100:33:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:100:33:function exp)', 'at ./src/rsh/battleRecord.rsh:368:60:application call to "isEffectCard" (defined at: ./src/rsh/battleRecord.rsh:64:74:function exp)'],
                    msg: 'log',
                    who: 'Oneself'
                    });
                  
                  if (v340) {
                    const v348 = stdlib.gt(v287, v152);
                    stdlib.protect(ctc1, await interact.log('我在判断效果卡函数判断是buff卡的情况下a的负分状态下，判断够不够扣除分数', v348), {
                      at: './src/rsh/battleRecord.rsh:105:37:application',
                      fs: ['at ./src/rsh/battleRecord.rsh:105:37:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:105:37:function exp)', 'at ./src/rsh/battleRecord.rsh:105:37:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:105:37:function exp)', 'at ./src/rsh/battleRecord.rsh:368:60:application call to "isEffectCard" (defined at: ./src/rsh/battleRecord.rsh:64:74:function exp)'],
                      msg: 'log',
                      who: 'Oneself'
                      });
                    
                    if (v348) {
                      const v356 = stdlib.sub(v287, v152);
                      const v367 = [v356, v288, v289, v290];
                      v291 = v367;
                      }
                    else {
                      const v358 = stdlib.sub(v152, v287);
                      const v368 = [v358, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:110:45:decimal', stdlib.UInt_max, 0), v289, v290];
                      v291 = v368;
                      }
                    }
                  else {
                    const v360 = stdlib.add(v287, v152);
                    const v369 = [v360, v288, v289, v290];
                    v291 = v369;
                    }
                  }
                }
              }
            const v370 = v291[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:368:23:array', stdlib.UInt_max, 0)];
            const v371 = v291[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:368:23:array', stdlib.UInt_max, 1)];
            const v372 = v291[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:368:23:array', stdlib.UInt_max, 2)];
            const v373 = v291[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:368:23:array', stdlib.UInt_max, 3)];
            let v374;
            const v375 = stdlib.lt(stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:67:9:decimal', stdlib.UInt_max, 0), v155);
            const v376 = stdlib.lt(v155, v94);
            const v377 = v375 ? v376 : false;
            if (v377) {
              stdlib.protect(ctc1, await interact.log('我在判断效果卡函数进入判断限制卡逻辑了'), {
                at: './src/rsh/battleRecord.rsh:69:25:application',
                fs: ['at ./src/rsh/battleRecord.rsh:69:25:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:69:25:function exp)', 'at ./src/rsh/battleRecord.rsh:69:25:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:69:25:function exp)', 'at ./src/rsh/battleRecord.rsh:371:60:application call to "isEffectCard" (defined at: ./src/rsh/battleRecord.rsh:64:74:function exp)'],
                msg: 'log',
                who: 'Oneself'
                });
              
              const v445 = [v370, v371, v372, v373];
              v374 = v445;
              }
            else {
              const v385 = stdlib.eq(v155, v194);
              if (v385) {
                stdlib.protect(ctc1, await interact.log('我在判断效果卡函数判断a1=b1'), {
                  at: './src/rsh/battleRecord.rsh:75:29:application',
                  fs: ['at ./src/rsh/battleRecord.rsh:75:29:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:75:29:function exp)', 'at ./src/rsh/battleRecord.rsh:75:29:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:75:29:function exp)', 'at ./src/rsh/battleRecord.rsh:371:60:application call to "isEffectCard" (defined at: ./src/rsh/battleRecord.rsh:64:74:function exp)'],
                  msg: 'log',
                  who: 'Oneself'
                  });
                
                const v446 = [v370, v371, v372, v373];
                v374 = v446;
                }
              else {
                const v393 = stdlib.gt(v156, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:80:57:decimal', stdlib.UInt_max, 0));
                stdlib.protect(ctc1, await interact.log('我在判断效果卡函数判断A2是否大于0', v393), {
                  at: './src/rsh/battleRecord.rsh:80:29:application',
                  fs: ['at ./src/rsh/battleRecord.rsh:80:29:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:80:29:function exp)', 'at ./src/rsh/battleRecord.rsh:80:29:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:80:29:function exp)', 'at ./src/rsh/battleRecord.rsh:371:60:application call to "isEffectCard" (defined at: ./src/rsh/battleRecord.rsh:64:74:function exp)'],
                  msg: 'log',
                  who: 'Oneself'
                  });
                
                if (v393) {
                  const v401 = stdlib.gt(v373, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:85:83:decimal', stdlib.UInt_max, 0));
                  stdlib.protect(ctc1, await interact.log('我在判断效果卡函数判断是debuff卡的情况下判断numb的状态是否大于0', v401), {
                    at: './src/rsh/battleRecord.rsh:85:33:application',
                    fs: ['at ./src/rsh/battleRecord.rsh:85:33:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:85:33:function exp)', 'at ./src/rsh/battleRecord.rsh:85:33:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:85:33:function exp)', 'at ./src/rsh/battleRecord.rsh:371:60:application call to "isEffectCard" (defined at: ./src/rsh/battleRecord.rsh:64:74:function exp)'],
                    msg: 'log',
                    who: 'Oneself'
                    });
                  
                  if (v401) {
                    const v409 = stdlib.add(v372, v155);
                    const v447 = [v370, v371, v409, v373];
                    v374 = v447;
                    }
                  else {
                    const v411 = stdlib.gt(v372, v155);
                    stdlib.protect(ctc1, await interact.log('我在判断效果卡函数判断是debuff卡的情况下判断到b的分数处于正分状态下开始判断够不够扣除分数,', v411), {
                      at: './src/rsh/battleRecord.rsh:90:37:application',
                      fs: ['at ./src/rsh/battleRecord.rsh:90:37:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:90:37:function exp)', 'at ./src/rsh/battleRecord.rsh:90:37:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:90:37:function exp)', 'at ./src/rsh/battleRecord.rsh:371:60:application call to "isEffectCard" (defined at: ./src/rsh/battleRecord.rsh:64:74:function exp)'],
                      msg: 'log',
                      who: 'Oneself'
                      });
                    
                    if (v411) {
                      const v419 = stdlib.sub(v372, v155);
                      const v448 = [v370, v371, v419, v373];
                      v374 = v448;
                      }
                    else {
                      const v421 = stdlib.sub(v155, v372);
                      const v449 = [v370, v371, v421, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:96:59:decimal', stdlib.UInt_max, 1)];
                      v374 = v449;
                      }
                    }
                  }
                else {
                  const v423 = stdlib.gt(v371, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:100:81:decimal', stdlib.UInt_max, 0));
                  stdlib.protect(ctc1, await interact.log('我在判断效果卡函数判断是buff卡的情况下判断numa的状态是否大于0', v423), {
                    at: './src/rsh/battleRecord.rsh:100:33:application',
                    fs: ['at ./src/rsh/battleRecord.rsh:100:33:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:100:33:function exp)', 'at ./src/rsh/battleRecord.rsh:100:33:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:100:33:function exp)', 'at ./src/rsh/battleRecord.rsh:371:60:application call to "isEffectCard" (defined at: ./src/rsh/battleRecord.rsh:64:74:function exp)'],
                    msg: 'log',
                    who: 'Oneself'
                    });
                  
                  if (v423) {
                    const v431 = stdlib.gt(v370, v155);
                    stdlib.protect(ctc1, await interact.log('我在判断效果卡函数判断是buff卡的情况下a的负分状态下，判断够不够扣除分数', v431), {
                      at: './src/rsh/battleRecord.rsh:105:37:application',
                      fs: ['at ./src/rsh/battleRecord.rsh:105:37:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:105:37:function exp)', 'at ./src/rsh/battleRecord.rsh:105:37:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:105:37:function exp)', 'at ./src/rsh/battleRecord.rsh:371:60:application call to "isEffectCard" (defined at: ./src/rsh/battleRecord.rsh:64:74:function exp)'],
                      msg: 'log',
                      who: 'Oneself'
                      });
                    
                    if (v431) {
                      const v439 = stdlib.sub(v370, v155);
                      const v450 = [v439, v371, v372, v373];
                      v374 = v450;
                      }
                    else {
                      const v441 = stdlib.sub(v155, v370);
                      const v451 = [v441, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:110:45:decimal', stdlib.UInt_max, 0), v372, v373];
                      v374 = v451;
                      }
                    }
                  else {
                    const v443 = stdlib.add(v370, v155);
                    const v452 = [v443, v371, v372, v373];
                    v374 = v452;
                    }
                  }
                }
              }
            const v453 = v374[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:371:23:array', stdlib.UInt_max, 0)];
            const v454 = v374[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:371:23:array', stdlib.UInt_max, 1)];
            const v455 = v374[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:371:23:array', stdlib.UInt_max, 2)];
            const v456 = v374[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:371:23:array', stdlib.UInt_max, 3)];
            let v457;
            const v458 = stdlib.gt(v158, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:129:18:decimal', stdlib.UInt_max, 0));
            if (v458) {
              const v459 = stdlib.add(v158, v197);
              const v460 = stdlib.gt(v89, v73);
              if (v460) {
                const v461 = stdlib.sub(v89, v73);
                const v462 = stdlib.add(v461, v459);
                const v470 = [v462];
                v457 = v470;
                }
              else {
                const v464 = stdlib.add(v89, v459);
                const v471 = [v464];
                v457 = v471;
                }
              }
            else {
              const v466 = stdlib.gt(v89, v73);
              if (v466) {
                const v467 = stdlib.sub(v89, v73);
                const v472 = [v467];
                v457 = v472;
                }
              else {
                const v473 = [v89];
                v457 = v473;
                }
              }
            const v474 = v457[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:373:23:array', stdlib.UInt_max, 0)];
            const v475 = stdlib.add(v91, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:375:145:decimal', stdlib.UInt_max, 1));
            const cv83 = v453;
            const cv84 = v454;
            const cv85 = v455;
            const cv86 = v456;
            const cv89 = v474;
            const cv90 = v90;
            const cv91 = v475;
            const cv92 = v92;
            const cv93 = v93;
            const cv94 = v94;
            const cv992 = v201;
            
            v83 = cv83;
            v84 = cv84;
            v85 = cv85;
            v86 = cv86;
            v89 = cv89;
            v90 = cv90;
            v91 = cv91;
            v92 = cv92;
            v93 = cv93;
            v94 = cv94;
            v992 = cv992;
            
            continue;
            
            }}
        const txn5 = await (ctc.sendrecv(10, 2, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:454:11:dot', stdlib.UInt_max, 9), [ctc6, ctc0, ctc6, ctc2, ctc2, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0], [v23, v24, v31, v51, v72, v83, v84, v85, v86, v992, v67, v65], [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:decimal', stdlib.UInt_max, 0), []], [ctc0, ctc0], true, true, false, (async (txn5) => {
          const sim_r = { txns: [], mapRefs: [], mapsPrev: [], mapsNext: [] };
          
          sim_r.prevSt = stdlib.digest(ctc9, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:454:11:dot', stdlib.UInt_max, 9), v23, v24, v31, v51, v72, v83, v84, v85, v86, v992]);
          sim_r.prevSt_noPrevTime = stdlib.digest(ctc10, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:454:11:dot', stdlib.UInt_max, 9), v23, v24, v31, v51, v72, v83, v84, v85, v86]);
          const [v820, v821] = txn5.data;
          const v824 = txn5.time;
          const v819 = txn5.from;
          
          sim_r.txns.push({
            amt: stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:decimal', stdlib.UInt_max, 0),
            kind: 'to',
            tok: undefined
            });
          const v823 = stdlib.addressEq(v23, v819);
          stdlib.assert(v823, {
            at: './src/rsh/battleRecord.rsh:454:11:dot',
            fs: [],
            msg: 'sender correct',
            who: 'Oneself'
            });
          const v826 = stdlib.digest(ctc3, [v820, v821]);
          const v827 = stdlib.digestEq(v72, v826);
          stdlib.assert(v827, {
            at: 'reach standard library:65:17:application',
            fs: ['at ./src/rsh/battleRecord.rsh:456:24:application call to "checkCommitment" (defined at: reach standard library:64:8:function exp)'],
            msg: null,
            who: 'Oneself'
            });
          sim_r.nextSt = stdlib.digest(ctc14, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:457:17:after expr stmt semicolon', stdlib.UInt_max, 10), v23, v24, v31, v51, v83, v84, v85, v86, v821, v824]);
          sim_r.nextSt_noTime = stdlib.digest(ctc15, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:457:17:after expr stmt semicolon', stdlib.UInt_max, 10), v23, v24, v31, v51, v83, v84, v85, v86, v821]);
          sim_r.view = [ctc5, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:457:17:after expr stmt semicolon', stdlib.UInt_max, 0)]];
          sim_r.isHalt = false;
          
          return sim_r;
          })));
        const [v820, v821] = txn5.data;
        const v824 = txn5.time;
        const v819 = txn5.from;
        ;
        const v823 = stdlib.addressEq(v23, v819);
        stdlib.assert(v823, {
          at: './src/rsh/battleRecord.rsh:454:11:dot',
          fs: [],
          msg: 'sender correct',
          who: 'Oneself'
          });
        const v826 = stdlib.digest(ctc3, [v820, v821]);
        const v827 = stdlib.digestEq(v72, v826);
        stdlib.assert(v827, {
          at: 'reach standard library:65:17:application',
          fs: ['at ./src/rsh/battleRecord.rsh:456:24:application call to "checkCommitment" (defined at: reach standard library:64:8:function exp)'],
          msg: null,
          who: 'Oneself'
          });
        const txn6 = await (ctc.recv(11, 2, [ctc0, ctc0], false, false));
        const [v833, v834] = txn6.data;
        const v837 = txn6.time;
        const v832 = txn6.from;
        ;
        const v836 = stdlib.addressEq(v31, v832);
        stdlib.assert(v836, {
          at: './src/rsh/battleRecord.rsh:462:11:dot',
          fs: [],
          msg: 'sender correct',
          who: 'Oneself'
          });
        const v839 = stdlib.digest(ctc3, [v833, v834]);
        const v840 = stdlib.digestEq(v51, v839);
        stdlib.assert(v840, {
          at: 'reach standard library:65:17:application',
          fs: ['at ./src/rsh/battleRecord.rsh:464:24:application call to "checkCommitment" (defined at: reach standard library:64:8:function exp)'],
          msg: null,
          who: 'Oneself'
          });
        let v842;
        const v844 = stdlib.eq(v84, v86);
        if (v844) {
          const v862 = stdlib.gt(v84, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:181:19:decimal', stdlib.UInt_max, 0));
          if (v862) {
            const v863 = stdlib.gt(v821, v83);
            if (v863) {
              const v864 = stdlib.sub(v821, v83);
              const v865 = stdlib.gt(v834, v85);
              if (v865) {
                const v866 = stdlib.sub(v834, v85);
                const v883 = [v864, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:188:35:decimal', stdlib.UInt_max, 0), v866, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:188:44:decimal', stdlib.UInt_max, 0)];
                v842 = v883;
                }
              else {
                const v868 = stdlib.sub(v85, v834);
                const v884 = [v864, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:191:35:decimal', stdlib.UInt_max, 0), v868, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:191:44:decimal', stdlib.UInt_max, 1)];
                v842 = v884;
                }
              }
            else {
              const v870 = stdlib.sub(v83, v821);
              const v871 = stdlib.gt(v834, v85);
              if (v871) {
                const v872 = stdlib.sub(v834, v85);
                const v885 = [v870, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:197:35:decimal', stdlib.UInt_max, 1), v872, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:197:44:decimal', stdlib.UInt_max, 0)];
                v842 = v885;
                }
              else {
                const v874 = stdlib.sub(v85, v834);
                const v886 = [v870, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:200:35:decimal', stdlib.UInt_max, 1), v874, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:200:44:decimal', stdlib.UInt_max, 1)];
                v842 = v886;
                }
              }
            }
          else {
            const v876 = stdlib.add(v83, v821);
            const v877 = stdlib.add(v85, v834);
            const v887 = [v876, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:207:27:decimal', stdlib.UInt_max, 0), v877, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:207:36:decimal', stdlib.UInt_max, 0)];
            v842 = v887;
            }
          }
        else {
          const v849 = stdlib.gt(v84, v86);
          if (v849) {
            const v850 = stdlib.add(v834, v85);
            const v851 = stdlib.gt(v821, v83);
            if (v851) {
              const v852 = stdlib.sub(v821, v83);
              const v879 = [v852, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:162:31:decimal', stdlib.UInt_max, 0), v850, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:162:40:decimal', stdlib.UInt_max, 0)];
              v842 = v879;
              }
            else {
              const v854 = stdlib.sub(v83, v821);
              const v880 = [v854, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:165:31:decimal', stdlib.UInt_max, 1), v850, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:165:40:decimal', stdlib.UInt_max, 0)];
              v842 = v880;
              }
            }
          else {
            const v856 = stdlib.add(v821, v83);
            const v857 = stdlib.gt(v834, v85);
            if (v857) {
              const v858 = stdlib.sub(v834, v85);
              const v881 = [v856, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:172:31:decimal', stdlib.UInt_max, 0), v858, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:172:40:decimal', stdlib.UInt_max, 0)];
              v842 = v881;
              }
            else {
              const v860 = stdlib.sub(v85, v834);
              const v882 = [v856, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:175:31:decimal', stdlib.UInt_max, 0), v860, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:175:40:decimal', stdlib.UInt_max, 1)];
              v842 = v882;
              }
            }
          }
        const v888 = v842[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:467:15:array', stdlib.UInt_max, 0)];
        const v889 = v842[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:467:15:array', stdlib.UInt_max, 1)];
        const v890 = v842[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:467:15:array', stdlib.UInt_max, 2)];
        const v891 = v842[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:467:15:array', stdlib.UInt_max, 3)];
        const v892 = stdlib.eq(v888, v890);
        const v893 = stdlib.eq(v889, v891);
        const v894 = v892 ? v893 : false;
        if (v894) {
          const v895 = stdlib.mul(stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:471:22:decimal', stdlib.UInt_max, 2), v24);
          ;
          stdlib.protect(ctc1, await interact.tellWhoWinHandle(v888, v889, v890, v891), {
            at: './src/rsh/battleRecord.rsh:493:38:application',
            fs: ['at ./src/rsh/battleRecord.rsh:492:13:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:492:25:function exp)'],
            msg: 'tellWhoWinHandle',
            who: 'Oneself'
            });
          
          return;}
        else {
          const v900 = stdlib.gt(v889, v891);
          if (v900) {
            const v901 = stdlib.mul(stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:475:26:decimal', stdlib.UInt_max, 2), v24);
            ;
            stdlib.protect(ctc1, await interact.tellWhoWinHandle(v888, v889, v890, v891), {
              at: './src/rsh/battleRecord.rsh:493:38:application',
              fs: ['at ./src/rsh/battleRecord.rsh:492:13:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:492:25:function exp)'],
              msg: 'tellWhoWinHandle',
              who: 'Oneself'
              });
            
            return;}
          else {
            if (v893) {
              const v907 = stdlib.gt(v889, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:478:34:decimal', stdlib.UInt_max, 0));
              if (v907) {
                const v908 = stdlib.mul(stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:479:34:decimal', stdlib.UInt_max, 2), v24);
                const v909 = stdlib.gt(v888, v890);
                const v910 = v909 ? v31 : v23;
                ;
                stdlib.protect(ctc1, await interact.tellWhoWinHandle(v888, v889, v890, v891), {
                  at: './src/rsh/battleRecord.rsh:493:38:application',
                  fs: ['at ./src/rsh/battleRecord.rsh:492:13:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:492:25:function exp)'],
                  msg: 'tellWhoWinHandle',
                  who: 'Oneself'
                  });
                
                return;}
              else {
                const v915 = stdlib.mul(stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:482:34:decimal', stdlib.UInt_max, 2), v24);
                const v916 = stdlib.gt(v888, v890);
                const v917 = v916 ? v23 : v31;
                ;
                stdlib.protect(ctc1, await interact.tellWhoWinHandle(v888, v889, v890, v891), {
                  at: './src/rsh/battleRecord.rsh:493:38:application',
                  fs: ['at ./src/rsh/battleRecord.rsh:492:13:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:492:25:function exp)'],
                  msg: 'tellWhoWinHandle',
                  who: 'Oneself'
                  });
                
                return;}}
            else {
              const v922 = stdlib.mul(stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:485:30:decimal', stdlib.UInt_max, 2), v24);
              ;
              stdlib.protect(ctc1, await interact.tellWhoWinHandle(v888, v889, v890, v891), {
                at: './src/rsh/battleRecord.rsh:493:38:application',
                fs: ['at ./src/rsh/battleRecord.rsh:492:13:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:492:25:function exp)'],
                msg: 'tellWhoWinHandle',
                who: 'Oneself'
                });
              
              return;}}}
        
        }
      }
    }
  
  
  };
export async function Opponent(ctc, interact) {
  if (ctc.sendrecv === undefined) {
    return Promise.reject(new Error(`The backend for Opponent expects to receive a contract as its first argument.`));}
  if (typeof(interact) !== 'object') {
    return Promise.reject(new Error(`The backend for Opponent expects to receive an interact object as its second argument.`));}
  const stdlib = ctc.stdlib;
  const ctc0 = stdlib.T_UInt;
  const ctc1 = stdlib.T_Null;
  const ctc2 = stdlib.T_Tuple([ctc0, ctc0]);
  const ctc3 = stdlib.T_Digest;
  const ctc4 = stdlib.T_Tuple([ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0]);
  const ctc5 = stdlib.T_Tuple([ctc0]);
  const ctc6 = stdlib.T_Address;
  const ctc7 = stdlib.T_Tuple([ctc0, ctc6, ctc0, ctc6, ctc3, ctc0, ctc3, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0]);
  const ctc8 = stdlib.T_Tuple([ctc0, ctc6, ctc0, ctc6, ctc3, ctc0, ctc3, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0]);
  const ctc9 = stdlib.T_Tuple([ctc0, ctc6, ctc0, ctc6, ctc3, ctc0, ctc3, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0]);
  const ctc10 = stdlib.T_Tuple([ctc0, ctc6, ctc0, ctc6, ctc3, ctc0, ctc3, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0]);
  const ctc11 = stdlib.T_Tuple([ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0]);
  const ctc12 = stdlib.T_Tuple([ctc0, ctc6, ctc0, ctc6, ctc3, ctc3, ctc0, ctc0, ctc0, ctc0, ctc0]);
  const ctc13 = stdlib.T_Tuple([ctc0, ctc6, ctc0, ctc6, ctc3, ctc3, ctc0, ctc0, ctc0, ctc0]);
  const ctc14 = stdlib.T_Tuple([]);
  const ctc15 = stdlib.T_Tuple([ctc0, ctc6, ctc0, ctc6, ctc3, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0]);
  const ctc16 = stdlib.T_Tuple([ctc0, ctc6, ctc0, ctc6, ctc3, ctc0, ctc0, ctc0, ctc0, ctc0]);
  const ctc17 = stdlib.T_Tuple([ctc0, ctc6, ctc0, ctc6, ctc0, ctc3, ctc0, ctc0]);
  const ctc18 = stdlib.T_Tuple([ctc0, ctc6, ctc0, ctc6, ctc0, ctc3, ctc0]);
  const ctc19 = stdlib.T_Tuple([ctc0, ctc6, ctc0, ctc6, ctc0, ctc0]);
  const ctc20 = stdlib.T_Tuple([ctc0, ctc6, ctc0, ctc6, ctc0]);
  const ctc21 = stdlib.T_Tuple([ctc0, ctc6, ctc0, ctc0]);
  const ctc22 = stdlib.T_Tuple([ctc0, ctc6, ctc0]);
  
  
  const v19 = await ctc.creationTime();
  const txn1 = await (ctc.recv(1, 1, [ctc0], false, false));
  const [v24] = txn1.data;
  const v27 = txn1.time;
  const v23 = txn1.from;
  ;
  stdlib.protect(ctc1, await interact.getTokenNumberHandle(v24), {
    at: './src/rsh/battleRecord.rsh:239:42:application',
    fs: ['at ./src/rsh/battleRecord.rsh:237:15:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:237:19:function exp)'],
    msg: 'getTokenNumberHandle',
    who: 'Opponent'
    });
  
  const txn2 = await (ctc.sendrecv(2, 0, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:242:11:dot', stdlib.UInt_max, 2), [ctc6, ctc0, ctc0], [v23, v24, v27], [v24, []], [], true, true, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:54:22:decimal', stdlib.UInt_max, 10), (async (txn2) => {
    const sim_r = { txns: [], mapRefs: [], mapsPrev: [], mapsNext: [] };
    
    sim_r.prevSt = stdlib.digest(ctc21, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:242:11:dot', stdlib.UInt_max, 1), v23, v24, v27]);
    sim_r.prevSt_noPrevTime = stdlib.digest(ctc22, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:242:11:dot', stdlib.UInt_max, 1), v23, v24]);
    const [] = txn2.data;
    const v34 = txn2.time;
    const v31 = txn2.from;
    
    const v33 = stdlib.add(v24, v24);
    sim_r.txns.push({
      amt: v24,
      kind: 'to',
      tok: undefined
      });
    sim_r.nextSt = stdlib.digest(ctc19, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:244:17:after expr stmt semicolon', stdlib.UInt_max, 3), v23, v24, v31, v33, v34]);
    sim_r.nextSt_noTime = stdlib.digest(ctc20, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:244:17:after expr stmt semicolon', stdlib.UInt_max, 3), v23, v24, v31, v33]);
    sim_r.view = [ctc5, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:244:17:after expr stmt semicolon', stdlib.UInt_max, 0)]];
    sim_r.isHalt = false;
    
    return sim_r;
    })));
  if (txn2.didTimeout) {
    const txn3 = await (ctc.recv(3, 0, [], false, false));
    const [] = txn3.data;
    const v977 = txn3.time;
    const v974 = txn3.from;
    ;
    const v976 = stdlib.addressEq(v23, v974);
    stdlib.assert(v976, {
      at: 'reach standard library:209:7:dot',
      fs: ['at ./src/rsh/battleRecord.rsh:242:57:application call to "closeTo" (defined at: reach standard library:207:8:function exp)'],
      msg: 'sender correct',
      who: 'Opponent'
      });
    ;
    stdlib.protect(ctc1, await interact.payTimeoutHandle(), {
      at: './src/rsh/battleRecord.rsh:223:42:application',
      fs: ['at ./src/rsh/battleRecord.rsh:222:17:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:222:29:function exp)', 'at reach standard library:212:8:application call to "after" (defined at: ./src/rsh/battleRecord.rsh:221:37:function exp)', 'at ./src/rsh/battleRecord.rsh:242:57:application call to "closeTo" (defined at: reach standard library:207:8:function exp)'],
      msg: 'payTimeoutHandle',
      who: 'Opponent'
      });
    
    return;
    }
  else {
    const [] = txn2.data;
    const v34 = txn2.time;
    const v31 = txn2.from;
    const v33 = stdlib.add(v24, v24);
    ;
    stdlib.protect(ctc1, await interact.tellGameStartStatus(true), {
      at: './src/rsh/battleRecord.rsh:247:41:application',
      fs: ['at ./src/rsh/battleRecord.rsh:246:13:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:246:25:function exp)'],
      msg: 'tellGameStartStatus',
      who: 'Opponent'
      });
    
    const v44 = stdlib.protect(ctc0, await interact.getHideRoleHandle(), {
      at: './src/rsh/battleRecord.rsh:260:61:application',
      fs: ['at ./src/rsh/battleRecord.rsh:259:15:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:259:19:function exp)'],
      msg: 'getHideRoleHandle',
      who: 'Opponent'
      });
    const v46 = stdlib.protect(ctc0, await interact.random(), {
      at: 'reach standard library:60:31:application',
      fs: ['at ./src/rsh/battleRecord.rsh:261:54:application call to "makeCommitment" (defined at: reach standard library:59:8:function exp)', 'at ./src/rsh/battleRecord.rsh:259:15:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:259:19:function exp)'],
      msg: 'random',
      who: 'Opponent'
      });
    const v47 = stdlib.digest(ctc2, [v46, v44]);
    const v49 = stdlib.protect(ctc0, await interact.setInitialHand(), {
      at: './src/rsh/battleRecord.rsh:263:62:application',
      fs: ['at ./src/rsh/battleRecord.rsh:259:15:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:259:19:function exp)'],
      msg: 'setInitialHand',
      who: 'Opponent'
      });
    
    const txn3 = await (ctc.sendrecv(4, 2, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:266:11:dot', stdlib.UInt_max, 4), [ctc6, ctc0, ctc6, ctc0, ctc0, ctc3, ctc0], [v23, v24, v31, v33, v34, v47, v49], [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:decimal', stdlib.UInt_max, 0), []], [ctc3, ctc0], true, true, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:57:23:decimal', stdlib.UInt_max, 5), (async (txn3) => {
      const sim_r = { txns: [], mapRefs: [], mapsPrev: [], mapsNext: [] };
      
      sim_r.prevSt = stdlib.digest(ctc19, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:266:11:dot', stdlib.UInt_max, 3), v23, v24, v31, v33, v34]);
      sim_r.prevSt_noPrevTime = stdlib.digest(ctc20, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:266:11:dot', stdlib.UInt_max, 3), v23, v24, v31, v33]);
      const [v51, v52] = txn3.data;
      const v55 = txn3.time;
      const v50 = txn3.from;
      
      sim_r.txns.push({
        amt: stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:decimal', stdlib.UInt_max, 0),
        kind: 'to',
        tok: undefined
        });
      const v54 = stdlib.addressEq(v31, v50);
      stdlib.assert(v54, {
        at: './src/rsh/battleRecord.rsh:266:11:dot',
        fs: [],
        msg: 'sender correct',
        who: 'Opponent'
        });
      sim_r.nextSt = stdlib.digest(ctc17, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:267:17:after expr stmt semicolon', stdlib.UInt_max, 5), v23, v24, v31, v33, v51, v52, v55]);
      sim_r.nextSt_noTime = stdlib.digest(ctc18, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:267:17:after expr stmt semicolon', stdlib.UInt_max, 5), v23, v24, v31, v33, v51, v52]);
      sim_r.view = [ctc5, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:267:17:after expr stmt semicolon', stdlib.UInt_max, 0)]];
      sim_r.isHalt = false;
      
      return sim_r;
      })));
    if (txn3.didTimeout) {
      const txn4 = await (ctc.recv(5, 0, [], false, false));
      const [] = txn4.data;
      const v958 = txn4.time;
      const v955 = txn4.from;
      ;
      const v957 = stdlib.addressEq(v23, v955);
      stdlib.assert(v957, {
        at: 'reach standard library:209:7:dot',
        fs: ['at ./src/rsh/battleRecord.rsh:266:72:application call to "closeTo" (defined at: reach standard library:207:8:function exp)'],
        msg: 'sender correct',
        who: 'Opponent'
        });
      ;
      stdlib.protect(ctc1, await interact.waitTimeoutHandle(), {
        at: './src/rsh/battleRecord.rsh:252:43:application',
        fs: ['at ./src/rsh/battleRecord.rsh:251:17:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:251:29:function exp)', 'at reach standard library:212:8:application call to "after" (defined at: ./src/rsh/battleRecord.rsh:250:38:function exp)', 'at ./src/rsh/battleRecord.rsh:266:72:application call to "closeTo" (defined at: reach standard library:207:8:function exp)'],
        msg: 'waitTimeoutHandle',
        who: 'Opponent'
        });
      
      return;
      }
    else {
      const [v51, v52] = txn3.data;
      const v55 = txn3.time;
      const v50 = txn3.from;
      ;
      const v54 = stdlib.addressEq(v31, v50);
      stdlib.assert(v54, {
        at: './src/rsh/battleRecord.rsh:266:11:dot',
        fs: [],
        msg: 'sender correct',
        who: 'Opponent'
        });
      stdlib.protect(ctc1, await interact.log('B提交了隐藏数据'), {
        at: './src/rsh/battleRecord.rsh:269:23:application',
        fs: ['at ./src/rsh/battleRecord.rsh:269:23:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:269:23:function exp)', 'at ./src/rsh/battleRecord.rsh:269:23:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:269:23:function exp)'],
        msg: 'log',
        who: 'Opponent'
        });
      
      const txn4 = await (ctc.recv(6, 2, [ctc3, ctc0], false, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:57:23:decimal', stdlib.UInt_max, 5)));
      if (txn4.didTimeout) {
        const txn5 = await (ctc.sendrecv(7, 0, stdlib.checkedBigNumberify('reach standard library:209:7:dot', stdlib.UInt_max, 6), [ctc6, ctc0, ctc6, ctc0, ctc3, ctc0, ctc0], [v23, v24, v31, v33, v51, v52, v55], [stdlib.checkedBigNumberify('reach standard library:decimal', stdlib.UInt_max, 0), []], [], true, true, false, (async (txn5) => {
          const sim_r = { txns: [], mapRefs: [], mapsPrev: [], mapsNext: [] };
          
          sim_r.prevSt = stdlib.digest(ctc17, [stdlib.checkedBigNumberify('reach standard library:209:7:dot', stdlib.UInt_max, 5), v23, v24, v31, v33, v51, v52, v55]);
          sim_r.prevSt_noPrevTime = stdlib.digest(ctc18, [stdlib.checkedBigNumberify('reach standard library:209:7:dot', stdlib.UInt_max, 5), v23, v24, v31, v33, v51, v52]);
          const [] = txn5.data;
          const v939 = txn5.time;
          const v936 = txn5.from;
          
          sim_r.txns.push({
            amt: stdlib.checkedBigNumberify('reach standard library:decimal', stdlib.UInt_max, 0),
            kind: 'to',
            tok: undefined
            });
          const v938 = stdlib.addressEq(v31, v936);
          stdlib.assert(v938, {
            at: 'reach standard library:209:7:dot',
            fs: ['at ./src/rsh/battleRecord.rsh:277:72:application call to "closeTo" (defined at: reach standard library:207:8:function exp)'],
            msg: 'sender correct',
            who: 'Opponent'
            });
          sim_r.txns.push({
            amt: v33,
            kind: 'from',
            to: v31,
            tok: undefined
            });
          sim_r.txns.push({
            kind: 'halt',
            tok: undefined
            })
          sim_r.nextSt = stdlib.digest(ctc14, []);
          sim_r.nextSt_noTime = stdlib.digest(ctc14, []);
          sim_r.view = [ctc14, []];
          sim_r.isHalt = true;
          
          return sim_r;
          })));
        const [] = txn5.data;
        const v939 = txn5.time;
        const v936 = txn5.from;
        ;
        const v938 = stdlib.addressEq(v31, v936);
        stdlib.assert(v938, {
          at: 'reach standard library:209:7:dot',
          fs: ['at ./src/rsh/battleRecord.rsh:277:72:application call to "closeTo" (defined at: reach standard library:207:8:function exp)'],
          msg: 'sender correct',
          who: 'Opponent'
          });
        ;
        stdlib.protect(ctc1, await interact.waitTimeoutHandle(), {
          at: './src/rsh/battleRecord.rsh:252:43:application',
          fs: ['at ./src/rsh/battleRecord.rsh:251:17:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:251:29:function exp)', 'at reach standard library:212:8:application call to "after" (defined at: ./src/rsh/battleRecord.rsh:250:38:function exp)', 'at ./src/rsh/battleRecord.rsh:277:72:application call to "closeTo" (defined at: reach standard library:207:8:function exp)'],
          msg: 'waitTimeoutHandle',
          who: 'Opponent'
          });
        
        return;
        }
      else {
        const [v72, v73] = txn4.data;
        const v76 = txn4.time;
        const v71 = txn4.from;
        ;
        const v75 = stdlib.addressEq(v23, v71);
        stdlib.assert(v75, {
          at: './src/rsh/battleRecord.rsh:277:11:dot',
          fs: [],
          msg: 'sender correct',
          who: 'Opponent'
          });
        let v83 = stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:294:162:decimal', stdlib.UInt_max, 0);
        let v84 = stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:294:165:decimal', stdlib.UInt_max, 0);
        let v85 = stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:294:168:decimal', stdlib.UInt_max, 0);
        let v86 = stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:294:171:decimal', stdlib.UInt_max, 0);
        let v89 = stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:294:156:decimal', stdlib.UInt_max, 0);
        let v90 = stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:294:177:decimal', stdlib.UInt_max, 0);
        let v91 = stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:294:159:decimal', stdlib.UInt_max, 1);
        let v92 = stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:294:174:decimal', stdlib.UInt_max, 2);
        let v93 = stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:294:153:decimal', stdlib.UInt_max, 1);
        let v94 = stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:294:186:decimal', stdlib.UInt_max, 50);
        let v992 = v76;
        
        while ((() => {
          const v101 = stdlib.gt(v93, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:298:25:decimal', stdlib.UInt_max, 0));
          const v103 = stdlib.eq(v89, v73);
          const v105 = v103 ? false : true;
          const v108 = v101 ? v105 : false;
          const v110 = stdlib.eq(v89, v52);
          const v112 = v110 ? false : true;
          const v115 = v108 ? v112 : false;
          
          return v115;})()) {
          stdlib.protect(ctc1, await interact.tellEndRoundStatus(v83, v84, v85, v86, v91, v89), {
            at: './src/rsh/battleRecord.rsh:302:44:application',
            fs: ['at ./src/rsh/battleRecord.rsh:301:17:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:301:29:function exp)'],
            msg: 'tellEndRoundStatus',
            who: 'Opponent'
            });
          
          const v123 = stdlib.mod(v91, v92);
          const v125 = stdlib.eq(v123, v90);
          if (v125) {
            stdlib.protect(ctc1, await interact.currentDrawCardAction(), {
              at: './src/rsh/battleRecord.rsh:382:51:application',
              fs: ['at ./src/rsh/battleRecord.rsh:380:23:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:380:27:function exp)'],
              msg: 'currentDrawCardAction',
              who: 'Opponent'
              });
            const v479 = stdlib.protect(ctc4, await interact.currentUseCardAction(), {
              at: './src/rsh/battleRecord.rsh:383:212:application',
              fs: ['at ./src/rsh/battleRecord.rsh:380:23:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:380:27:function exp)'],
              msg: 'currentUseCardAction',
              who: 'Opponent'
              });
            const v480 = v479[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:383:27:array', stdlib.UInt_max, 0)];
            const v481 = v479[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:383:27:array', stdlib.UInt_max, 1)];
            const v482 = v479[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:383:27:array', stdlib.UInt_max, 2)];
            const v483 = v479[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:383:27:array', stdlib.UInt_max, 3)];
            const v484 = v479[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:383:27:array', stdlib.UInt_max, 4)];
            const v485 = v479[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:383:27:array', stdlib.UInt_max, 5)];
            const v486 = v479[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:383:27:array', stdlib.UInt_max, 6)];
            const v487 = v479[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:383:27:array', stdlib.UInt_max, 7)];
            const v488 = v479[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:383:27:array', stdlib.UInt_max, 8)];
            const v489 = v479[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:383:27:array', stdlib.UInt_max, 9)];
            const v490 = v479[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:383:27:array', stdlib.UInt_max, 10)];
            const v491 = v479[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:383:27:array', stdlib.UInt_max, 11)];
            const v492 = v479[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:383:27:array', stdlib.UInt_max, 12)];
            
            const txn5 = await (ctc.sendrecv(12, 13, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:402:19:dot', stdlib.UInt_max, 17), [ctc6, ctc0, ctc6, ctc3, ctc0, ctc3, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0], [v23, v24, v31, v51, v52, v72, v73, v83, v84, v85, v86, v89, v90, v91, v92, v93, v94, v992, v480, v481, v482, v483, v484, v485, v486, v487, v488, v489, v490, v491, v492], [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:decimal', stdlib.UInt_max, 0), []], [ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0], true, true, false, (async (txn5) => {
              const sim_r = { txns: [], mapRefs: [], mapsPrev: [], mapsNext: [] };
              
              sim_r.prevSt = stdlib.digest(ctc9, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:402:19:dot', stdlib.UInt_max, 16), v23, v24, v31, v51, v52, v72, v73, v83, v84, v85, v86, v89, v90, v91, v92, v93, v94, v992]);
              sim_r.prevSt_noPrevTime = stdlib.digest(ctc10, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:402:19:dot', stdlib.UInt_max, 16), v23, v24, v31, v51, v52, v72, v73, v83, v84, v85, v86, v89, v90, v91, v92, v93, v94]);
              const [v494, v495, v496, v497, v498, v499, v500, v501, v502, v503, v504, v505, v506] = txn5.data;
              const v509 = txn5.time;
              const v493 = txn5.from;
              
              sim_r.txns.push({
                amt: stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:decimal', stdlib.UInt_max, 0),
                kind: 'to',
                tok: undefined
                });
              const v508 = stdlib.addressEq(v31, v493);
              stdlib.assert(v508, {
                at: './src/rsh/battleRecord.rsh:402:19:dot',
                fs: [],
                msg: 'sender correct',
                who: 'Opponent'
                });
              sim_r.nextSt = stdlib.digest(ctc7, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:404:25:after expr stmt semicolon', stdlib.UInt_max, 17), v23, v24, v31, v51, v52, v72, v73, v83, v84, v85, v86, v89, v90, v91, v92, v93, v94, v495, v496, v498, v499, v501, v502, v504, v509]);
              sim_r.nextSt_noTime = stdlib.digest(ctc8, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:404:25:after expr stmt semicolon', stdlib.UInt_max, 17), v23, v24, v31, v51, v52, v72, v73, v83, v84, v85, v86, v89, v90, v91, v92, v93, v94, v495, v496, v498, v499, v501, v502, v504]);
              sim_r.view = [ctc5, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:404:25:after expr stmt semicolon', stdlib.UInt_max, 0)]];
              sim_r.isHalt = false;
              
              return sim_r;
              })));
            const [v494, v495, v496, v497, v498, v499, v500, v501, v502, v503, v504, v505, v506] = txn5.data;
            const v509 = txn5.time;
            const v493 = txn5.from;
            ;
            const v508 = stdlib.addressEq(v31, v493);
            stdlib.assert(v508, {
              at: './src/rsh/battleRecord.rsh:402:19:dot',
              fs: [],
              msg: 'sender correct',
              who: 'Opponent'
              });
            const txn6 = await (ctc.recv(13, 12, [ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0], false, false));
            const [v527, v528, v529, v530, v531, v532, v533, v534, v535, v536, v537, v538] = txn6.data;
            const v541 = txn6.time;
            const v526 = txn6.from;
            ;
            const v540 = stdlib.addressEq(v23, v526);
            stdlib.assert(v540, {
              at: './src/rsh/battleRecord.rsh:429:19:dot',
              fs: [],
              msg: 'sender correct',
              who: 'Opponent'
              });
            stdlib.protect(ctc1, await interact.tellwaitplayerUseCardResult(v527, v530, v533, v536), {
              at: './src/rsh/battleRecord.rsh:433:55:application',
              fs: ['at ./src/rsh/battleRecord.rsh:433:55:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:433:55:function exp)', 'at ./src/rsh/battleRecord.rsh:433:55:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:433:55:function exp)'],
              msg: 'tellwaitplayerUseCardResult',
              who: 'Opponent'
              });
            
            let v548;
            const v549 = stdlib.lt(stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:67:9:decimal', stdlib.UInt_max, 0), v495);
            const v550 = stdlib.lt(v495, v94);
            const v551 = v549 ? v550 : false;
            if (v551) {
              stdlib.protect(ctc1, await interact.log('我在判断效果卡函数进入判断限制卡逻辑了'), {
                at: './src/rsh/battleRecord.rsh:69:25:application',
                fs: ['at ./src/rsh/battleRecord.rsh:69:25:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:69:25:function exp)', 'at ./src/rsh/battleRecord.rsh:69:25:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:69:25:function exp)', 'at ./src/rsh/battleRecord.rsh:435:60:application call to "isEffectCard" (defined at: ./src/rsh/battleRecord.rsh:64:74:function exp)'],
                msg: 'log',
                who: 'Opponent'
                });
              
              const v619 = [v85, v86, v83, v84];
              v548 = v619;
              }
            else {
              const v559 = stdlib.eq(v495, v528);
              if (v559) {
                stdlib.protect(ctc1, await interact.log('我在判断效果卡函数判断a1=b1'), {
                  at: './src/rsh/battleRecord.rsh:75:29:application',
                  fs: ['at ./src/rsh/battleRecord.rsh:75:29:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:75:29:function exp)', 'at ./src/rsh/battleRecord.rsh:75:29:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:75:29:function exp)', 'at ./src/rsh/battleRecord.rsh:435:60:application call to "isEffectCard" (defined at: ./src/rsh/battleRecord.rsh:64:74:function exp)'],
                  msg: 'log',
                  who: 'Opponent'
                  });
                
                const v620 = [v85, v86, v83, v84];
                v548 = v620;
                }
              else {
                const v567 = stdlib.gt(v496, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:80:57:decimal', stdlib.UInt_max, 0));
                stdlib.protect(ctc1, await interact.log('我在判断效果卡函数判断A2是否大于0', v567), {
                  at: './src/rsh/battleRecord.rsh:80:29:application',
                  fs: ['at ./src/rsh/battleRecord.rsh:80:29:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:80:29:function exp)', 'at ./src/rsh/battleRecord.rsh:80:29:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:80:29:function exp)', 'at ./src/rsh/battleRecord.rsh:435:60:application call to "isEffectCard" (defined at: ./src/rsh/battleRecord.rsh:64:74:function exp)'],
                  msg: 'log',
                  who: 'Opponent'
                  });
                
                if (v567) {
                  const v575 = stdlib.gt(v84, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:85:83:decimal', stdlib.UInt_max, 0));
                  stdlib.protect(ctc1, await interact.log('我在判断效果卡函数判断是debuff卡的情况下判断numb的状态是否大于0', v575), {
                    at: './src/rsh/battleRecord.rsh:85:33:application',
                    fs: ['at ./src/rsh/battleRecord.rsh:85:33:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:85:33:function exp)', 'at ./src/rsh/battleRecord.rsh:85:33:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:85:33:function exp)', 'at ./src/rsh/battleRecord.rsh:435:60:application call to "isEffectCard" (defined at: ./src/rsh/battleRecord.rsh:64:74:function exp)'],
                    msg: 'log',
                    who: 'Opponent'
                    });
                  
                  if (v575) {
                    const v583 = stdlib.add(v83, v495);
                    const v621 = [v85, v86, v583, v84];
                    v548 = v621;
                    }
                  else {
                    const v585 = stdlib.gt(v83, v495);
                    stdlib.protect(ctc1, await interact.log('我在判断效果卡函数判断是debuff卡的情况下判断到b的分数处于正分状态下开始判断够不够扣除分数,', v585), {
                      at: './src/rsh/battleRecord.rsh:90:37:application',
                      fs: ['at ./src/rsh/battleRecord.rsh:90:37:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:90:37:function exp)', 'at ./src/rsh/battleRecord.rsh:90:37:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:90:37:function exp)', 'at ./src/rsh/battleRecord.rsh:435:60:application call to "isEffectCard" (defined at: ./src/rsh/battleRecord.rsh:64:74:function exp)'],
                      msg: 'log',
                      who: 'Opponent'
                      });
                    
                    if (v585) {
                      const v593 = stdlib.sub(v83, v495);
                      const v622 = [v85, v86, v593, v84];
                      v548 = v622;
                      }
                    else {
                      const v595 = stdlib.sub(v495, v83);
                      const v623 = [v85, v86, v595, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:96:59:decimal', stdlib.UInt_max, 1)];
                      v548 = v623;
                      }
                    }
                  }
                else {
                  const v597 = stdlib.gt(v86, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:100:81:decimal', stdlib.UInt_max, 0));
                  stdlib.protect(ctc1, await interact.log('我在判断效果卡函数判断是buff卡的情况下判断numa的状态是否大于0', v597), {
                    at: './src/rsh/battleRecord.rsh:100:33:application',
                    fs: ['at ./src/rsh/battleRecord.rsh:100:33:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:100:33:function exp)', 'at ./src/rsh/battleRecord.rsh:100:33:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:100:33:function exp)', 'at ./src/rsh/battleRecord.rsh:435:60:application call to "isEffectCard" (defined at: ./src/rsh/battleRecord.rsh:64:74:function exp)'],
                    msg: 'log',
                    who: 'Opponent'
                    });
                  
                  if (v597) {
                    const v605 = stdlib.gt(v85, v495);
                    stdlib.protect(ctc1, await interact.log('我在判断效果卡函数判断是buff卡的情况下a的负分状态下，判断够不够扣除分数', v605), {
                      at: './src/rsh/battleRecord.rsh:105:37:application',
                      fs: ['at ./src/rsh/battleRecord.rsh:105:37:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:105:37:function exp)', 'at ./src/rsh/battleRecord.rsh:105:37:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:105:37:function exp)', 'at ./src/rsh/battleRecord.rsh:435:60:application call to "isEffectCard" (defined at: ./src/rsh/battleRecord.rsh:64:74:function exp)'],
                      msg: 'log',
                      who: 'Opponent'
                      });
                    
                    if (v605) {
                      const v613 = stdlib.sub(v85, v495);
                      const v624 = [v613, v86, v83, v84];
                      v548 = v624;
                      }
                    else {
                      const v615 = stdlib.sub(v495, v85);
                      const v625 = [v615, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:110:45:decimal', stdlib.UInt_max, 0), v83, v84];
                      v548 = v625;
                      }
                    }
                  else {
                    const v617 = stdlib.add(v85, v495);
                    const v626 = [v617, v86, v83, v84];
                    v548 = v626;
                    }
                  }
                }
              }
            const v627 = v548[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:435:23:array', stdlib.UInt_max, 0)];
            const v628 = v548[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:435:23:array', stdlib.UInt_max, 1)];
            const v629 = v548[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:435:23:array', stdlib.UInt_max, 2)];
            const v630 = v548[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:435:23:array', stdlib.UInt_max, 3)];
            let v631;
            const v632 = stdlib.lt(stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:67:9:decimal', stdlib.UInt_max, 0), v498);
            const v633 = stdlib.lt(v498, v94);
            const v634 = v632 ? v633 : false;
            if (v634) {
              stdlib.protect(ctc1, await interact.log('我在判断效果卡函数进入判断限制卡逻辑了'), {
                at: './src/rsh/battleRecord.rsh:69:25:application',
                fs: ['at ./src/rsh/battleRecord.rsh:69:25:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:69:25:function exp)', 'at ./src/rsh/battleRecord.rsh:69:25:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:69:25:function exp)', 'at ./src/rsh/battleRecord.rsh:436:60:application call to "isEffectCard" (defined at: ./src/rsh/battleRecord.rsh:64:74:function exp)'],
                msg: 'log',
                who: 'Opponent'
                });
              
              const v702 = [v627, v628, v629, v630];
              v631 = v702;
              }
            else {
              const v642 = stdlib.eq(v498, v531);
              if (v642) {
                stdlib.protect(ctc1, await interact.log('我在判断效果卡函数判断a1=b1'), {
                  at: './src/rsh/battleRecord.rsh:75:29:application',
                  fs: ['at ./src/rsh/battleRecord.rsh:75:29:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:75:29:function exp)', 'at ./src/rsh/battleRecord.rsh:75:29:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:75:29:function exp)', 'at ./src/rsh/battleRecord.rsh:436:60:application call to "isEffectCard" (defined at: ./src/rsh/battleRecord.rsh:64:74:function exp)'],
                  msg: 'log',
                  who: 'Opponent'
                  });
                
                const v703 = [v627, v628, v629, v630];
                v631 = v703;
                }
              else {
                const v650 = stdlib.gt(v499, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:80:57:decimal', stdlib.UInt_max, 0));
                stdlib.protect(ctc1, await interact.log('我在判断效果卡函数判断A2是否大于0', v650), {
                  at: './src/rsh/battleRecord.rsh:80:29:application',
                  fs: ['at ./src/rsh/battleRecord.rsh:80:29:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:80:29:function exp)', 'at ./src/rsh/battleRecord.rsh:80:29:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:80:29:function exp)', 'at ./src/rsh/battleRecord.rsh:436:60:application call to "isEffectCard" (defined at: ./src/rsh/battleRecord.rsh:64:74:function exp)'],
                  msg: 'log',
                  who: 'Opponent'
                  });
                
                if (v650) {
                  const v658 = stdlib.gt(v630, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:85:83:decimal', stdlib.UInt_max, 0));
                  stdlib.protect(ctc1, await interact.log('我在判断效果卡函数判断是debuff卡的情况下判断numb的状态是否大于0', v658), {
                    at: './src/rsh/battleRecord.rsh:85:33:application',
                    fs: ['at ./src/rsh/battleRecord.rsh:85:33:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:85:33:function exp)', 'at ./src/rsh/battleRecord.rsh:85:33:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:85:33:function exp)', 'at ./src/rsh/battleRecord.rsh:436:60:application call to "isEffectCard" (defined at: ./src/rsh/battleRecord.rsh:64:74:function exp)'],
                    msg: 'log',
                    who: 'Opponent'
                    });
                  
                  if (v658) {
                    const v666 = stdlib.add(v629, v498);
                    const v704 = [v627, v628, v666, v630];
                    v631 = v704;
                    }
                  else {
                    const v668 = stdlib.gt(v629, v498);
                    stdlib.protect(ctc1, await interact.log('我在判断效果卡函数判断是debuff卡的情况下判断到b的分数处于正分状态下开始判断够不够扣除分数,', v668), {
                      at: './src/rsh/battleRecord.rsh:90:37:application',
                      fs: ['at ./src/rsh/battleRecord.rsh:90:37:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:90:37:function exp)', 'at ./src/rsh/battleRecord.rsh:90:37:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:90:37:function exp)', 'at ./src/rsh/battleRecord.rsh:436:60:application call to "isEffectCard" (defined at: ./src/rsh/battleRecord.rsh:64:74:function exp)'],
                      msg: 'log',
                      who: 'Opponent'
                      });
                    
                    if (v668) {
                      const v676 = stdlib.sub(v629, v498);
                      const v705 = [v627, v628, v676, v630];
                      v631 = v705;
                      }
                    else {
                      const v678 = stdlib.sub(v498, v629);
                      const v706 = [v627, v628, v678, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:96:59:decimal', stdlib.UInt_max, 1)];
                      v631 = v706;
                      }
                    }
                  }
                else {
                  const v680 = stdlib.gt(v628, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:100:81:decimal', stdlib.UInt_max, 0));
                  stdlib.protect(ctc1, await interact.log('我在判断效果卡函数判断是buff卡的情况下判断numa的状态是否大于0', v680), {
                    at: './src/rsh/battleRecord.rsh:100:33:application',
                    fs: ['at ./src/rsh/battleRecord.rsh:100:33:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:100:33:function exp)', 'at ./src/rsh/battleRecord.rsh:100:33:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:100:33:function exp)', 'at ./src/rsh/battleRecord.rsh:436:60:application call to "isEffectCard" (defined at: ./src/rsh/battleRecord.rsh:64:74:function exp)'],
                    msg: 'log',
                    who: 'Opponent'
                    });
                  
                  if (v680) {
                    const v688 = stdlib.gt(v627, v498);
                    stdlib.protect(ctc1, await interact.log('我在判断效果卡函数判断是buff卡的情况下a的负分状态下，判断够不够扣除分数', v688), {
                      at: './src/rsh/battleRecord.rsh:105:37:application',
                      fs: ['at ./src/rsh/battleRecord.rsh:105:37:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:105:37:function exp)', 'at ./src/rsh/battleRecord.rsh:105:37:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:105:37:function exp)', 'at ./src/rsh/battleRecord.rsh:436:60:application call to "isEffectCard" (defined at: ./src/rsh/battleRecord.rsh:64:74:function exp)'],
                      msg: 'log',
                      who: 'Opponent'
                      });
                    
                    if (v688) {
                      const v696 = stdlib.sub(v627, v498);
                      const v707 = [v696, v628, v629, v630];
                      v631 = v707;
                      }
                    else {
                      const v698 = stdlib.sub(v498, v627);
                      const v708 = [v698, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:110:45:decimal', stdlib.UInt_max, 0), v629, v630];
                      v631 = v708;
                      }
                    }
                  else {
                    const v700 = stdlib.add(v627, v498);
                    const v709 = [v700, v628, v629, v630];
                    v631 = v709;
                    }
                  }
                }
              }
            const v710 = v631[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:436:23:array', stdlib.UInt_max, 0)];
            const v711 = v631[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:436:23:array', stdlib.UInt_max, 1)];
            const v712 = v631[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:436:23:array', stdlib.UInt_max, 2)];
            const v713 = v631[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:436:23:array', stdlib.UInt_max, 3)];
            let v714;
            const v715 = stdlib.lt(stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:67:9:decimal', stdlib.UInt_max, 0), v501);
            const v716 = stdlib.lt(v501, v94);
            const v717 = v715 ? v716 : false;
            if (v717) {
              stdlib.protect(ctc1, await interact.log('我在判断效果卡函数进入判断限制卡逻辑了'), {
                at: './src/rsh/battleRecord.rsh:69:25:application',
                fs: ['at ./src/rsh/battleRecord.rsh:69:25:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:69:25:function exp)', 'at ./src/rsh/battleRecord.rsh:69:25:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:69:25:function exp)', 'at ./src/rsh/battleRecord.rsh:437:60:application call to "isEffectCard" (defined at: ./src/rsh/battleRecord.rsh:64:74:function exp)'],
                msg: 'log',
                who: 'Opponent'
                });
              
              const v785 = [v710, v711, v712, v713];
              v714 = v785;
              }
            else {
              const v725 = stdlib.eq(v501, v534);
              if (v725) {
                stdlib.protect(ctc1, await interact.log('我在判断效果卡函数判断a1=b1'), {
                  at: './src/rsh/battleRecord.rsh:75:29:application',
                  fs: ['at ./src/rsh/battleRecord.rsh:75:29:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:75:29:function exp)', 'at ./src/rsh/battleRecord.rsh:75:29:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:75:29:function exp)', 'at ./src/rsh/battleRecord.rsh:437:60:application call to "isEffectCard" (defined at: ./src/rsh/battleRecord.rsh:64:74:function exp)'],
                  msg: 'log',
                  who: 'Opponent'
                  });
                
                const v786 = [v710, v711, v712, v713];
                v714 = v786;
                }
              else {
                const v733 = stdlib.gt(v502, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:80:57:decimal', stdlib.UInt_max, 0));
                stdlib.protect(ctc1, await interact.log('我在判断效果卡函数判断A2是否大于0', v733), {
                  at: './src/rsh/battleRecord.rsh:80:29:application',
                  fs: ['at ./src/rsh/battleRecord.rsh:80:29:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:80:29:function exp)', 'at ./src/rsh/battleRecord.rsh:80:29:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:80:29:function exp)', 'at ./src/rsh/battleRecord.rsh:437:60:application call to "isEffectCard" (defined at: ./src/rsh/battleRecord.rsh:64:74:function exp)'],
                  msg: 'log',
                  who: 'Opponent'
                  });
                
                if (v733) {
                  const v741 = stdlib.gt(v713, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:85:83:decimal', stdlib.UInt_max, 0));
                  stdlib.protect(ctc1, await interact.log('我在判断效果卡函数判断是debuff卡的情况下判断numb的状态是否大于0', v741), {
                    at: './src/rsh/battleRecord.rsh:85:33:application',
                    fs: ['at ./src/rsh/battleRecord.rsh:85:33:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:85:33:function exp)', 'at ./src/rsh/battleRecord.rsh:85:33:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:85:33:function exp)', 'at ./src/rsh/battleRecord.rsh:437:60:application call to "isEffectCard" (defined at: ./src/rsh/battleRecord.rsh:64:74:function exp)'],
                    msg: 'log',
                    who: 'Opponent'
                    });
                  
                  if (v741) {
                    const v749 = stdlib.add(v712, v501);
                    const v787 = [v710, v711, v749, v713];
                    v714 = v787;
                    }
                  else {
                    const v751 = stdlib.gt(v712, v501);
                    stdlib.protect(ctc1, await interact.log('我在判断效果卡函数判断是debuff卡的情况下判断到b的分数处于正分状态下开始判断够不够扣除分数,', v751), {
                      at: './src/rsh/battleRecord.rsh:90:37:application',
                      fs: ['at ./src/rsh/battleRecord.rsh:90:37:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:90:37:function exp)', 'at ./src/rsh/battleRecord.rsh:90:37:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:90:37:function exp)', 'at ./src/rsh/battleRecord.rsh:437:60:application call to "isEffectCard" (defined at: ./src/rsh/battleRecord.rsh:64:74:function exp)'],
                      msg: 'log',
                      who: 'Opponent'
                      });
                    
                    if (v751) {
                      const v759 = stdlib.sub(v712, v501);
                      const v788 = [v710, v711, v759, v713];
                      v714 = v788;
                      }
                    else {
                      const v761 = stdlib.sub(v501, v712);
                      const v789 = [v710, v711, v761, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:96:59:decimal', stdlib.UInt_max, 1)];
                      v714 = v789;
                      }
                    }
                  }
                else {
                  const v763 = stdlib.gt(v711, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:100:81:decimal', stdlib.UInt_max, 0));
                  stdlib.protect(ctc1, await interact.log('我在判断效果卡函数判断是buff卡的情况下判断numa的状态是否大于0', v763), {
                    at: './src/rsh/battleRecord.rsh:100:33:application',
                    fs: ['at ./src/rsh/battleRecord.rsh:100:33:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:100:33:function exp)', 'at ./src/rsh/battleRecord.rsh:100:33:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:100:33:function exp)', 'at ./src/rsh/battleRecord.rsh:437:60:application call to "isEffectCard" (defined at: ./src/rsh/battleRecord.rsh:64:74:function exp)'],
                    msg: 'log',
                    who: 'Opponent'
                    });
                  
                  if (v763) {
                    const v771 = stdlib.gt(v710, v501);
                    stdlib.protect(ctc1, await interact.log('我在判断效果卡函数判断是buff卡的情况下a的负分状态下，判断够不够扣除分数', v771), {
                      at: './src/rsh/battleRecord.rsh:105:37:application',
                      fs: ['at ./src/rsh/battleRecord.rsh:105:37:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:105:37:function exp)', 'at ./src/rsh/battleRecord.rsh:105:37:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:105:37:function exp)', 'at ./src/rsh/battleRecord.rsh:437:60:application call to "isEffectCard" (defined at: ./src/rsh/battleRecord.rsh:64:74:function exp)'],
                      msg: 'log',
                      who: 'Opponent'
                      });
                    
                    if (v771) {
                      const v779 = stdlib.sub(v710, v501);
                      const v790 = [v779, v711, v712, v713];
                      v714 = v790;
                      }
                    else {
                      const v781 = stdlib.sub(v501, v710);
                      const v791 = [v781, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:110:45:decimal', stdlib.UInt_max, 0), v712, v713];
                      v714 = v791;
                      }
                    }
                  else {
                    const v783 = stdlib.add(v710, v501);
                    const v792 = [v783, v711, v712, v713];
                    v714 = v792;
                    }
                  }
                }
              }
            const v793 = v714[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:437:23:array', stdlib.UInt_max, 0)];
            const v794 = v714[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:437:23:array', stdlib.UInt_max, 1)];
            const v795 = v714[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:437:23:array', stdlib.UInt_max, 2)];
            const v796 = v714[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:437:23:array', stdlib.UInt_max, 3)];
            let v797;
            const v798 = stdlib.gt(v504, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:129:18:decimal', stdlib.UInt_max, 0));
            if (v798) {
              const v799 = stdlib.add(v504, v537);
              const v800 = stdlib.gt(v89, v52);
              if (v800) {
                const v801 = stdlib.sub(v89, v52);
                const v802 = stdlib.add(v801, v799);
                const v810 = [v802];
                v797 = v810;
                }
              else {
                const v804 = stdlib.add(v89, v799);
                const v811 = [v804];
                v797 = v811;
                }
              }
            else {
              const v806 = stdlib.gt(v89, v52);
              if (v806) {
                const v807 = stdlib.sub(v89, v52);
                const v812 = [v807];
                v797 = v812;
                }
              else {
                const v813 = [v89];
                v797 = v813;
                }
              }
            const v814 = v797[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:439:23:array', stdlib.UInt_max, 0)];
            const v815 = stdlib.add(v91, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:442:145:decimal', stdlib.UInt_max, 1));
            const cv83 = v795;
            const cv84 = v796;
            const cv85 = v793;
            const cv86 = v794;
            const cv89 = v814;
            const cv90 = v90;
            const cv91 = v815;
            const cv92 = v92;
            const cv93 = v93;
            const cv94 = v94;
            const cv992 = v541;
            
            v83 = cv83;
            v84 = cv84;
            v85 = cv85;
            v86 = cv86;
            v89 = cv89;
            v90 = cv90;
            v91 = cv91;
            v92 = cv92;
            v93 = cv93;
            v94 = cv94;
            v992 = cv992;
            
            continue;
            
            }
          else {
            const txn5 = await (ctc.recv(14, 13, [ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0], false, false));
            const [v148, v149, v150, v151, v152, v153, v154, v155, v156, v157, v158, v159, v160] = txn5.data;
            const v163 = txn5.time;
            const v147 = txn5.from;
            ;
            const v162 = stdlib.addressEq(v23, v147);
            stdlib.assert(v162, {
              at: './src/rsh/battleRecord.rsh:335:19:dot',
              fs: [],
              msg: 'sender correct',
              who: 'Opponent'
              });
            const v173 = stdlib.protect(ctc11, await interact.waitUseCardAction(v148, v151, v154, v157, v160), {
              at: './src/rsh/battleRecord.rsh:340:196:application',
              fs: ['at ./src/rsh/battleRecord.rsh:338:23:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:338:27:function exp)'],
              msg: 'waitUseCardAction',
              who: 'Opponent'
              });
            const v174 = v173[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:340:27:array', stdlib.UInt_max, 0)];
            const v175 = v173[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:340:27:array', stdlib.UInt_max, 1)];
            const v176 = v173[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:340:27:array', stdlib.UInt_max, 2)];
            const v177 = v173[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:340:27:array', stdlib.UInt_max, 3)];
            const v178 = v173[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:340:27:array', stdlib.UInt_max, 4)];
            const v179 = v173[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:340:27:array', stdlib.UInt_max, 5)];
            const v180 = v173[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:340:27:array', stdlib.UInt_max, 6)];
            const v181 = v173[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:340:27:array', stdlib.UInt_max, 7)];
            const v182 = v173[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:340:27:array', stdlib.UInt_max, 8)];
            const v183 = v173[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:340:27:array', stdlib.UInt_max, 9)];
            const v184 = v173[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:340:27:array', stdlib.UInt_max, 10)];
            const v185 = v173[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:340:27:array', stdlib.UInt_max, 11)];
            
            const txn6 = await (ctc.sendrecv(15, 12, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:361:19:dot', stdlib.UInt_max, 24), [ctc6, ctc0, ctc6, ctc3, ctc0, ctc3, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0], [v23, v24, v31, v51, v52, v72, v73, v83, v84, v85, v86, v89, v90, v91, v92, v93, v94, v149, v150, v152, v153, v155, v156, v158, v163, v174, v175, v176, v177, v178, v179, v180, v181, v182, v183, v184, v185], [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:decimal', stdlib.UInt_max, 0), []], [ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0], true, true, false, (async (txn6) => {
              const sim_r = { txns: [], mapRefs: [], mapsPrev: [], mapsNext: [] };
              
              sim_r.prevSt = stdlib.digest(ctc7, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:361:19:dot', stdlib.UInt_max, 19), v23, v24, v31, v51, v52, v72, v73, v83, v84, v85, v86, v89, v90, v91, v92, v93, v94, v149, v150, v152, v153, v155, v156, v158, v163]);
              sim_r.prevSt_noPrevTime = stdlib.digest(ctc8, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:361:19:dot', stdlib.UInt_max, 19), v23, v24, v31, v51, v52, v72, v73, v83, v84, v85, v86, v89, v90, v91, v92, v93, v94, v149, v150, v152, v153, v155, v156, v158]);
              const [v187, v188, v189, v190, v191, v192, v193, v194, v195, v196, v197, v198] = txn6.data;
              const v201 = txn6.time;
              const v186 = txn6.from;
              
              sim_r.txns.push({
                amt: stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:decimal', stdlib.UInt_max, 0),
                kind: 'to',
                tok: undefined
                });
              const v200 = stdlib.addressEq(v31, v186);
              stdlib.assert(v200, {
                at: './src/rsh/battleRecord.rsh:361:19:dot',
                fs: [],
                msg: 'sender correct',
                who: 'Opponent'
                });
              let v208;
              const v209 = stdlib.lt(stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:67:9:decimal', stdlib.UInt_max, 0), v149);
              const v210 = stdlib.lt(v149, v94);
              const v211 = v209 ? v210 : false;
              if (v211) {
                const v279 = [v83, v84, v85, v86];
                v208 = v279;
                }
              else {
                const v219 = stdlib.eq(v149, v188);
                if (v219) {
                  const v280 = [v83, v84, v85, v86];
                  v208 = v280;
                  }
                else {
                  const v227 = stdlib.gt(v150, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:80:57:decimal', stdlib.UInt_max, 0));
                  if (v227) {
                    const v235 = stdlib.gt(v86, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:85:83:decimal', stdlib.UInt_max, 0));
                    if (v235) {
                      const v243 = stdlib.add(v85, v149);
                      const v281 = [v83, v84, v243, v86];
                      v208 = v281;
                      }
                    else {
                      const v245 = stdlib.gt(v85, v149);
                      if (v245) {
                        const v253 = stdlib.sub(v85, v149);
                        const v282 = [v83, v84, v253, v86];
                        v208 = v282;
                        }
                      else {
                        const v255 = stdlib.sub(v149, v85);
                        const v283 = [v83, v84, v255, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:96:59:decimal', stdlib.UInt_max, 1)];
                        v208 = v283;
                        }
                      }
                    }
                  else {
                    const v257 = stdlib.gt(v84, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:100:81:decimal', stdlib.UInt_max, 0));
                    if (v257) {
                      const v265 = stdlib.gt(v83, v149);
                      if (v265) {
                        const v273 = stdlib.sub(v83, v149);
                        const v284 = [v273, v84, v85, v86];
                        v208 = v284;
                        }
                      else {
                        const v275 = stdlib.sub(v149, v83);
                        const v285 = [v275, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:110:45:decimal', stdlib.UInt_max, 0), v85, v86];
                        v208 = v285;
                        }
                      }
                    else {
                      const v277 = stdlib.add(v83, v149);
                      const v286 = [v277, v84, v85, v86];
                      v208 = v286;
                      }
                    }
                  }
                }
              const v287 = v208[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:366:23:array', stdlib.UInt_max, 0)];
              const v288 = v208[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:366:23:array', stdlib.UInt_max, 1)];
              const v289 = v208[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:366:23:array', stdlib.UInt_max, 2)];
              const v290 = v208[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:366:23:array', stdlib.UInt_max, 3)];
              let v291;
              const v292 = stdlib.lt(stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:67:9:decimal', stdlib.UInt_max, 0), v152);
              const v293 = stdlib.lt(v152, v94);
              const v294 = v292 ? v293 : false;
              if (v294) {
                const v362 = [v287, v288, v289, v290];
                v291 = v362;
                }
              else {
                const v302 = stdlib.eq(v152, v191);
                if (v302) {
                  const v363 = [v287, v288, v289, v290];
                  v291 = v363;
                  }
                else {
                  const v310 = stdlib.gt(v153, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:80:57:decimal', stdlib.UInt_max, 0));
                  if (v310) {
                    const v318 = stdlib.gt(v290, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:85:83:decimal', stdlib.UInt_max, 0));
                    if (v318) {
                      const v326 = stdlib.add(v289, v152);
                      const v364 = [v287, v288, v326, v290];
                      v291 = v364;
                      }
                    else {
                      const v328 = stdlib.gt(v289, v152);
                      if (v328) {
                        const v336 = stdlib.sub(v289, v152);
                        const v365 = [v287, v288, v336, v290];
                        v291 = v365;
                        }
                      else {
                        const v338 = stdlib.sub(v152, v289);
                        const v366 = [v287, v288, v338, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:96:59:decimal', stdlib.UInt_max, 1)];
                        v291 = v366;
                        }
                      }
                    }
                  else {
                    const v340 = stdlib.gt(v288, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:100:81:decimal', stdlib.UInt_max, 0));
                    if (v340) {
                      const v348 = stdlib.gt(v287, v152);
                      if (v348) {
                        const v356 = stdlib.sub(v287, v152);
                        const v367 = [v356, v288, v289, v290];
                        v291 = v367;
                        }
                      else {
                        const v358 = stdlib.sub(v152, v287);
                        const v368 = [v358, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:110:45:decimal', stdlib.UInt_max, 0), v289, v290];
                        v291 = v368;
                        }
                      }
                    else {
                      const v360 = stdlib.add(v287, v152);
                      const v369 = [v360, v288, v289, v290];
                      v291 = v369;
                      }
                    }
                  }
                }
              const v370 = v291[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:368:23:array', stdlib.UInt_max, 0)];
              const v371 = v291[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:368:23:array', stdlib.UInt_max, 1)];
              const v372 = v291[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:368:23:array', stdlib.UInt_max, 2)];
              const v373 = v291[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:368:23:array', stdlib.UInt_max, 3)];
              let v374;
              const v375 = stdlib.lt(stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:67:9:decimal', stdlib.UInt_max, 0), v155);
              const v376 = stdlib.lt(v155, v94);
              const v377 = v375 ? v376 : false;
              if (v377) {
                const v445 = [v370, v371, v372, v373];
                v374 = v445;
                }
              else {
                const v385 = stdlib.eq(v155, v194);
                if (v385) {
                  const v446 = [v370, v371, v372, v373];
                  v374 = v446;
                  }
                else {
                  const v393 = stdlib.gt(v156, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:80:57:decimal', stdlib.UInt_max, 0));
                  if (v393) {
                    const v401 = stdlib.gt(v373, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:85:83:decimal', stdlib.UInt_max, 0));
                    if (v401) {
                      const v409 = stdlib.add(v372, v155);
                      const v447 = [v370, v371, v409, v373];
                      v374 = v447;
                      }
                    else {
                      const v411 = stdlib.gt(v372, v155);
                      if (v411) {
                        const v419 = stdlib.sub(v372, v155);
                        const v448 = [v370, v371, v419, v373];
                        v374 = v448;
                        }
                      else {
                        const v421 = stdlib.sub(v155, v372);
                        const v449 = [v370, v371, v421, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:96:59:decimal', stdlib.UInt_max, 1)];
                        v374 = v449;
                        }
                      }
                    }
                  else {
                    const v423 = stdlib.gt(v371, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:100:81:decimal', stdlib.UInt_max, 0));
                    if (v423) {
                      const v431 = stdlib.gt(v370, v155);
                      if (v431) {
                        const v439 = stdlib.sub(v370, v155);
                        const v450 = [v439, v371, v372, v373];
                        v374 = v450;
                        }
                      else {
                        const v441 = stdlib.sub(v155, v370);
                        const v451 = [v441, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:110:45:decimal', stdlib.UInt_max, 0), v372, v373];
                        v374 = v451;
                        }
                      }
                    else {
                      const v443 = stdlib.add(v370, v155);
                      const v452 = [v443, v371, v372, v373];
                      v374 = v452;
                      }
                    }
                  }
                }
              const v453 = v374[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:371:23:array', stdlib.UInt_max, 0)];
              const v454 = v374[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:371:23:array', stdlib.UInt_max, 1)];
              const v455 = v374[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:371:23:array', stdlib.UInt_max, 2)];
              const v456 = v374[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:371:23:array', stdlib.UInt_max, 3)];
              let v457;
              const v458 = stdlib.gt(v158, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:129:18:decimal', stdlib.UInt_max, 0));
              if (v458) {
                const v459 = stdlib.add(v158, v197);
                const v460 = stdlib.gt(v89, v73);
                if (v460) {
                  const v461 = stdlib.sub(v89, v73);
                  const v462 = stdlib.add(v461, v459);
                  const v470 = [v462];
                  v457 = v470;
                  }
                else {
                  const v464 = stdlib.add(v89, v459);
                  const v471 = [v464];
                  v457 = v471;
                  }
                }
              else {
                const v466 = stdlib.gt(v89, v73);
                if (v466) {
                  const v467 = stdlib.sub(v89, v73);
                  const v472 = [v467];
                  v457 = v472;
                  }
                else {
                  const v473 = [v89];
                  v457 = v473;
                  }
                }
              const v474 = v457[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:373:23:array', stdlib.UInt_max, 0)];
              const v475 = stdlib.add(v91, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:375:145:decimal', stdlib.UInt_max, 1));
              const cv83 = v453;
              const cv84 = v454;
              const cv85 = v455;
              const cv86 = v456;
              const cv89 = v474;
              const cv90 = v90;
              const cv91 = v475;
              const cv92 = v92;
              const cv93 = v93;
              const cv94 = v94;
              const cv992 = v201;
              
              (() => {
                const v83 = cv83;
                const v84 = cv84;
                const v85 = cv85;
                const v86 = cv86;
                const v89 = cv89;
                const v90 = cv90;
                const v91 = cv91;
                const v92 = cv92;
                const v93 = cv93;
                const v94 = cv94;
                const v992 = cv992;
                
                if ((() => {
                  const v101 = stdlib.gt(v93, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:298:25:decimal', stdlib.UInt_max, 0));
                  const v103 = stdlib.eq(v89, v73);
                  const v105 = v103 ? false : true;
                  const v108 = v101 ? v105 : false;
                  const v110 = stdlib.eq(v89, v52);
                  const v112 = v110 ? false : true;
                  const v115 = v108 ? v112 : false;
                  
                  return v115;})()) {
                  
                  const v123 = stdlib.mod(v91, v92);
                  const v125 = stdlib.eq(v123, v90);
                  if (v125) {
                    sim_r.nextSt = stdlib.digest(ctc9, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:379:25:after expr stmt semicolon', stdlib.UInt_max, 16), v23, v24, v31, v51, v52, v72, v73, v83, v84, v85, v86, v89, v90, v91, v92, v93, v94, v992]);
                    sim_r.nextSt_noTime = stdlib.digest(ctc10, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:379:25:after expr stmt semicolon', stdlib.UInt_max, 16), v23, v24, v31, v51, v52, v72, v73, v83, v84, v85, v86, v89, v90, v91, v92, v93, v94]);
                    sim_r.view = [ctc5, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:379:25:after expr stmt semicolon', stdlib.UInt_max, 0)]];
                    sim_r.isHalt = false;
                    }
                  else {
                    sim_r.nextSt = stdlib.digest(ctc9, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:311:25:after expr stmt semicolon', stdlib.UInt_max, 18), v23, v24, v31, v51, v52, v72, v73, v83, v84, v85, v86, v89, v90, v91, v92, v93, v94, v992]);
                    sim_r.nextSt_noTime = stdlib.digest(ctc10, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:311:25:after expr stmt semicolon', stdlib.UInt_max, 18), v23, v24, v31, v51, v52, v72, v73, v83, v84, v85, v86, v89, v90, v91, v92, v93, v94]);
                    sim_r.view = [ctc5, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:311:25:after expr stmt semicolon', stdlib.UInt_max, 0)]];
                    sim_r.isHalt = false;
                    }}
                else {
                  sim_r.nextSt = stdlib.digest(ctc12, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:448:17:after expr stmt semicolon', stdlib.UInt_max, 9), v23, v24, v31, v51, v72, v83, v84, v85, v86, v992]);
                  sim_r.nextSt_noTime = stdlib.digest(ctc13, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:448:17:after expr stmt semicolon', stdlib.UInt_max, 9), v23, v24, v31, v51, v72, v83, v84, v85, v86]);
                  sim_r.view = [ctc5, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:448:17:after expr stmt semicolon', stdlib.UInt_max, 0)]];
                  sim_r.isHalt = false;
                  }})();
              return sim_r;
              })));
            const [v187, v188, v189, v190, v191, v192, v193, v194, v195, v196, v197, v198] = txn6.data;
            const v201 = txn6.time;
            const v186 = txn6.from;
            ;
            const v200 = stdlib.addressEq(v31, v186);
            stdlib.assert(v200, {
              at: './src/rsh/battleRecord.rsh:361:19:dot',
              fs: [],
              msg: 'sender correct',
              who: 'Opponent'
              });
            let v208;
            const v209 = stdlib.lt(stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:67:9:decimal', stdlib.UInt_max, 0), v149);
            const v210 = stdlib.lt(v149, v94);
            const v211 = v209 ? v210 : false;
            if (v211) {
              const v279 = [v83, v84, v85, v86];
              v208 = v279;
              }
            else {
              const v219 = stdlib.eq(v149, v188);
              if (v219) {
                const v280 = [v83, v84, v85, v86];
                v208 = v280;
                }
              else {
                const v227 = stdlib.gt(v150, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:80:57:decimal', stdlib.UInt_max, 0));
                if (v227) {
                  const v235 = stdlib.gt(v86, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:85:83:decimal', stdlib.UInt_max, 0));
                  if (v235) {
                    const v243 = stdlib.add(v85, v149);
                    const v281 = [v83, v84, v243, v86];
                    v208 = v281;
                    }
                  else {
                    const v245 = stdlib.gt(v85, v149);
                    if (v245) {
                      const v253 = stdlib.sub(v85, v149);
                      const v282 = [v83, v84, v253, v86];
                      v208 = v282;
                      }
                    else {
                      const v255 = stdlib.sub(v149, v85);
                      const v283 = [v83, v84, v255, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:96:59:decimal', stdlib.UInt_max, 1)];
                      v208 = v283;
                      }
                    }
                  }
                else {
                  const v257 = stdlib.gt(v84, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:100:81:decimal', stdlib.UInt_max, 0));
                  if (v257) {
                    const v265 = stdlib.gt(v83, v149);
                    if (v265) {
                      const v273 = stdlib.sub(v83, v149);
                      const v284 = [v273, v84, v85, v86];
                      v208 = v284;
                      }
                    else {
                      const v275 = stdlib.sub(v149, v83);
                      const v285 = [v275, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:110:45:decimal', stdlib.UInt_max, 0), v85, v86];
                      v208 = v285;
                      }
                    }
                  else {
                    const v277 = stdlib.add(v83, v149);
                    const v286 = [v277, v84, v85, v86];
                    v208 = v286;
                    }
                  }
                }
              }
            const v287 = v208[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:366:23:array', stdlib.UInt_max, 0)];
            const v288 = v208[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:366:23:array', stdlib.UInt_max, 1)];
            const v289 = v208[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:366:23:array', stdlib.UInt_max, 2)];
            const v290 = v208[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:366:23:array', stdlib.UInt_max, 3)];
            let v291;
            const v292 = stdlib.lt(stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:67:9:decimal', stdlib.UInt_max, 0), v152);
            const v293 = stdlib.lt(v152, v94);
            const v294 = v292 ? v293 : false;
            if (v294) {
              const v362 = [v287, v288, v289, v290];
              v291 = v362;
              }
            else {
              const v302 = stdlib.eq(v152, v191);
              if (v302) {
                const v363 = [v287, v288, v289, v290];
                v291 = v363;
                }
              else {
                const v310 = stdlib.gt(v153, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:80:57:decimal', stdlib.UInt_max, 0));
                if (v310) {
                  const v318 = stdlib.gt(v290, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:85:83:decimal', stdlib.UInt_max, 0));
                  if (v318) {
                    const v326 = stdlib.add(v289, v152);
                    const v364 = [v287, v288, v326, v290];
                    v291 = v364;
                    }
                  else {
                    const v328 = stdlib.gt(v289, v152);
                    if (v328) {
                      const v336 = stdlib.sub(v289, v152);
                      const v365 = [v287, v288, v336, v290];
                      v291 = v365;
                      }
                    else {
                      const v338 = stdlib.sub(v152, v289);
                      const v366 = [v287, v288, v338, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:96:59:decimal', stdlib.UInt_max, 1)];
                      v291 = v366;
                      }
                    }
                  }
                else {
                  const v340 = stdlib.gt(v288, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:100:81:decimal', stdlib.UInt_max, 0));
                  if (v340) {
                    const v348 = stdlib.gt(v287, v152);
                    if (v348) {
                      const v356 = stdlib.sub(v287, v152);
                      const v367 = [v356, v288, v289, v290];
                      v291 = v367;
                      }
                    else {
                      const v358 = stdlib.sub(v152, v287);
                      const v368 = [v358, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:110:45:decimal', stdlib.UInt_max, 0), v289, v290];
                      v291 = v368;
                      }
                    }
                  else {
                    const v360 = stdlib.add(v287, v152);
                    const v369 = [v360, v288, v289, v290];
                    v291 = v369;
                    }
                  }
                }
              }
            const v370 = v291[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:368:23:array', stdlib.UInt_max, 0)];
            const v371 = v291[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:368:23:array', stdlib.UInt_max, 1)];
            const v372 = v291[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:368:23:array', stdlib.UInt_max, 2)];
            const v373 = v291[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:368:23:array', stdlib.UInt_max, 3)];
            let v374;
            const v375 = stdlib.lt(stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:67:9:decimal', stdlib.UInt_max, 0), v155);
            const v376 = stdlib.lt(v155, v94);
            const v377 = v375 ? v376 : false;
            if (v377) {
              const v445 = [v370, v371, v372, v373];
              v374 = v445;
              }
            else {
              const v385 = stdlib.eq(v155, v194);
              if (v385) {
                const v446 = [v370, v371, v372, v373];
                v374 = v446;
                }
              else {
                const v393 = stdlib.gt(v156, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:80:57:decimal', stdlib.UInt_max, 0));
                if (v393) {
                  const v401 = stdlib.gt(v373, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:85:83:decimal', stdlib.UInt_max, 0));
                  if (v401) {
                    const v409 = stdlib.add(v372, v155);
                    const v447 = [v370, v371, v409, v373];
                    v374 = v447;
                    }
                  else {
                    const v411 = stdlib.gt(v372, v155);
                    if (v411) {
                      const v419 = stdlib.sub(v372, v155);
                      const v448 = [v370, v371, v419, v373];
                      v374 = v448;
                      }
                    else {
                      const v421 = stdlib.sub(v155, v372);
                      const v449 = [v370, v371, v421, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:96:59:decimal', stdlib.UInt_max, 1)];
                      v374 = v449;
                      }
                    }
                  }
                else {
                  const v423 = stdlib.gt(v371, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:100:81:decimal', stdlib.UInt_max, 0));
                  if (v423) {
                    const v431 = stdlib.gt(v370, v155);
                    if (v431) {
                      const v439 = stdlib.sub(v370, v155);
                      const v450 = [v439, v371, v372, v373];
                      v374 = v450;
                      }
                    else {
                      const v441 = stdlib.sub(v155, v370);
                      const v451 = [v441, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:110:45:decimal', stdlib.UInt_max, 0), v372, v373];
                      v374 = v451;
                      }
                    }
                  else {
                    const v443 = stdlib.add(v370, v155);
                    const v452 = [v443, v371, v372, v373];
                    v374 = v452;
                    }
                  }
                }
              }
            const v453 = v374[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:371:23:array', stdlib.UInt_max, 0)];
            const v454 = v374[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:371:23:array', stdlib.UInt_max, 1)];
            const v455 = v374[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:371:23:array', stdlib.UInt_max, 2)];
            const v456 = v374[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:371:23:array', stdlib.UInt_max, 3)];
            let v457;
            const v458 = stdlib.gt(v158, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:129:18:decimal', stdlib.UInt_max, 0));
            if (v458) {
              const v459 = stdlib.add(v158, v197);
              const v460 = stdlib.gt(v89, v73);
              if (v460) {
                const v461 = stdlib.sub(v89, v73);
                const v462 = stdlib.add(v461, v459);
                const v470 = [v462];
                v457 = v470;
                }
              else {
                const v464 = stdlib.add(v89, v459);
                const v471 = [v464];
                v457 = v471;
                }
              }
            else {
              const v466 = stdlib.gt(v89, v73);
              if (v466) {
                const v467 = stdlib.sub(v89, v73);
                const v472 = [v467];
                v457 = v472;
                }
              else {
                const v473 = [v89];
                v457 = v473;
                }
              }
            const v474 = v457[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:373:23:array', stdlib.UInt_max, 0)];
            const v475 = stdlib.add(v91, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:375:145:decimal', stdlib.UInt_max, 1));
            const cv83 = v453;
            const cv84 = v454;
            const cv85 = v455;
            const cv86 = v456;
            const cv89 = v474;
            const cv90 = v90;
            const cv91 = v475;
            const cv92 = v92;
            const cv93 = v93;
            const cv94 = v94;
            const cv992 = v201;
            
            v83 = cv83;
            v84 = cv84;
            v85 = cv85;
            v86 = cv86;
            v89 = cv89;
            v90 = cv90;
            v91 = cv91;
            v92 = cv92;
            v93 = cv93;
            v94 = cv94;
            v992 = cv992;
            
            continue;
            
            }}
        const txn5 = await (ctc.recv(10, 2, [ctc0, ctc0], false, false));
        const [v820, v821] = txn5.data;
        const v824 = txn5.time;
        const v819 = txn5.from;
        ;
        const v823 = stdlib.addressEq(v23, v819);
        stdlib.assert(v823, {
          at: './src/rsh/battleRecord.rsh:454:11:dot',
          fs: [],
          msg: 'sender correct',
          who: 'Opponent'
          });
        const v826 = stdlib.digest(ctc2, [v820, v821]);
        const v827 = stdlib.digestEq(v72, v826);
        stdlib.assert(v827, {
          at: 'reach standard library:65:17:application',
          fs: ['at ./src/rsh/battleRecord.rsh:456:24:application call to "checkCommitment" (defined at: reach standard library:64:8:function exp)'],
          msg: null,
          who: 'Opponent'
          });
        const txn6 = await (ctc.sendrecv(11, 2, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:462:11:dot', stdlib.UInt_max, 9), [ctc6, ctc0, ctc6, ctc3, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0, ctc0], [v23, v24, v31, v51, v83, v84, v85, v86, v821, v824, v46, v44], [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:decimal', stdlib.UInt_max, 0), []], [ctc0, ctc0], true, true, false, (async (txn6) => {
          const sim_r = { txns: [], mapRefs: [], mapsPrev: [], mapsNext: [] };
          
          sim_r.prevSt = stdlib.digest(ctc15, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:462:11:dot', stdlib.UInt_max, 10), v23, v24, v31, v51, v83, v84, v85, v86, v821, v824]);
          sim_r.prevSt_noPrevTime = stdlib.digest(ctc16, [stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:462:11:dot', stdlib.UInt_max, 10), v23, v24, v31, v51, v83, v84, v85, v86, v821]);
          const [v833, v834] = txn6.data;
          const v837 = txn6.time;
          const v832 = txn6.from;
          
          sim_r.txns.push({
            amt: stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:decimal', stdlib.UInt_max, 0),
            kind: 'to',
            tok: undefined
            });
          const v836 = stdlib.addressEq(v31, v832);
          stdlib.assert(v836, {
            at: './src/rsh/battleRecord.rsh:462:11:dot',
            fs: [],
            msg: 'sender correct',
            who: 'Opponent'
            });
          const v839 = stdlib.digest(ctc2, [v833, v834]);
          const v840 = stdlib.digestEq(v51, v839);
          stdlib.assert(v840, {
            at: 'reach standard library:65:17:application',
            fs: ['at ./src/rsh/battleRecord.rsh:464:24:application call to "checkCommitment" (defined at: reach standard library:64:8:function exp)'],
            msg: null,
            who: 'Opponent'
            });
          let v842;
          const v844 = stdlib.eq(v84, v86);
          if (v844) {
            const v862 = stdlib.gt(v84, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:181:19:decimal', stdlib.UInt_max, 0));
            if (v862) {
              const v863 = stdlib.gt(v821, v83);
              if (v863) {
                const v864 = stdlib.sub(v821, v83);
                const v865 = stdlib.gt(v834, v85);
                if (v865) {
                  const v866 = stdlib.sub(v834, v85);
                  const v883 = [v864, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:188:35:decimal', stdlib.UInt_max, 0), v866, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:188:44:decimal', stdlib.UInt_max, 0)];
                  v842 = v883;
                  }
                else {
                  const v868 = stdlib.sub(v85, v834);
                  const v884 = [v864, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:191:35:decimal', stdlib.UInt_max, 0), v868, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:191:44:decimal', stdlib.UInt_max, 1)];
                  v842 = v884;
                  }
                }
              else {
                const v870 = stdlib.sub(v83, v821);
                const v871 = stdlib.gt(v834, v85);
                if (v871) {
                  const v872 = stdlib.sub(v834, v85);
                  const v885 = [v870, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:197:35:decimal', stdlib.UInt_max, 1), v872, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:197:44:decimal', stdlib.UInt_max, 0)];
                  v842 = v885;
                  }
                else {
                  const v874 = stdlib.sub(v85, v834);
                  const v886 = [v870, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:200:35:decimal', stdlib.UInt_max, 1), v874, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:200:44:decimal', stdlib.UInt_max, 1)];
                  v842 = v886;
                  }
                }
              }
            else {
              const v876 = stdlib.add(v83, v821);
              const v877 = stdlib.add(v85, v834);
              const v887 = [v876, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:207:27:decimal', stdlib.UInt_max, 0), v877, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:207:36:decimal', stdlib.UInt_max, 0)];
              v842 = v887;
              }
            }
          else {
            const v849 = stdlib.gt(v84, v86);
            if (v849) {
              const v850 = stdlib.add(v834, v85);
              const v851 = stdlib.gt(v821, v83);
              if (v851) {
                const v852 = stdlib.sub(v821, v83);
                const v879 = [v852, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:162:31:decimal', stdlib.UInt_max, 0), v850, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:162:40:decimal', stdlib.UInt_max, 0)];
                v842 = v879;
                }
              else {
                const v854 = stdlib.sub(v83, v821);
                const v880 = [v854, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:165:31:decimal', stdlib.UInt_max, 1), v850, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:165:40:decimal', stdlib.UInt_max, 0)];
                v842 = v880;
                }
              }
            else {
              const v856 = stdlib.add(v821, v83);
              const v857 = stdlib.gt(v834, v85);
              if (v857) {
                const v858 = stdlib.sub(v834, v85);
                const v881 = [v856, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:172:31:decimal', stdlib.UInt_max, 0), v858, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:172:40:decimal', stdlib.UInt_max, 0)];
                v842 = v881;
                }
              else {
                const v860 = stdlib.sub(v85, v834);
                const v882 = [v856, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:175:31:decimal', stdlib.UInt_max, 0), v860, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:175:40:decimal', stdlib.UInt_max, 1)];
                v842 = v882;
                }
              }
            }
          const v888 = v842[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:467:15:array', stdlib.UInt_max, 0)];
          const v889 = v842[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:467:15:array', stdlib.UInt_max, 1)];
          const v890 = v842[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:467:15:array', stdlib.UInt_max, 2)];
          const v891 = v842[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:467:15:array', stdlib.UInt_max, 3)];
          const v892 = stdlib.eq(v888, v890);
          const v893 = stdlib.eq(v889, v891);
          const v894 = v892 ? v893 : false;
          if (v894) {
            const v895 = stdlib.mul(stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:471:22:decimal', stdlib.UInt_max, 2), v24);
            sim_r.txns.push({
              amt: v895,
              kind: 'from',
              to: v23,
              tok: undefined
              });
            sim_r.txns.push({
              kind: 'halt',
              tok: undefined
              })
            sim_r.nextSt = stdlib.digest(ctc14, []);
            sim_r.nextSt_noTime = stdlib.digest(ctc14, []);
            sim_r.view = [ctc14, []];
            sim_r.isHalt = true;
            }
          else {
            const v900 = stdlib.gt(v889, v891);
            if (v900) {
              const v901 = stdlib.mul(stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:475:26:decimal', stdlib.UInt_max, 2), v24);
              sim_r.txns.push({
                amt: v901,
                kind: 'from',
                to: v31,
                tok: undefined
                });
              sim_r.txns.push({
                kind: 'halt',
                tok: undefined
                })
              sim_r.nextSt = stdlib.digest(ctc14, []);
              sim_r.nextSt_noTime = stdlib.digest(ctc14, []);
              sim_r.view = [ctc14, []];
              sim_r.isHalt = true;
              }
            else {
              if (v893) {
                const v907 = stdlib.gt(v889, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:478:34:decimal', stdlib.UInt_max, 0));
                if (v907) {
                  const v908 = stdlib.mul(stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:479:34:decimal', stdlib.UInt_max, 2), v24);
                  const v909 = stdlib.gt(v888, v890);
                  const v910 = v909 ? v31 : v23;
                  sim_r.txns.push({
                    amt: v908,
                    kind: 'from',
                    to: v910,
                    tok: undefined
                    });
                  sim_r.txns.push({
                    kind: 'halt',
                    tok: undefined
                    })
                  sim_r.nextSt = stdlib.digest(ctc14, []);
                  sim_r.nextSt_noTime = stdlib.digest(ctc14, []);
                  sim_r.view = [ctc14, []];
                  sim_r.isHalt = true;
                  }
                else {
                  const v915 = stdlib.mul(stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:482:34:decimal', stdlib.UInt_max, 2), v24);
                  const v916 = stdlib.gt(v888, v890);
                  const v917 = v916 ? v23 : v31;
                  sim_r.txns.push({
                    amt: v915,
                    kind: 'from',
                    to: v917,
                    tok: undefined
                    });
                  sim_r.txns.push({
                    kind: 'halt',
                    tok: undefined
                    })
                  sim_r.nextSt = stdlib.digest(ctc14, []);
                  sim_r.nextSt_noTime = stdlib.digest(ctc14, []);
                  sim_r.view = [ctc14, []];
                  sim_r.isHalt = true;
                  }}
              else {
                const v922 = stdlib.mul(stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:485:30:decimal', stdlib.UInt_max, 2), v24);
                sim_r.txns.push({
                  amt: v922,
                  kind: 'from',
                  to: v23,
                  tok: undefined
                  });
                sim_r.txns.push({
                  kind: 'halt',
                  tok: undefined
                  })
                sim_r.nextSt = stdlib.digest(ctc14, []);
                sim_r.nextSt_noTime = stdlib.digest(ctc14, []);
                sim_r.view = [ctc14, []];
                sim_r.isHalt = true;
                }}}
          return sim_r;
          })));
        const [v833, v834] = txn6.data;
        const v837 = txn6.time;
        const v832 = txn6.from;
        ;
        const v836 = stdlib.addressEq(v31, v832);
        stdlib.assert(v836, {
          at: './src/rsh/battleRecord.rsh:462:11:dot',
          fs: [],
          msg: 'sender correct',
          who: 'Opponent'
          });
        const v839 = stdlib.digest(ctc2, [v833, v834]);
        const v840 = stdlib.digestEq(v51, v839);
        stdlib.assert(v840, {
          at: 'reach standard library:65:17:application',
          fs: ['at ./src/rsh/battleRecord.rsh:464:24:application call to "checkCommitment" (defined at: reach standard library:64:8:function exp)'],
          msg: null,
          who: 'Opponent'
          });
        let v842;
        const v844 = stdlib.eq(v84, v86);
        if (v844) {
          const v862 = stdlib.gt(v84, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:181:19:decimal', stdlib.UInt_max, 0));
          if (v862) {
            const v863 = stdlib.gt(v821, v83);
            if (v863) {
              const v864 = stdlib.sub(v821, v83);
              const v865 = stdlib.gt(v834, v85);
              if (v865) {
                const v866 = stdlib.sub(v834, v85);
                const v883 = [v864, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:188:35:decimal', stdlib.UInt_max, 0), v866, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:188:44:decimal', stdlib.UInt_max, 0)];
                v842 = v883;
                }
              else {
                const v868 = stdlib.sub(v85, v834);
                const v884 = [v864, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:191:35:decimal', stdlib.UInt_max, 0), v868, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:191:44:decimal', stdlib.UInt_max, 1)];
                v842 = v884;
                }
              }
            else {
              const v870 = stdlib.sub(v83, v821);
              const v871 = stdlib.gt(v834, v85);
              if (v871) {
                const v872 = stdlib.sub(v834, v85);
                const v885 = [v870, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:197:35:decimal', stdlib.UInt_max, 1), v872, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:197:44:decimal', stdlib.UInt_max, 0)];
                v842 = v885;
                }
              else {
                const v874 = stdlib.sub(v85, v834);
                const v886 = [v870, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:200:35:decimal', stdlib.UInt_max, 1), v874, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:200:44:decimal', stdlib.UInt_max, 1)];
                v842 = v886;
                }
              }
            }
          else {
            const v876 = stdlib.add(v83, v821);
            const v877 = stdlib.add(v85, v834);
            const v887 = [v876, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:207:27:decimal', stdlib.UInt_max, 0), v877, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:207:36:decimal', stdlib.UInt_max, 0)];
            v842 = v887;
            }
          }
        else {
          const v849 = stdlib.gt(v84, v86);
          if (v849) {
            const v850 = stdlib.add(v834, v85);
            const v851 = stdlib.gt(v821, v83);
            if (v851) {
              const v852 = stdlib.sub(v821, v83);
              const v879 = [v852, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:162:31:decimal', stdlib.UInt_max, 0), v850, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:162:40:decimal', stdlib.UInt_max, 0)];
              v842 = v879;
              }
            else {
              const v854 = stdlib.sub(v83, v821);
              const v880 = [v854, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:165:31:decimal', stdlib.UInt_max, 1), v850, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:165:40:decimal', stdlib.UInt_max, 0)];
              v842 = v880;
              }
            }
          else {
            const v856 = stdlib.add(v821, v83);
            const v857 = stdlib.gt(v834, v85);
            if (v857) {
              const v858 = stdlib.sub(v834, v85);
              const v881 = [v856, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:172:31:decimal', stdlib.UInt_max, 0), v858, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:172:40:decimal', stdlib.UInt_max, 0)];
              v842 = v881;
              }
            else {
              const v860 = stdlib.sub(v85, v834);
              const v882 = [v856, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:175:31:decimal', stdlib.UInt_max, 0), v860, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:175:40:decimal', stdlib.UInt_max, 1)];
              v842 = v882;
              }
            }
          }
        const v888 = v842[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:467:15:array', stdlib.UInt_max, 0)];
        const v889 = v842[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:467:15:array', stdlib.UInt_max, 1)];
        const v890 = v842[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:467:15:array', stdlib.UInt_max, 2)];
        const v891 = v842[stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:467:15:array', stdlib.UInt_max, 3)];
        const v892 = stdlib.eq(v888, v890);
        const v893 = stdlib.eq(v889, v891);
        const v894 = v892 ? v893 : false;
        if (v894) {
          const v895 = stdlib.mul(stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:471:22:decimal', stdlib.UInt_max, 2), v24);
          ;
          stdlib.protect(ctc1, await interact.tellWhoWinHandle(v888, v889, v890, v891), {
            at: './src/rsh/battleRecord.rsh:493:38:application',
            fs: ['at ./src/rsh/battleRecord.rsh:492:13:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:492:25:function exp)'],
            msg: 'tellWhoWinHandle',
            who: 'Opponent'
            });
          
          return;}
        else {
          const v900 = stdlib.gt(v889, v891);
          if (v900) {
            const v901 = stdlib.mul(stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:475:26:decimal', stdlib.UInt_max, 2), v24);
            ;
            stdlib.protect(ctc1, await interact.tellWhoWinHandle(v888, v889, v890, v891), {
              at: './src/rsh/battleRecord.rsh:493:38:application',
              fs: ['at ./src/rsh/battleRecord.rsh:492:13:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:492:25:function exp)'],
              msg: 'tellWhoWinHandle',
              who: 'Opponent'
              });
            
            return;}
          else {
            if (v893) {
              const v907 = stdlib.gt(v889, stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:478:34:decimal', stdlib.UInt_max, 0));
              if (v907) {
                const v908 = stdlib.mul(stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:479:34:decimal', stdlib.UInt_max, 2), v24);
                const v909 = stdlib.gt(v888, v890);
                const v910 = v909 ? v31 : v23;
                ;
                stdlib.protect(ctc1, await interact.tellWhoWinHandle(v888, v889, v890, v891), {
                  at: './src/rsh/battleRecord.rsh:493:38:application',
                  fs: ['at ./src/rsh/battleRecord.rsh:492:13:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:492:25:function exp)'],
                  msg: 'tellWhoWinHandle',
                  who: 'Opponent'
                  });
                
                return;}
              else {
                const v915 = stdlib.mul(stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:482:34:decimal', stdlib.UInt_max, 2), v24);
                const v916 = stdlib.gt(v888, v890);
                const v917 = v916 ? v23 : v31;
                ;
                stdlib.protect(ctc1, await interact.tellWhoWinHandle(v888, v889, v890, v891), {
                  at: './src/rsh/battleRecord.rsh:493:38:application',
                  fs: ['at ./src/rsh/battleRecord.rsh:492:13:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:492:25:function exp)'],
                  msg: 'tellWhoWinHandle',
                  who: 'Opponent'
                  });
                
                return;}}
            else {
              const v922 = stdlib.mul(stdlib.checkedBigNumberify('./src/rsh/battleRecord.rsh:485:30:decimal', stdlib.UInt_max, 2), v24);
              ;
              stdlib.protect(ctc1, await interact.tellWhoWinHandle(v888, v889, v890, v891), {
                at: './src/rsh/battleRecord.rsh:493:38:application',
                fs: ['at ./src/rsh/battleRecord.rsh:492:13:application call to [unknown function] (defined at: ./src/rsh/battleRecord.rsh:492:25:function exp)'],
                msg: 'tellWhoWinHandle',
                who: 'Opponent'
                });
              
              return;}}}
        
        }
      }
    }
  
  
  };

const _ALGO = {
  appApproval: `#pragma version 3
txn RekeyTo
global ZeroAddress
==
assert
txn OnCompletion
int OptIn
==
bz normal
global GroupSize
int 1
==
assert
b done
normal:
gtxna 0 ApplicationArgs 8
store 5
// Check that everyone's here
global GroupSize
int 3
>=
assert
// Check txnAppl (us)
txn GroupIndex
int 0
==
assert
// Check txnFromHandler
int 0
gtxn 2 Sender
byte "{{m1}}"
==
||
gtxn 2 Sender
byte "{{m2}}"
==
||
gtxn 2 Sender
byte "{{m3}}"
==
||
gtxn 2 Sender
byte "{{m4}}"
==
||
gtxn 2 Sender
byte "{{m5}}"
==
||
gtxn 2 Sender
byte "{{m6}}"
==
||
gtxn 2 Sender
byte "{{m7}}"
==
||
gtxn 2 Sender
byte "{{m10}}"
==
||
gtxn 2 Sender
byte "{{m11}}"
==
||
gtxn 2 Sender
byte "{{m12}}"
==
||
gtxn 2 Sender
byte "{{m13}}"
==
||
gtxn 2 Sender
byte "{{m14}}"
==
||
gtxn 2 Sender
byte "{{m15}}"
==
||
assert
byte base64(cw==)
app_global_get
gtxna 0 ApplicationArgs 0
==
assert
byte base64(cw==)
gtxna 0 ApplicationArgs 1
app_global_put
byte base64(bA==)
app_global_get
gtxna 0 ApplicationArgs 5
btoi
==
assert
byte base64(bA==)
global Round
app_global_put
int 0
txn NumAccounts
==
assert
byte base64(aA==)
gtxna 0 ApplicationArgs 3
btoi
app_global_put
byte base64(aA==)
app_global_get
bnz halted
txn OnCompletion
int NoOp
==
assert
b done
halted:
txn OnCompletion
int DeleteApplication
==
assert
done:
int 1
return
`,
  appApproval0: `#pragma version 3
// Check that we're an App
txn TypeEnum
int appl
==
assert
txn RekeyTo
global ZeroAddress
==
assert
txn Sender
byte "{{Deployer}}"
==
assert
txn ApplicationID
bz init
global GroupSize
int 2
==
assert
gtxn 1 TypeEnum
int pay
==
assert
gtxn 1 Amount
int 100000
==
assert
// We don't check the receiver, because we don't know it yet, because the escrow account embeds our id
// We don't check the sender, because we don't care... anyone is allowed to fund it. We'll give it back to the deployer, though.
txn OnCompletion
int UpdateApplication
==
assert
byte base64(cw==)
// compute state in HM_Set 0
int 0
itob
keccak256
app_global_put
byte base64(bA==)
global Round
app_global_put
byte base64(aA==)
int 0
app_global_put
b done
init:
global GroupSize
int 1
==
assert
txn OnCompletion
int NoOp
==
assert
done:
int 1
return
`,
  appClear: `#pragma version 3
// We're alone
global GroupSize
int 1
==
assert
// We're halted
byte base64(aA==)
app_global_get
int 1
==
assert
done:
int 1
return
`,
  ctc: `#pragma version 3
// Check size
global GroupSize
int 3
>=
assert
// Check txnAppl
gtxn 0 TypeEnum
int appl
==
assert
gtxn 0 ApplicationID
byte "{{ApplicationID}}"
btoi
==
assert
// Don't check anything else, because app does
// Check us
txn TypeEnum
int pay
==
int axfer
dup2
==
||
assert
txn RekeyTo
global ZeroAddress
==
assert
txn GroupIndex
int 3
>=
assert
done:
int 1
return
`,
  mapArgSize: 165,
  mapDataKeys: 0,
  mapDataSize: 0,
  mapRecordSize: 33,
  stepargs: [null, {
    count: 9,
    size: 254
    }, {
    count: 9,
    size: 286
    }, {
    count: 9,
    size: 286
    }, {
    count: 9,
    size: 366
    }, {
    count: 9,
    size: 326
    }, {
    count: 9,
    size: 406
    }, {
    count: 9,
    size: 366
    }, null, null, {
    count: 9,
    size: 430
    }, {
    count: 9,
    size: 406
    }, {
    count: 9,
    size: 582
    }, {
    count: 9,
    size: 630
    }, {
    count: 9,
    size: 582
    }, {
    count: 9,
    size: 630
    }],
  steps: [null, `#pragma version 3
gtxna 0 ApplicationArgs 1
store 0
gtxna 0 ApplicationArgs 2
store 1
gtxna 0 ApplicationArgs 3
store 2
gtxna 0 ApplicationArgs 4
store 3
gtxna 0 ApplicationArgs 5
store 4
gtxna 0 ApplicationArgs 8
store 5
int 0
store 6
gtxna 0 ApplicationArgs 7
dup
substring 0 8
btoi
store 255
pop
// Handler 1
// Check txnAppl
gtxn 0 TypeEnum
int appl
==
assert
gtxn 0 ApplicationID
byte "{{ApplicationID}}"
btoi
==
assert
gtxn 0 NumAppArgs
int 9
==
assert
// Check txnToHandler
gtxn 1 TypeEnum
int pay
==
assert
gtxn 1 Receiver
txn Sender
==
assert
gtxn 1 Amount
gtxn 2 Fee
int 100000
+
==
assert
// Check txnFromHandler (us)
txn GroupIndex
int 2
==
assert
txn TypeEnum
int pay
==
assert
txn Amount
int 0
==
assert
txn Receiver
gtxn 1 Sender
==
assert
// compute state in HM_Check 0
int 0
itob
keccak256
gtxna 0 ApplicationArgs 0
==
assert
txn CloseRemainderTo
gtxn 1 Sender
==
assert
// Run body
// "CheckPay"
// "./src/rsh/battleRecord.rsh:233:11:dot"
// "[]"
gtxn 3 TypeEnum
int pay
==
assert
gtxn 3 Receiver
byte "{{ContractAddr}}"
==
assert
gtxn 3 Amount
load 3
btoi
-
load 255
==
assert
// We don't care who the sender is... this means that you can get other people to pay for you if you want.
byte base64()
load 1
==
assert
// compute state in HM_Set 1
int 1
itob
gtxn 0 Sender
concat
load 255
itob
concat
keccak256
load 0
==
assert
load 2
btoi
int 0
==
assert
// Check GroupSize
global GroupSize
int 4
==
assert
load 3
btoi
int 0
==
assert
// Check time limits
checkAccts:
gtxn 0 NumAccounts
load 6
==
assert
done:
int 1
return
`, `#pragma version 3
gtxna 0 ApplicationArgs 1
store 0
gtxna 0 ApplicationArgs 2
store 1
gtxna 0 ApplicationArgs 3
store 2
gtxna 0 ApplicationArgs 4
store 3
gtxna 0 ApplicationArgs 5
store 4
gtxna 0 ApplicationArgs 8
store 5
int 0
store 6
gtxna 0 ApplicationArgs 6
dup
substring 0 32
store 255
dup
substring 32 40
btoi
store 254
pop
// Handler 2
// Check txnAppl
gtxn 0 TypeEnum
int appl
==
assert
gtxn 0 ApplicationID
byte "{{ApplicationID}}"
btoi
==
assert
gtxn 0 NumAppArgs
int 9
==
assert
// Check txnToHandler
gtxn 1 TypeEnum
int pay
==
assert
gtxn 1 Receiver
txn Sender
==
assert
gtxn 1 Amount
gtxn 2 Fee
int 100000
+
==
assert
// Check txnFromHandler (us)
txn GroupIndex
int 2
==
assert
txn TypeEnum
int pay
==
assert
txn Amount
int 0
==
assert
txn Receiver
gtxn 1 Sender
==
assert
// compute state in HM_Check 1
int 1
itob
load 255
concat
load 254
itob
concat
keccak256
gtxna 0 ApplicationArgs 0
==
assert
txn CloseRemainderTo
gtxn 1 Sender
==
assert
// Run body
load 254
dup
+
store 253
// "CheckPay"
// "./src/rsh/battleRecord.rsh:242:11:dot"
// "[]"
gtxn 3 TypeEnum
int pay
==
assert
gtxn 3 Receiver
byte "{{ContractAddr}}"
==
assert
gtxn 3 Amount
load 3
btoi
-
load 254
==
assert
// We don't care who the sender is... this means that you can get other people to pay for you if you want.
byte base64()
load 1
==
assert
// compute state in HM_Set 3
int 3
itob
load 255
concat
load 254
itob
concat
gtxn 0 Sender
concat
load 253
itob
concat
keccak256
load 0
==
assert
load 2
btoi
int 0
==
assert
// Check GroupSize
global GroupSize
int 4
==
assert
load 3
btoi
int 0
==
assert
// Check time limits
load 4
btoi
int 10
+
dup
gtxn 0 LastValid
>=
assert
dup
gtxn 1 LastValid
>=
assert
dup
gtxn 2 LastValid
>=
assert
dup
gtxn 3 LastValid
>=
assert
pop
checkAccts:
gtxn 0 NumAccounts
load 6
==
assert
done:
int 1
return
`, `#pragma version 3
gtxna 0 ApplicationArgs 1
store 0
gtxna 0 ApplicationArgs 2
store 1
gtxna 0 ApplicationArgs 3
store 2
gtxna 0 ApplicationArgs 4
store 3
gtxna 0 ApplicationArgs 5
store 4
gtxna 0 ApplicationArgs 8
store 5
int 0
store 6
gtxna 0 ApplicationArgs 6
dup
substring 0 32
store 255
dup
substring 32 40
btoi
store 254
pop
// Handler 3
// Check txnAppl
gtxn 0 TypeEnum
int appl
==
assert
gtxn 0 ApplicationID
byte "{{ApplicationID}}"
btoi
==
assert
gtxn 0 NumAppArgs
int 9
==
assert
// Check txnToHandler
gtxn 1 TypeEnum
int pay
==
assert
gtxn 1 Receiver
txn Sender
==
assert
gtxn 1 Amount
gtxn 2 Fee
int 100000
+
==
assert
// Check txnFromHandler (us)
txn GroupIndex
int 2
==
assert
txn TypeEnum
int pay
==
assert
txn Amount
int 0
==
assert
txn Receiver
gtxn 1 Sender
==
assert
// compute state in HM_Check 1
int 1
itob
load 255
concat
load 254
itob
concat
keccak256
gtxna 0 ApplicationArgs 0
==
assert
txn CloseRemainderTo
gtxn 1 Sender
==
assert
// Run body
// "CheckPay"
// "reach standard library:209:7:dot"
// "[at ./src/rsh/battleRecord.rsh:242:57:application call to \"closeTo\" (defined at: reach standard library:207:8:function exp)]"
gtxn 3 TypeEnum
int pay
==
assert
gtxn 3 Receiver
byte "{{ContractAddr}}"
==
assert
gtxn 3 Amount
load 3
btoi
==
assert
// We don't care who the sender is... this means that you can get other people to pay for you if you want.
// Just "sender correct"
// "reach standard library:209:7:dot"
// "[at ./src/rsh/battleRecord.rsh:242:57:application call to \"closeTo\" (defined at: reach standard library:207:8:function exp)]"
load 255
gtxn 0 Sender
==
assert
gtxn 4 TypeEnum
int pay
==
assert
gtxn 4 Receiver
load 255
==
assert
gtxn 4 Amount
load 254
==
assert
gtxn 4 Sender
byte "{{ContractAddr}}"
==
assert
byte base64()
load 1
==
assert
gtxn 5 TypeEnum
int pay
==
assert
// We don't check the receiver
gtxn 5 Amount
int 0
==
assert
gtxn 5 Sender
byte "{{ContractAddr}}"
==
assert
gtxn 5 CloseRemainderTo
byte "{{Deployer}}"
==
assert
load 2
btoi
int 1
==
assert
// Check GroupSize
global GroupSize
int 6
==
assert
load 3
btoi
gtxn 4 Fee
gtxn 5 Fee
+
==
assert
// Check time limits
load 4
btoi
int 10
+
dup
gtxn 0 FirstValid
<=
assert
dup
gtxn 1 FirstValid
<=
assert
dup
gtxn 2 FirstValid
<=
assert
dup
gtxn 3 FirstValid
<=
assert
dup
gtxn 4 FirstValid
<=
assert
dup
gtxn 5 FirstValid
<=
assert
pop
checkAccts:
gtxn 0 NumAccounts
load 6
==
assert
done:
int 1
return
`, `#pragma version 3
gtxna 0 ApplicationArgs 1
store 0
gtxna 0 ApplicationArgs 2
store 1
gtxna 0 ApplicationArgs 3
store 2
gtxna 0 ApplicationArgs 4
store 3
gtxna 0 ApplicationArgs 5
store 4
gtxna 0 ApplicationArgs 8
store 5
int 0
store 6
gtxna 0 ApplicationArgs 6
dup
substring 0 32
store 255
dup
substring 32 40
btoi
store 254
dup
substring 40 72
store 253
dup
substring 72 80
btoi
store 252
pop
gtxna 0 ApplicationArgs 7
dup
substring 0 32
store 251
dup
substring 32 40
btoi
store 250
pop
// Handler 4
// Check txnAppl
gtxn 0 TypeEnum
int appl
==
assert
gtxn 0 ApplicationID
byte "{{ApplicationID}}"
btoi
==
assert
gtxn 0 NumAppArgs
int 9
==
assert
// Check txnToHandler
gtxn 1 TypeEnum
int pay
==
assert
gtxn 1 Receiver
txn Sender
==
assert
gtxn 1 Amount
gtxn 2 Fee
int 100000
+
==
assert
// Check txnFromHandler (us)
txn GroupIndex
int 2
==
assert
txn TypeEnum
int pay
==
assert
txn Amount
int 0
==
assert
txn Receiver
gtxn 1 Sender
==
assert
// compute state in HM_Check 3
int 3
itob
load 255
concat
load 254
itob
concat
load 253
concat
load 252
itob
concat
keccak256
gtxna 0 ApplicationArgs 0
==
assert
txn CloseRemainderTo
gtxn 1 Sender
==
assert
// Run body
// "CheckPay"
// "./src/rsh/battleRecord.rsh:266:11:dot"
// "[]"
gtxn 3 TypeEnum
int pay
==
assert
gtxn 3 Receiver
byte "{{ContractAddr}}"
==
assert
gtxn 3 Amount
load 3
btoi
==
assert
// We don't care who the sender is... this means that you can get other people to pay for you if you want.
// Just "sender correct"
// "./src/rsh/battleRecord.rsh:266:11:dot"
// "[]"
load 253
gtxn 0 Sender
==
assert
byte base64()
load 1
==
assert
// compute state in HM_Set 5
int 5
itob
load 255
concat
load 254
itob
concat
load 253
concat
load 252
itob
concat
load 251
concat
load 250
itob
concat
keccak256
load 0
==
assert
load 2
btoi
int 0
==
assert
// Check GroupSize
global GroupSize
int 4
==
assert
load 3
btoi
int 0
==
assert
// Check time limits
load 4
btoi
int 5
+
dup
gtxn 0 LastValid
>=
assert
dup
gtxn 1 LastValid
>=
assert
dup
gtxn 2 LastValid
>=
assert
dup
gtxn 3 LastValid
>=
assert
pop
checkAccts:
gtxn 0 NumAccounts
load 6
==
assert
done:
int 1
return
`, `#pragma version 3
gtxna 0 ApplicationArgs 1
store 0
gtxna 0 ApplicationArgs 2
store 1
gtxna 0 ApplicationArgs 3
store 2
gtxna 0 ApplicationArgs 4
store 3
gtxna 0 ApplicationArgs 5
store 4
gtxna 0 ApplicationArgs 8
store 5
int 0
store 6
gtxna 0 ApplicationArgs 6
dup
substring 0 32
store 255
dup
substring 32 40
btoi
store 254
dup
substring 40 72
store 253
dup
substring 72 80
btoi
store 252
pop
// Handler 5
// Check txnAppl
gtxn 0 TypeEnum
int appl
==
assert
gtxn 0 ApplicationID
byte "{{ApplicationID}}"
btoi
==
assert
gtxn 0 NumAppArgs
int 9
==
assert
// Check txnToHandler
gtxn 1 TypeEnum
int pay
==
assert
gtxn 1 Receiver
txn Sender
==
assert
gtxn 1 Amount
gtxn 2 Fee
int 100000
+
==
assert
// Check txnFromHandler (us)
txn GroupIndex
int 2
==
assert
txn TypeEnum
int pay
==
assert
txn Amount
int 0
==
assert
txn Receiver
gtxn 1 Sender
==
assert
// compute state in HM_Check 3
int 3
itob
load 255
concat
load 254
itob
concat
load 253
concat
load 252
itob
concat
keccak256
gtxna 0 ApplicationArgs 0
==
assert
txn CloseRemainderTo
gtxn 1 Sender
==
assert
// Run body
// "CheckPay"
// "reach standard library:209:7:dot"
// "[at ./src/rsh/battleRecord.rsh:266:72:application call to \"closeTo\" (defined at: reach standard library:207:8:function exp)]"
gtxn 3 TypeEnum
int pay
==
assert
gtxn 3 Receiver
byte "{{ContractAddr}}"
==
assert
gtxn 3 Amount
load 3
btoi
==
assert
// We don't care who the sender is... this means that you can get other people to pay for you if you want.
// Just "sender correct"
// "reach standard library:209:7:dot"
// "[at ./src/rsh/battleRecord.rsh:266:72:application call to \"closeTo\" (defined at: reach standard library:207:8:function exp)]"
load 255
gtxn 0 Sender
==
assert
gtxn 4 TypeEnum
int pay
==
assert
gtxn 4 Receiver
load 255
==
assert
gtxn 4 Amount
load 252
==
assert
gtxn 4 Sender
byte "{{ContractAddr}}"
==
assert
byte base64()
load 1
==
assert
gtxn 5 TypeEnum
int pay
==
assert
// We don't check the receiver
gtxn 5 Amount
int 0
==
assert
gtxn 5 Sender
byte "{{ContractAddr}}"
==
assert
gtxn 5 CloseRemainderTo
byte "{{Deployer}}"
==
assert
load 2
btoi
int 1
==
assert
// Check GroupSize
global GroupSize
int 6
==
assert
load 3
btoi
gtxn 4 Fee
gtxn 5 Fee
+
==
assert
// Check time limits
load 4
btoi
int 5
+
dup
gtxn 0 FirstValid
<=
assert
dup
gtxn 1 FirstValid
<=
assert
dup
gtxn 2 FirstValid
<=
assert
dup
gtxn 3 FirstValid
<=
assert
dup
gtxn 4 FirstValid
<=
assert
dup
gtxn 5 FirstValid
<=
assert
pop
checkAccts:
gtxn 0 NumAccounts
load 6
==
assert
done:
int 1
return
`, `#pragma version 3
gtxna 0 ApplicationArgs 1
store 0
gtxna 0 ApplicationArgs 2
store 1
gtxna 0 ApplicationArgs 3
store 2
gtxna 0 ApplicationArgs 4
store 3
gtxna 0 ApplicationArgs 5
store 4
gtxna 0 ApplicationArgs 8
store 5
int 0
store 6
gtxna 0 ApplicationArgs 6
dup
substring 0 32
store 255
dup
substring 32 40
btoi
store 254
dup
substring 40 72
store 253
dup
substring 72 80
btoi
store 252
dup
substring 80 112
store 251
dup
substring 112 120
btoi
store 250
pop
gtxna 0 ApplicationArgs 7
dup
substring 0 32
store 249
dup
substring 32 40
btoi
store 248
pop
// Handler 6
// Check txnAppl
gtxn 0 TypeEnum
int appl
==
assert
gtxn 0 ApplicationID
byte "{{ApplicationID}}"
btoi
==
assert
gtxn 0 NumAppArgs
int 9
==
assert
// Check txnToHandler
gtxn 1 TypeEnum
int pay
==
assert
gtxn 1 Receiver
txn Sender
==
assert
gtxn 1 Amount
gtxn 2 Fee
int 100000
+
==
assert
// Check txnFromHandler (us)
txn GroupIndex
int 2
==
assert
txn TypeEnum
int pay
==
assert
txn Amount
int 0
==
assert
txn Receiver
gtxn 1 Sender
==
assert
// compute state in HM_Check 5
int 5
itob
load 255
concat
load 254
itob
concat
load 253
concat
load 252
itob
concat
load 251
concat
load 250
itob
concat
keccak256
gtxna 0 ApplicationArgs 0
==
assert
txn CloseRemainderTo
gtxn 1 Sender
==
assert
// Run body
// "CheckPay"
// "./src/rsh/battleRecord.rsh:277:11:dot"
// "[]"
gtxn 3 TypeEnum
int pay
==
assert
gtxn 3 Receiver
byte "{{ContractAddr}}"
==
assert
gtxn 3 Amount
load 3
btoi
==
assert
// We don't care who the sender is... this means that you can get other people to pay for you if you want.
// Just "sender correct"
// "./src/rsh/battleRecord.rsh:277:11:dot"
// "[]"
load 255
gtxn 0 Sender
==
assert
int 1
int 0
>
int 0
load 248
==
!
&&
int 0
load 250
==
!
&&
bz l0
int 1
int 2
%
int 0
==
bz l1
byte base64()
load 1
==
assert
// compute state in HM_Set 16
int 16
itob
load 255
concat
load 254
itob
concat
load 253
concat
load 251
concat
load 250
itob
concat
load 249
concat
load 248
itob
concat
int 0
itob
concat
int 0
itob
concat
int 0
itob
concat
int 0
itob
concat
int 0
itob
concat
int 0
itob
concat
int 1
itob
concat
int 2
itob
concat
int 1
itob
concat
int 50
itob
concat
keccak256
load 0
==
assert
load 2
btoi
int 0
==
assert
// Check GroupSize
global GroupSize
int 4
==
assert
load 3
btoi
int 0
==
assert
// Check time limits
load 4
btoi
int 5
+
dup
gtxn 0 LastValid
>=
assert
dup
gtxn 1 LastValid
>=
assert
dup
gtxn 2 LastValid
>=
assert
dup
gtxn 3 LastValid
>=
assert
pop
b checkAccts
l1:
byte base64()
load 1
==
assert
// compute state in HM_Set 18
int 18
itob
load 255
concat
load 254
itob
concat
load 253
concat
load 251
concat
load 250
itob
concat
load 249
concat
load 248
itob
concat
int 0
itob
concat
int 0
itob
concat
int 0
itob
concat
int 0
itob
concat
int 0
itob
concat
int 0
itob
concat
int 1
itob
concat
int 2
itob
concat
int 1
itob
concat
int 50
itob
concat
keccak256
load 0
==
assert
load 2
btoi
int 0
==
assert
// Check GroupSize
global GroupSize
int 4
==
assert
load 3
btoi
int 0
==
assert
// Check time limits
load 4
btoi
int 5
+
dup
gtxn 0 LastValid
>=
assert
dup
gtxn 1 LastValid
>=
assert
dup
gtxn 2 LastValid
>=
assert
dup
gtxn 3 LastValid
>=
assert
pop
b checkAccts
l0:
byte base64()
load 1
==
assert
// compute state in HM_Set 9
int 9
itob
load 255
concat
load 254
itob
concat
load 253
concat
load 251
concat
load 249
concat
int 0
itob
concat
int 0
itob
concat
int 0
itob
concat
int 0
itob
concat
keccak256
load 0
==
assert
load 2
btoi
int 0
==
assert
// Check GroupSize
global GroupSize
int 4
==
assert
load 3
btoi
int 0
==
assert
// Check time limits
load 4
btoi
int 5
+
dup
gtxn 0 LastValid
>=
assert
dup
gtxn 1 LastValid
>=
assert
dup
gtxn 2 LastValid
>=
assert
dup
gtxn 3 LastValid
>=
assert
pop
checkAccts:
gtxn 0 NumAccounts
load 6
==
assert
done:
int 1
return
`, `#pragma version 3
gtxna 0 ApplicationArgs 1
store 0
gtxna 0 ApplicationArgs 2
store 1
gtxna 0 ApplicationArgs 3
store 2
gtxna 0 ApplicationArgs 4
store 3
gtxna 0 ApplicationArgs 5
store 4
gtxna 0 ApplicationArgs 8
store 5
int 0
store 6
gtxna 0 ApplicationArgs 6
dup
substring 0 32
store 255
dup
substring 32 40
btoi
store 254
dup
substring 40 72
store 253
dup
substring 72 80
btoi
store 252
dup
substring 80 112
store 251
dup
substring 112 120
btoi
store 250
pop
// Handler 7
// Check txnAppl
gtxn 0 TypeEnum
int appl
==
assert
gtxn 0 ApplicationID
byte "{{ApplicationID}}"
btoi
==
assert
gtxn 0 NumAppArgs
int 9
==
assert
// Check txnToHandler
gtxn 1 TypeEnum
int pay
==
assert
gtxn 1 Receiver
txn Sender
==
assert
gtxn 1 Amount
gtxn 2 Fee
int 100000
+
==
assert
// Check txnFromHandler (us)
txn GroupIndex
int 2
==
assert
txn TypeEnum
int pay
==
assert
txn Amount
int 0
==
assert
txn Receiver
gtxn 1 Sender
==
assert
// compute state in HM_Check 5
int 5
itob
load 255
concat
load 254
itob
concat
load 253
concat
load 252
itob
concat
load 251
concat
load 250
itob
concat
keccak256
gtxna 0 ApplicationArgs 0
==
assert
txn CloseRemainderTo
gtxn 1 Sender
==
assert
// Run body
// "CheckPay"
// "reach standard library:209:7:dot"
// "[at ./src/rsh/battleRecord.rsh:277:72:application call to \"closeTo\" (defined at: reach standard library:207:8:function exp)]"
gtxn 3 TypeEnum
int pay
==
assert
gtxn 3 Receiver
byte "{{ContractAddr}}"
==
assert
gtxn 3 Amount
load 3
btoi
==
assert
// We don't care who the sender is... this means that you can get other people to pay for you if you want.
// Just "sender correct"
// "reach standard library:209:7:dot"
// "[at ./src/rsh/battleRecord.rsh:277:72:application call to \"closeTo\" (defined at: reach standard library:207:8:function exp)]"
load 253
gtxn 0 Sender
==
assert
gtxn 4 TypeEnum
int pay
==
assert
gtxn 4 Receiver
load 253
==
assert
gtxn 4 Amount
load 252
==
assert
gtxn 4 Sender
byte "{{ContractAddr}}"
==
assert
byte base64()
load 1
==
assert
gtxn 5 TypeEnum
int pay
==
assert
// We don't check the receiver
gtxn 5 Amount
int 0
==
assert
gtxn 5 Sender
byte "{{ContractAddr}}"
==
assert
gtxn 5 CloseRemainderTo
byte "{{Deployer}}"
==
assert
load 2
btoi
int 1
==
assert
// Check GroupSize
global GroupSize
int 6
==
assert
load 3
btoi
gtxn 4 Fee
gtxn 5 Fee
+
==
assert
// Check time limits
load 4
btoi
int 5
+
dup
gtxn 0 FirstValid
<=
assert
dup
gtxn 1 FirstValid
<=
assert
dup
gtxn 2 FirstValid
<=
assert
dup
gtxn 3 FirstValid
<=
assert
dup
gtxn 4 FirstValid
<=
assert
dup
gtxn 5 FirstValid
<=
assert
pop
checkAccts:
gtxn 0 NumAccounts
load 6
==
assert
done:
int 1
return
`, null, null, `#pragma version 3
gtxna 0 ApplicationArgs 1
store 0
gtxna 0 ApplicationArgs 2
store 1
gtxna 0 ApplicationArgs 3
store 2
gtxna 0 ApplicationArgs 4
store 3
gtxna 0 ApplicationArgs 5
store 4
gtxna 0 ApplicationArgs 8
store 5
int 0
store 6
gtxna 0 ApplicationArgs 6
dup
substring 0 32
store 255
dup
substring 32 40
btoi
store 254
dup
substring 40 72
store 253
dup
substring 72 104
store 252
dup
substring 104 136
store 251
dup
substring 136 144
btoi
store 250
dup
substring 144 152
btoi
store 249
dup
substring 152 160
btoi
store 248
dup
substring 160 168
btoi
store 247
pop
gtxna 0 ApplicationArgs 7
dup
substring 0 8
btoi
store 246
dup
substring 8 16
btoi
store 245
pop
// Handler 10
// Check txnAppl
gtxn 0 TypeEnum
int appl
==
assert
gtxn 0 ApplicationID
byte "{{ApplicationID}}"
btoi
==
assert
gtxn 0 NumAppArgs
int 9
==
assert
// Check txnToHandler
gtxn 1 TypeEnum
int pay
==
assert
gtxn 1 Receiver
txn Sender
==
assert
gtxn 1 Amount
gtxn 2 Fee
int 100000
+
==
assert
// Check txnFromHandler (us)
txn GroupIndex
int 2
==
assert
txn TypeEnum
int pay
==
assert
txn Amount
int 0
==
assert
txn Receiver
gtxn 1 Sender
==
assert
// compute state in HM_Check 9
int 9
itob
load 255
concat
load 254
itob
concat
load 253
concat
load 252
concat
load 251
concat
load 250
itob
concat
load 249
itob
concat
load 248
itob
concat
load 247
itob
concat
keccak256
gtxna 0 ApplicationArgs 0
==
assert
txn CloseRemainderTo
gtxn 1 Sender
==
assert
// Run body
// "CheckPay"
// "./src/rsh/battleRecord.rsh:454:11:dot"
// "[]"
gtxn 3 TypeEnum
int pay
==
assert
gtxn 3 Receiver
byte "{{ContractAddr}}"
==
assert
gtxn 3 Amount
load 3
btoi
==
assert
// We don't care who the sender is... this means that you can get other people to pay for you if you want.
// Just "sender correct"
// "./src/rsh/battleRecord.rsh:454:11:dot"
// "[]"
load 255
gtxn 0 Sender
==
assert
// Nothing
// "reach standard library:65:17:application"
// "[at ./src/rsh/battleRecord.rsh:456:24:application call to \"checkCommitment\" (defined at: reach standard library:64:8:function exp)]"
load 251
load 246
itob
load 245
itob
concat
keccak256
==
assert
byte base64()
load 1
==
assert
// compute state in HM_Set 10
int 10
itob
load 255
concat
load 254
itob
concat
load 253
concat
load 252
concat
load 250
itob
concat
load 249
itob
concat
load 248
itob
concat
load 247
itob
concat
load 245
itob
concat
keccak256
load 0
==
assert
load 2
btoi
int 0
==
assert
// Check GroupSize
global GroupSize
int 4
==
assert
load 3
btoi
int 0
==
assert
// Check time limits
checkAccts:
gtxn 0 NumAccounts
load 6
==
assert
done:
int 1
return
`, `#pragma version 3
gtxna 0 ApplicationArgs 1
store 0
gtxna 0 ApplicationArgs 2
store 1
gtxna 0 ApplicationArgs 3
store 2
gtxna 0 ApplicationArgs 4
store 3
gtxna 0 ApplicationArgs 5
store 4
gtxna 0 ApplicationArgs 8
store 5
int 0
store 6
gtxna 0 ApplicationArgs 6
dup
substring 0 32
store 255
dup
substring 32 40
btoi
store 254
dup
substring 40 72
store 253
dup
substring 72 104
store 252
dup
substring 104 112
btoi
store 251
dup
substring 112 120
btoi
store 250
dup
substring 120 128
btoi
store 249
dup
substring 128 136
btoi
store 248
dup
substring 136 144
btoi
store 247
pop
gtxna 0 ApplicationArgs 7
dup
substring 0 8
btoi
store 246
dup
substring 8 16
btoi
store 245
pop
// Handler 11
// Check txnAppl
gtxn 0 TypeEnum
int appl
==
assert
gtxn 0 ApplicationID
byte "{{ApplicationID}}"
btoi
==
assert
gtxn 0 NumAppArgs
int 9
==
assert
// Check txnToHandler
gtxn 1 TypeEnum
int pay
==
assert
gtxn 1 Receiver
txn Sender
==
assert
gtxn 1 Amount
gtxn 2 Fee
int 100000
+
==
assert
// Check txnFromHandler (us)
txn GroupIndex
int 2
==
assert
txn TypeEnum
int pay
==
assert
txn Amount
int 0
==
assert
txn Receiver
gtxn 1 Sender
==
assert
// compute state in HM_Check 10
int 10
itob
load 255
concat
load 254
itob
concat
load 253
concat
load 252
concat
load 251
itob
concat
load 250
itob
concat
load 249
itob
concat
load 248
itob
concat
load 247
itob
concat
keccak256
gtxna 0 ApplicationArgs 0
==
assert
txn CloseRemainderTo
gtxn 1 Sender
==
assert
// Run body
// "CheckPay"
// "./src/rsh/battleRecord.rsh:462:11:dot"
// "[]"
gtxn 3 TypeEnum
int pay
==
assert
gtxn 3 Receiver
byte "{{ContractAddr}}"
==
assert
gtxn 3 Amount
load 3
btoi
==
assert
// We don't care who the sender is... this means that you can get other people to pay for you if you want.
// Just "sender correct"
// "./src/rsh/battleRecord.rsh:462:11:dot"
// "[]"
load 253
gtxn 0 Sender
==
assert
// Nothing
// "reach standard library:65:17:application"
// "[at ./src/rsh/battleRecord.rsh:464:24:application call to \"checkCommitment\" (defined at: reach standard library:64:8:function exp)]"
load 252
load 246
itob
load 245
itob
concat
keccak256
==
assert
load 250
load 248
==
bz l0
load 250
int 0
>
bz l2
load 247
load 251
>
bz l4
load 247
load 251
-
store 242
load 245
load 249
>
bz l6
load 242
itob
int 0
itob
concat
load 245
load 249
-
itob
concat
int 0
itob
concat
store 243
b l7
l6:
load 242
itob
int 0
itob
concat
load 249
load 245
-
itob
concat
int 1
itob
concat
store 243
l7:
b l5
l4:
load 251
load 247
-
store 242
load 245
load 249
>
bz l8
load 242
itob
int 1
itob
concat
load 245
load 249
-
itob
concat
int 0
itob
concat
store 243
b l9
l8:
load 242
itob
int 1
itob
concat
load 249
load 245
-
itob
concat
int 1
itob
concat
store 243
l9:
l5:
b l3
l2:
load 251
load 247
+
itob
int 0
itob
concat
load 249
load 245
+
itob
concat
int 0
itob
concat
store 243
l3:
b l1
l0:
load 250
load 248
>
bz l10
load 245
load 249
+
store 242
load 247
load 251
>
bz l12
load 247
load 251
-
itob
int 0
itob
concat
load 242
itob
concat
int 0
itob
concat
store 243
b l13
l12:
load 251
load 247
-
itob
int 1
itob
concat
load 242
itob
concat
int 0
itob
concat
store 243
l13:
b l11
l10:
load 247
load 251
+
store 242
load 245
load 249
>
bz l14
load 242
itob
int 0
itob
concat
load 245
load 249
-
itob
concat
int 0
itob
concat
store 243
b l15
l14:
load 242
itob
int 0
itob
concat
load 249
load 245
-
itob
concat
int 1
itob
concat
store 243
l15:
l11:
l1:
load 243
substring 0 8
btoi
store 242
load 243
substring 8 16
btoi
store 241
load 243
substring 16 24
btoi
store 240
load 243
substring 24 32
btoi
store 239
load 241
load 239
==
store 238
load 242
load 240
==
load 238
&&
bz l16
gtxn 4 TypeEnum
int pay
==
assert
gtxn 4 Receiver
load 255
==
assert
gtxn 4 Amount
int 2
load 254
*
==
assert
gtxn 4 Sender
byte "{{ContractAddr}}"
==
assert
byte base64()
load 1
==
assert
gtxn 5 TypeEnum
int pay
==
assert
// We don't check the receiver
gtxn 5 Amount
int 0
==
assert
gtxn 5 Sender
byte "{{ContractAddr}}"
==
assert
gtxn 5 CloseRemainderTo
byte "{{Deployer}}"
==
assert
load 2
btoi
int 1
==
assert
// Check GroupSize
global GroupSize
int 6
==
assert
load 3
btoi
gtxn 4 Fee
gtxn 5 Fee
+
==
assert
// Check time limits
b checkAccts
l16:
load 241
load 239
>
bz l17
gtxn 4 TypeEnum
int pay
==
assert
gtxn 4 Receiver
load 253
==
assert
gtxn 4 Amount
int 2
load 254
*
==
assert
gtxn 4 Sender
byte "{{ContractAddr}}"
==
assert
byte base64()
load 1
==
assert
gtxn 5 TypeEnum
int pay
==
assert
// We don't check the receiver
gtxn 5 Amount
int 0
==
assert
gtxn 5 Sender
byte "{{ContractAddr}}"
==
assert
gtxn 5 CloseRemainderTo
byte "{{Deployer}}"
==
assert
load 2
btoi
int 1
==
assert
// Check GroupSize
global GroupSize
int 6
==
assert
load 3
btoi
gtxn 4 Fee
gtxn 5 Fee
+
==
assert
// Check time limits
b checkAccts
l17:
load 238
bz l18
load 241
int 0
>
bz l19
gtxn 4 TypeEnum
int pay
==
assert
gtxn 4 Receiver
load 255
load 253
load 242
load 240
>
select
==
assert
gtxn 4 Amount
int 2
load 254
*
==
assert
gtxn 4 Sender
byte "{{ContractAddr}}"
==
assert
byte base64()
load 1
==
assert
gtxn 5 TypeEnum
int pay
==
assert
// We don't check the receiver
gtxn 5 Amount
int 0
==
assert
gtxn 5 Sender
byte "{{ContractAddr}}"
==
assert
gtxn 5 CloseRemainderTo
byte "{{Deployer}}"
==
assert
load 2
btoi
int 1
==
assert
// Check GroupSize
global GroupSize
int 6
==
assert
load 3
btoi
gtxn 4 Fee
gtxn 5 Fee
+
==
assert
// Check time limits
b checkAccts
l19:
gtxn 4 TypeEnum
int pay
==
assert
gtxn 4 Receiver
load 253
load 255
load 242
load 240
>
select
==
assert
gtxn 4 Amount
int 2
load 254
*
==
assert
gtxn 4 Sender
byte "{{ContractAddr}}"
==
assert
byte base64()
load 1
==
assert
gtxn 5 TypeEnum
int pay
==
assert
// We don't check the receiver
gtxn 5 Amount
int 0
==
assert
gtxn 5 Sender
byte "{{ContractAddr}}"
==
assert
gtxn 5 CloseRemainderTo
byte "{{Deployer}}"
==
assert
load 2
btoi
int 1
==
assert
// Check GroupSize
global GroupSize
int 6
==
assert
load 3
btoi
gtxn 4 Fee
gtxn 5 Fee
+
==
assert
// Check time limits
b checkAccts
l18:
gtxn 4 TypeEnum
int pay
==
assert
gtxn 4 Receiver
load 255
==
assert
gtxn 4 Amount
int 2
load 254
*
==
assert
gtxn 4 Sender
byte "{{ContractAddr}}"
==
assert
byte base64()
load 1
==
assert
gtxn 5 TypeEnum
int pay
==
assert
// We don't check the receiver
gtxn 5 Amount
int 0
==
assert
gtxn 5 Sender
byte "{{ContractAddr}}"
==
assert
gtxn 5 CloseRemainderTo
byte "{{Deployer}}"
==
assert
load 2
btoi
int 1
==
assert
// Check GroupSize
global GroupSize
int 6
==
assert
load 3
btoi
gtxn 4 Fee
gtxn 5 Fee
+
==
assert
// Check time limits
checkAccts:
gtxn 0 NumAccounts
load 6
==
assert
done:
int 1
return
`, `#pragma version 3
gtxna 0 ApplicationArgs 1
store 0
gtxna 0 ApplicationArgs 2
store 1
gtxna 0 ApplicationArgs 3
store 2
gtxna 0 ApplicationArgs 4
store 3
gtxna 0 ApplicationArgs 5
store 4
gtxna 0 ApplicationArgs 8
store 5
int 0
store 6
gtxna 0 ApplicationArgs 6
dup
substring 0 32
store 255
dup
substring 32 40
btoi
store 254
dup
substring 40 72
store 253
dup
substring 72 104
store 252
dup
substring 104 112
btoi
store 251
dup
substring 112 144
store 250
dup
substring 144 152
btoi
store 249
dup
substring 152 160
btoi
store 248
dup
substring 160 168
btoi
store 247
dup
substring 168 176
btoi
store 246
dup
substring 176 184
btoi
store 245
dup
substring 184 192
btoi
store 244
dup
substring 192 200
btoi
store 243
dup
substring 200 208
btoi
store 242
dup
substring 208 216
btoi
store 241
dup
substring 216 224
btoi
store 240
dup
substring 224 232
btoi
store 239
pop
gtxna 0 ApplicationArgs 7
dup
substring 0 8
btoi
store 238
dup
substring 8 16
btoi
store 237
dup
substring 16 24
btoi
store 236
dup
substring 24 32
btoi
store 235
dup
substring 32 40
btoi
store 234
dup
substring 40 48
btoi
store 233
dup
substring 48 56
btoi
store 232
dup
substring 56 64
btoi
store 231
dup
substring 64 72
btoi
store 230
dup
substring 72 80
btoi
store 229
dup
substring 80 88
btoi
store 228
dup
substring 88 96
btoi
store 227
dup
substring 96 104
btoi
store 226
pop
// Handler 12
// Check txnAppl
gtxn 0 TypeEnum
int appl
==
assert
gtxn 0 ApplicationID
byte "{{ApplicationID}}"
btoi
==
assert
gtxn 0 NumAppArgs
int 9
==
assert
// Check txnToHandler
gtxn 1 TypeEnum
int pay
==
assert
gtxn 1 Receiver
txn Sender
==
assert
gtxn 1 Amount
gtxn 2 Fee
int 100000
+
==
assert
// Check txnFromHandler (us)
txn GroupIndex
int 2
==
assert
txn TypeEnum
int pay
==
assert
txn Amount
int 0
==
assert
txn Receiver
gtxn 1 Sender
==
assert
// compute state in HM_Check 16
int 16
itob
load 255
concat
load 254
itob
concat
load 253
concat
load 252
concat
load 251
itob
concat
load 250
concat
load 249
itob
concat
load 248
itob
concat
load 247
itob
concat
load 246
itob
concat
load 245
itob
concat
load 244
itob
concat
load 243
itob
concat
load 242
itob
concat
load 241
itob
concat
load 240
itob
concat
load 239
itob
concat
keccak256
gtxna 0 ApplicationArgs 0
==
assert
txn CloseRemainderTo
gtxn 1 Sender
==
assert
// Run body
// "CheckPay"
// "./src/rsh/battleRecord.rsh:402:19:dot"
// "[]"
gtxn 3 TypeEnum
int pay
==
assert
gtxn 3 Receiver
byte "{{ContractAddr}}"
==
assert
gtxn 3 Amount
load 3
btoi
==
assert
// We don't care who the sender is... this means that you can get other people to pay for you if you want.
// Just "sender correct"
// "./src/rsh/battleRecord.rsh:402:19:dot"
// "[]"
load 253
gtxn 0 Sender
==
assert
byte base64()
load 1
==
assert
// compute state in HM_Set 17
int 17
itob
load 255
concat
load 254
itob
concat
load 253
concat
load 252
concat
load 251
itob
concat
load 250
concat
load 249
itob
concat
load 248
itob
concat
load 247
itob
concat
load 246
itob
concat
load 245
itob
concat
load 244
itob
concat
load 243
itob
concat
load 242
itob
concat
load 241
itob
concat
load 240
itob
concat
load 239
itob
concat
load 237
itob
concat
load 236
itob
concat
load 234
itob
concat
load 233
itob
concat
load 231
itob
concat
load 230
itob
concat
load 228
itob
concat
keccak256
load 0
==
assert
load 2
btoi
int 0
==
assert
// Check GroupSize
global GroupSize
int 4
==
assert
load 3
btoi
int 0
==
assert
// Check time limits
checkAccts:
gtxn 0 NumAccounts
load 6
==
assert
done:
int 1
return
`, `#pragma version 3
gtxna 0 ApplicationArgs 1
store 0
gtxna 0 ApplicationArgs 2
store 1
gtxna 0 ApplicationArgs 3
store 2
gtxna 0 ApplicationArgs 4
store 3
gtxna 0 ApplicationArgs 5
store 4
gtxna 0 ApplicationArgs 8
store 5
int 0
store 6
gtxna 0 ApplicationArgs 6
dup
substring 0 32
store 255
dup
substring 32 40
btoi
store 254
dup
substring 40 72
store 253
dup
substring 72 104
store 252
dup
substring 104 112
btoi
store 251
dup
substring 112 144
store 250
dup
substring 144 152
btoi
store 249
dup
substring 152 160
btoi
store 248
dup
substring 160 168
btoi
store 247
dup
substring 168 176
btoi
store 246
dup
substring 176 184
btoi
store 245
dup
substring 184 192
btoi
store 244
dup
substring 192 200
btoi
store 243
dup
substring 200 208
btoi
store 242
dup
substring 208 216
btoi
store 241
dup
substring 216 224
btoi
store 240
dup
substring 224 232
btoi
store 239
dup
substring 232 240
btoi
store 238
dup
substring 240 248
btoi
store 237
dup
int 248
int 256
substring3
btoi
store 236
dup
int 256
int 264
substring3
btoi
store 235
dup
int 264
int 272
substring3
btoi
store 234
dup
int 272
int 280
substring3
btoi
store 233
dup
int 280
int 288
substring3
btoi
store 232
pop
gtxna 0 ApplicationArgs 7
dup
substring 0 8
btoi
store 231
dup
substring 8 16
btoi
store 230
dup
substring 16 24
btoi
store 229
dup
substring 24 32
btoi
store 228
dup
substring 32 40
btoi
store 227
dup
substring 40 48
btoi
store 226
dup
substring 48 56
btoi
store 225
dup
substring 56 64
btoi
store 224
dup
substring 64 72
btoi
store 223
dup
substring 72 80
btoi
store 222
dup
substring 80 88
btoi
store 221
dup
substring 88 96
btoi
store 220
pop
// Handler 13
// Check txnAppl
gtxn 0 TypeEnum
int appl
==
assert
gtxn 0 ApplicationID
byte "{{ApplicationID}}"
btoi
==
assert
gtxn 0 NumAppArgs
int 9
==
assert
// Check txnToHandler
gtxn 1 TypeEnum
int pay
==
assert
gtxn 1 Receiver
txn Sender
==
assert
gtxn 1 Amount
gtxn 2 Fee
int 100000
+
==
assert
// Check txnFromHandler (us)
txn GroupIndex
int 2
==
assert
txn TypeEnum
int pay
==
assert
txn Amount
int 0
==
assert
txn Receiver
gtxn 1 Sender
==
assert
// compute state in HM_Check 17
int 17
itob
load 255
concat
load 254
itob
concat
load 253
concat
load 252
concat
load 251
itob
concat
load 250
concat
load 249
itob
concat
load 248
itob
concat
load 247
itob
concat
load 246
itob
concat
load 245
itob
concat
load 244
itob
concat
load 243
itob
concat
load 242
itob
concat
load 241
itob
concat
load 240
itob
concat
load 239
itob
concat
load 238
itob
concat
load 237
itob
concat
load 236
itob
concat
load 235
itob
concat
load 234
itob
concat
load 233
itob
concat
load 232
itob
concat
keccak256
gtxna 0 ApplicationArgs 0
==
assert
txn CloseRemainderTo
gtxn 1 Sender
==
assert
// Run body
// "CheckPay"
// "./src/rsh/battleRecord.rsh:429:19:dot"
// "[]"
gtxn 3 TypeEnum
int pay
==
assert
gtxn 3 Receiver
byte "{{ContractAddr}}"
==
assert
gtxn 3 Amount
load 3
btoi
==
assert
// We don't care who the sender is... this means that you can get other people to pay for you if you want.
// Just "sender correct"
// "./src/rsh/battleRecord.rsh:429:19:dot"
// "[]"
load 255
gtxn 0 Sender
==
assert
int 0
load 238
<
load 238
load 239
<
&&
bz l0
load 246
itob
load 245
itob
concat
load 248
itob
concat
load 247
itob
concat
store 215
b l1
l0:
load 238
load 230
==
bz l2
load 246
itob
load 245
itob
concat
load 248
itob
concat
load 247
itob
concat
store 215
b l3
l2:
load 237
int 0
>
bz l4
load 247
int 0
>
bz l6
load 246
itob
load 245
itob
concat
load 248
load 238
+
itob
concat
load 247
itob
concat
store 215
b l7
l6:
load 248
load 238
>
bz l8
load 246
itob
load 245
itob
concat
load 248
load 238
-
itob
concat
load 247
itob
concat
store 215
b l9
l8:
load 246
itob
load 245
itob
concat
load 238
load 248
-
itob
concat
int 1
itob
concat
store 215
l9:
l7:
b l5
l4:
load 245
int 0
>
bz l10
load 246
load 238
>
bz l12
load 246
load 238
-
itob
load 245
itob
concat
load 248
itob
concat
load 247
itob
concat
store 215
b l13
l12:
load 238
load 246
-
itob
int 0
itob
concat
load 248
itob
concat
load 247
itob
concat
store 215
l13:
b l11
l10:
load 246
load 238
+
itob
load 245
itob
concat
load 248
itob
concat
load 247
itob
concat
store 215
l11:
l5:
l3:
l1:
load 215
substring 0 8
btoi
store 214
load 215
substring 8 16
btoi
store 213
load 215
substring 16 24
btoi
store 212
load 215
substring 24 32
btoi
store 211
int 0
load 236
<
load 236
load 239
<
&&
bz l14
load 214
itob
load 213
itob
concat
load 212
itob
concat
load 211
itob
concat
store 210
b l15
l14:
load 236
load 227
==
bz l16
load 214
itob
load 213
itob
concat
load 212
itob
concat
load 211
itob
concat
store 210
b l17
l16:
load 235
int 0
>
bz l18
load 211
int 0
>
bz l20
load 214
itob
load 213
itob
concat
load 212
load 236
+
itob
concat
load 211
itob
concat
store 210
b l21
l20:
load 212
load 236
>
bz l22
load 214
itob
load 213
itob
concat
load 212
load 236
-
itob
concat
load 211
itob
concat
store 210
b l23
l22:
load 214
itob
load 213
itob
concat
load 236
load 212
-
itob
concat
int 1
itob
concat
store 210
l23:
l21:
b l19
l18:
load 213
int 0
>
bz l24
load 214
load 236
>
bz l26
load 214
load 236
-
itob
load 213
itob
concat
load 212
itob
concat
load 211
itob
concat
store 210
b l27
l26:
load 236
load 214
-
itob
int 0
itob
concat
load 212
itob
concat
load 211
itob
concat
store 210
l27:
b l25
l24:
load 214
load 236
+
itob
load 213
itob
concat
load 212
itob
concat
load 211
itob
concat
store 210
l25:
l19:
l17:
l15:
load 210
substring 0 8
btoi
store 209
load 210
substring 8 16
btoi
store 208
load 210
substring 16 24
btoi
store 207
load 210
substring 24 32
btoi
store 206
int 0
load 234
<
load 234
load 239
<
&&
bz l28
load 209
itob
load 208
itob
concat
load 207
itob
concat
load 206
itob
concat
store 205
b l29
l28:
load 234
load 224
==
bz l30
load 209
itob
load 208
itob
concat
load 207
itob
concat
load 206
itob
concat
store 205
b l31
l30:
load 233
int 0
>
bz l32
load 206
int 0
>
bz l34
load 209
itob
load 208
itob
concat
load 207
load 234
+
itob
concat
load 206
itob
concat
store 205
b l35
l34:
load 207
load 234
>
bz l36
load 209
itob
load 208
itob
concat
load 207
load 234
-
itob
concat
load 206
itob
concat
store 205
b l37
l36:
load 209
itob
load 208
itob
concat
load 234
load 207
-
itob
concat
int 1
itob
concat
store 205
l37:
l35:
b l33
l32:
load 208
int 0
>
bz l38
load 209
load 234
>
bz l40
load 209
load 234
-
itob
load 208
itob
concat
load 207
itob
concat
load 206
itob
concat
store 205
b l41
l40:
load 234
load 209
-
itob
int 0
itob
concat
load 207
itob
concat
load 206
itob
concat
store 205
l41:
b l39
l38:
load 209
load 234
+
itob
load 208
itob
concat
load 207
itob
concat
load 206
itob
concat
store 205
l39:
l33:
l31:
l29:
load 205
substring 0 8
btoi
store 204
load 205
substring 8 16
btoi
store 203
load 205
substring 16 24
btoi
store 202
load 205
substring 24 32
btoi
store 201
load 232
int 0
>
bz l42
load 232
load 221
+
store 199
load 244
load 251
>
bz l44
load 244
load 251
-
load 199
+
itob
store 200
b l45
l44:
load 244
load 199
+
itob
store 200
l45:
b l43
l42:
load 244
load 251
>
bz l46
load 244
load 251
-
itob
store 200
b l47
l46:
load 244
itob
store 200
l47:
l43:
load 200
substring 0 8
btoi
store 199
load 242
int 1
+
store 198
load 240
int 0
>
load 199
load 249
==
!
&&
load 199
load 251
==
!
&&
bz l48
load 198
load 241
%
load 243
==
bz l49
byte base64()
load 1
==
assert
// compute state in HM_Set 16
int 16
itob
load 255
concat
load 254
itob
concat
load 253
concat
load 252
concat
load 251
itob
concat
load 250
concat
load 249
itob
concat
load 202
itob
concat
load 201
itob
concat
load 204
itob
concat
load 203
itob
concat
load 199
itob
concat
load 243
itob
concat
load 198
itob
concat
load 241
itob
concat
load 240
itob
concat
load 239
itob
concat
keccak256
load 0
==
assert
load 2
btoi
int 0
==
assert
// Check GroupSize
global GroupSize
int 4
==
assert
load 3
btoi
int 0
==
assert
// Check time limits
b checkAccts
l49:
byte base64()
load 1
==
assert
// compute state in HM_Set 18
int 18
itob
load 255
concat
load 254
itob
concat
load 253
concat
load 252
concat
load 251
itob
concat
load 250
concat
load 249
itob
concat
load 202
itob
concat
load 201
itob
concat
load 204
itob
concat
load 203
itob
concat
load 199
itob
concat
load 243
itob
concat
load 198
itob
concat
load 241
itob
concat
load 240
itob
concat
load 239
itob
concat
keccak256
load 0
==
assert
load 2
btoi
int 0
==
assert
// Check GroupSize
global GroupSize
int 4
==
assert
load 3
btoi
int 0
==
assert
// Check time limits
b checkAccts
l48:
byte base64()
load 1
==
assert
// compute state in HM_Set 9
int 9
itob
load 255
concat
load 254
itob
concat
load 253
concat
load 252
concat
load 250
concat
load 202
itob
concat
load 201
itob
concat
load 204
itob
concat
load 203
itob
concat
keccak256
load 0
==
assert
load 2
btoi
int 0
==
assert
// Check GroupSize
global GroupSize
int 4
==
assert
load 3
btoi
int 0
==
assert
// Check time limits
checkAccts:
gtxn 0 NumAccounts
load 6
==
assert
done:
int 1
return
`, `#pragma version 3
gtxna 0 ApplicationArgs 1
store 0
gtxna 0 ApplicationArgs 2
store 1
gtxna 0 ApplicationArgs 3
store 2
gtxna 0 ApplicationArgs 4
store 3
gtxna 0 ApplicationArgs 5
store 4
gtxna 0 ApplicationArgs 8
store 5
int 0
store 6
gtxna 0 ApplicationArgs 6
dup
substring 0 32
store 255
dup
substring 32 40
btoi
store 254
dup
substring 40 72
store 253
dup
substring 72 104
store 252
dup
substring 104 112
btoi
store 251
dup
substring 112 144
store 250
dup
substring 144 152
btoi
store 249
dup
substring 152 160
btoi
store 248
dup
substring 160 168
btoi
store 247
dup
substring 168 176
btoi
store 246
dup
substring 176 184
btoi
store 245
dup
substring 184 192
btoi
store 244
dup
substring 192 200
btoi
store 243
dup
substring 200 208
btoi
store 242
dup
substring 208 216
btoi
store 241
dup
substring 216 224
btoi
store 240
dup
substring 224 232
btoi
store 239
pop
gtxna 0 ApplicationArgs 7
dup
substring 0 8
btoi
store 238
dup
substring 8 16
btoi
store 237
dup
substring 16 24
btoi
store 236
dup
substring 24 32
btoi
store 235
dup
substring 32 40
btoi
store 234
dup
substring 40 48
btoi
store 233
dup
substring 48 56
btoi
store 232
dup
substring 56 64
btoi
store 231
dup
substring 64 72
btoi
store 230
dup
substring 72 80
btoi
store 229
dup
substring 80 88
btoi
store 228
dup
substring 88 96
btoi
store 227
dup
substring 96 104
btoi
store 226
pop
// Handler 14
// Check txnAppl
gtxn 0 TypeEnum
int appl
==
assert
gtxn 0 ApplicationID
byte "{{ApplicationID}}"
btoi
==
assert
gtxn 0 NumAppArgs
int 9
==
assert
// Check txnToHandler
gtxn 1 TypeEnum
int pay
==
assert
gtxn 1 Receiver
txn Sender
==
assert
gtxn 1 Amount
gtxn 2 Fee
int 100000
+
==
assert
// Check txnFromHandler (us)
txn GroupIndex
int 2
==
assert
txn TypeEnum
int pay
==
assert
txn Amount
int 0
==
assert
txn Receiver
gtxn 1 Sender
==
assert
// compute state in HM_Check 18
int 18
itob
load 255
concat
load 254
itob
concat
load 253
concat
load 252
concat
load 251
itob
concat
load 250
concat
load 249
itob
concat
load 248
itob
concat
load 247
itob
concat
load 246
itob
concat
load 245
itob
concat
load 244
itob
concat
load 243
itob
concat
load 242
itob
concat
load 241
itob
concat
load 240
itob
concat
load 239
itob
concat
keccak256
gtxna 0 ApplicationArgs 0
==
assert
txn CloseRemainderTo
gtxn 1 Sender
==
assert
// Run body
// "CheckPay"
// "./src/rsh/battleRecord.rsh:335:19:dot"
// "[]"
gtxn 3 TypeEnum
int pay
==
assert
gtxn 3 Receiver
byte "{{ContractAddr}}"
==
assert
gtxn 3 Amount
load 3
btoi
==
assert
// We don't care who the sender is... this means that you can get other people to pay for you if you want.
// Just "sender correct"
// "./src/rsh/battleRecord.rsh:335:19:dot"
// "[]"
load 255
gtxn 0 Sender
==
assert
byte base64()
load 1
==
assert
// compute state in HM_Set 19
int 19
itob
load 255
concat
load 254
itob
concat
load 253
concat
load 252
concat
load 251
itob
concat
load 250
concat
load 249
itob
concat
load 248
itob
concat
load 247
itob
concat
load 246
itob
concat
load 245
itob
concat
load 244
itob
concat
load 243
itob
concat
load 242
itob
concat
load 241
itob
concat
load 240
itob
concat
load 239
itob
concat
load 237
itob
concat
load 236
itob
concat
load 234
itob
concat
load 233
itob
concat
load 231
itob
concat
load 230
itob
concat
load 228
itob
concat
keccak256
load 0
==
assert
load 2
btoi
int 0
==
assert
// Check GroupSize
global GroupSize
int 4
==
assert
load 3
btoi
int 0
==
assert
// Check time limits
checkAccts:
gtxn 0 NumAccounts
load 6
==
assert
done:
int 1
return
`, `#pragma version 3
gtxna 0 ApplicationArgs 1
store 0
gtxna 0 ApplicationArgs 2
store 1
gtxna 0 ApplicationArgs 3
store 2
gtxna 0 ApplicationArgs 4
store 3
gtxna 0 ApplicationArgs 5
store 4
gtxna 0 ApplicationArgs 8
store 5
int 0
store 6
gtxna 0 ApplicationArgs 6
dup
substring 0 32
store 255
dup
substring 32 40
btoi
store 254
dup
substring 40 72
store 253
dup
substring 72 104
store 252
dup
substring 104 112
btoi
store 251
dup
substring 112 144
store 250
dup
substring 144 152
btoi
store 249
dup
substring 152 160
btoi
store 248
dup
substring 160 168
btoi
store 247
dup
substring 168 176
btoi
store 246
dup
substring 176 184
btoi
store 245
dup
substring 184 192
btoi
store 244
dup
substring 192 200
btoi
store 243
dup
substring 200 208
btoi
store 242
dup
substring 208 216
btoi
store 241
dup
substring 216 224
btoi
store 240
dup
substring 224 232
btoi
store 239
dup
substring 232 240
btoi
store 238
dup
substring 240 248
btoi
store 237
dup
int 248
int 256
substring3
btoi
store 236
dup
int 256
int 264
substring3
btoi
store 235
dup
int 264
int 272
substring3
btoi
store 234
dup
int 272
int 280
substring3
btoi
store 233
dup
int 280
int 288
substring3
btoi
store 232
pop
gtxna 0 ApplicationArgs 7
dup
substring 0 8
btoi
store 231
dup
substring 8 16
btoi
store 230
dup
substring 16 24
btoi
store 229
dup
substring 24 32
btoi
store 228
dup
substring 32 40
btoi
store 227
dup
substring 40 48
btoi
store 226
dup
substring 48 56
btoi
store 225
dup
substring 56 64
btoi
store 224
dup
substring 64 72
btoi
store 223
dup
substring 72 80
btoi
store 222
dup
substring 80 88
btoi
store 221
dup
substring 88 96
btoi
store 220
pop
// Handler 15
// Check txnAppl
gtxn 0 TypeEnum
int appl
==
assert
gtxn 0 ApplicationID
byte "{{ApplicationID}}"
btoi
==
assert
gtxn 0 NumAppArgs
int 9
==
assert
// Check txnToHandler
gtxn 1 TypeEnum
int pay
==
assert
gtxn 1 Receiver
txn Sender
==
assert
gtxn 1 Amount
gtxn 2 Fee
int 100000
+
==
assert
// Check txnFromHandler (us)
txn GroupIndex
int 2
==
assert
txn TypeEnum
int pay
==
assert
txn Amount
int 0
==
assert
txn Receiver
gtxn 1 Sender
==
assert
// compute state in HM_Check 19
int 19
itob
load 255
concat
load 254
itob
concat
load 253
concat
load 252
concat
load 251
itob
concat
load 250
concat
load 249
itob
concat
load 248
itob
concat
load 247
itob
concat
load 246
itob
concat
load 245
itob
concat
load 244
itob
concat
load 243
itob
concat
load 242
itob
concat
load 241
itob
concat
load 240
itob
concat
load 239
itob
concat
load 238
itob
concat
load 237
itob
concat
load 236
itob
concat
load 235
itob
concat
load 234
itob
concat
load 233
itob
concat
load 232
itob
concat
keccak256
gtxna 0 ApplicationArgs 0
==
assert
txn CloseRemainderTo
gtxn 1 Sender
==
assert
// Run body
// "CheckPay"
// "./src/rsh/battleRecord.rsh:361:19:dot"
// "[]"
gtxn 3 TypeEnum
int pay
==
assert
gtxn 3 Receiver
byte "{{ContractAddr}}"
==
assert
gtxn 3 Amount
load 3
btoi
==
assert
// We don't care who the sender is... this means that you can get other people to pay for you if you want.
// Just "sender correct"
// "./src/rsh/battleRecord.rsh:361:19:dot"
// "[]"
load 253
gtxn 0 Sender
==
assert
int 0
load 238
<
load 238
load 239
<
&&
bz l0
load 248
itob
load 247
itob
concat
load 246
itob
concat
load 245
itob
concat
store 215
b l1
l0:
load 238
load 230
==
bz l2
load 248
itob
load 247
itob
concat
load 246
itob
concat
load 245
itob
concat
store 215
b l3
l2:
load 237
int 0
>
bz l4
load 245
int 0
>
bz l6
load 248
itob
load 247
itob
concat
load 246
load 238
+
itob
concat
load 245
itob
concat
store 215
b l7
l6:
load 246
load 238
>
bz l8
load 248
itob
load 247
itob
concat
load 246
load 238
-
itob
concat
load 245
itob
concat
store 215
b l9
l8:
load 248
itob
load 247
itob
concat
load 238
load 246
-
itob
concat
int 1
itob
concat
store 215
l9:
l7:
b l5
l4:
load 247
int 0
>
bz l10
load 248
load 238
>
bz l12
load 248
load 238
-
itob
load 247
itob
concat
load 246
itob
concat
load 245
itob
concat
store 215
b l13
l12:
load 238
load 248
-
itob
int 0
itob
concat
load 246
itob
concat
load 245
itob
concat
store 215
l13:
b l11
l10:
load 248
load 238
+
itob
load 247
itob
concat
load 246
itob
concat
load 245
itob
concat
store 215
l11:
l5:
l3:
l1:
load 215
substring 0 8
btoi
store 214
load 215
substring 8 16
btoi
store 213
load 215
substring 16 24
btoi
store 212
load 215
substring 24 32
btoi
store 211
int 0
load 236
<
load 236
load 239
<
&&
bz l14
load 214
itob
load 213
itob
concat
load 212
itob
concat
load 211
itob
concat
store 210
b l15
l14:
load 236
load 227
==
bz l16
load 214
itob
load 213
itob
concat
load 212
itob
concat
load 211
itob
concat
store 210
b l17
l16:
load 235
int 0
>
bz l18
load 211
int 0
>
bz l20
load 214
itob
load 213
itob
concat
load 212
load 236
+
itob
concat
load 211
itob
concat
store 210
b l21
l20:
load 212
load 236
>
bz l22
load 214
itob
load 213
itob
concat
load 212
load 236
-
itob
concat
load 211
itob
concat
store 210
b l23
l22:
load 214
itob
load 213
itob
concat
load 236
load 212
-
itob
concat
int 1
itob
concat
store 210
l23:
l21:
b l19
l18:
load 213
int 0
>
bz l24
load 214
load 236
>
bz l26
load 214
load 236
-
itob
load 213
itob
concat
load 212
itob
concat
load 211
itob
concat
store 210
b l27
l26:
load 236
load 214
-
itob
int 0
itob
concat
load 212
itob
concat
load 211
itob
concat
store 210
l27:
b l25
l24:
load 214
load 236
+
itob
load 213
itob
concat
load 212
itob
concat
load 211
itob
concat
store 210
l25:
l19:
l17:
l15:
load 210
substring 0 8
btoi
store 209
load 210
substring 8 16
btoi
store 208
load 210
substring 16 24
btoi
store 207
load 210
substring 24 32
btoi
store 206
int 0
load 234
<
load 234
load 239
<
&&
bz l28
load 209
itob
load 208
itob
concat
load 207
itob
concat
load 206
itob
concat
store 205
b l29
l28:
load 234
load 224
==
bz l30
load 209
itob
load 208
itob
concat
load 207
itob
concat
load 206
itob
concat
store 205
b l31
l30:
load 233
int 0
>
bz l32
load 206
int 0
>
bz l34
load 209
itob
load 208
itob
concat
load 207
load 234
+
itob
concat
load 206
itob
concat
store 205
b l35
l34:
load 207
load 234
>
bz l36
load 209
itob
load 208
itob
concat
load 207
load 234
-
itob
concat
load 206
itob
concat
store 205
b l37
l36:
load 209
itob
load 208
itob
concat
load 234
load 207
-
itob
concat
int 1
itob
concat
store 205
l37:
l35:
b l33
l32:
load 208
int 0
>
bz l38
load 209
load 234
>
bz l40
load 209
load 234
-
itob
load 208
itob
concat
load 207
itob
concat
load 206
itob
concat
store 205
b l41
l40:
load 234
load 209
-
itob
int 0
itob
concat
load 207
itob
concat
load 206
itob
concat
store 205
l41:
b l39
l38:
load 209
load 234
+
itob
load 208
itob
concat
load 207
itob
concat
load 206
itob
concat
store 205
l39:
l33:
l31:
l29:
load 205
substring 0 8
btoi
store 204
load 205
substring 8 16
btoi
store 203
load 205
substring 16 24
btoi
store 202
load 205
substring 24 32
btoi
store 201
load 232
int 0
>
bz l42
load 232
load 221
+
store 199
load 244
load 249
>
bz l44
load 244
load 249
-
load 199
+
itob
store 200
b l45
l44:
load 244
load 199
+
itob
store 200
l45:
b l43
l42:
load 244
load 249
>
bz l46
load 244
load 249
-
itob
store 200
b l47
l46:
load 244
itob
store 200
l47:
l43:
load 200
substring 0 8
btoi
store 199
load 242
int 1
+
store 198
load 240
int 0
>
load 199
load 249
==
!
&&
load 199
load 251
==
!
&&
bz l48
load 198
load 241
%
load 243
==
bz l49
byte base64()
load 1
==
assert
// compute state in HM_Set 16
int 16
itob
load 255
concat
load 254
itob
concat
load 253
concat
load 252
concat
load 251
itob
concat
load 250
concat
load 249
itob
concat
load 204
itob
concat
load 203
itob
concat
load 202
itob
concat
load 201
itob
concat
load 199
itob
concat
load 243
itob
concat
load 198
itob
concat
load 241
itob
concat
load 240
itob
concat
load 239
itob
concat
keccak256
load 0
==
assert
load 2
btoi
int 0
==
assert
// Check GroupSize
global GroupSize
int 4
==
assert
load 3
btoi
int 0
==
assert
// Check time limits
b checkAccts
l49:
byte base64()
load 1
==
assert
// compute state in HM_Set 18
int 18
itob
load 255
concat
load 254
itob
concat
load 253
concat
load 252
concat
load 251
itob
concat
load 250
concat
load 249
itob
concat
load 204
itob
concat
load 203
itob
concat
load 202
itob
concat
load 201
itob
concat
load 199
itob
concat
load 243
itob
concat
load 198
itob
concat
load 241
itob
concat
load 240
itob
concat
load 239
itob
concat
keccak256
load 0
==
assert
load 2
btoi
int 0
==
assert
// Check GroupSize
global GroupSize
int 4
==
assert
load 3
btoi
int 0
==
assert
// Check time limits
b checkAccts
l48:
byte base64()
load 1
==
assert
// compute state in HM_Set 9
int 9
itob
load 255
concat
load 254
itob
concat
load 253
concat
load 252
concat
load 250
concat
load 204
itob
concat
load 203
itob
concat
load 202
itob
concat
load 201
itob
concat
keccak256
load 0
==
assert
load 2
btoi
int 0
==
assert
// Check GroupSize
global GroupSize
int 4
==
assert
load 3
btoi
int 0
==
assert
// Check time limits
checkAccts:
gtxn 0 NumAccounts
load 6
==
assert
done:
int 1
return
`],
  unsupported: [],
  version: 1,
  viewKeys: 0,
  viewSize: 0
  };
const _ETH = {
  ABI: `[
  {
    "inputs": [],
    "stateMutability": "payable",
    "type": "constructor"
  },
  {
    "inputs": [
      {
        "internalType": "uint256",
        "name": "msg",
        "type": "uint256"
      }
    ],
    "name": "ReachError",
    "type": "error"
  },
  {
    "anonymous": false,
    "inputs": [],
    "name": "e0",
    "type": "event"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "components": [
          {
            "components": [
              {
                "internalType": "uint256",
                "name": "v19",
                "type": "uint256"
              }
            ],
            "internalType": "struct T0",
            "name": "svs",
            "type": "tuple"
          },
          {
            "components": [
              {
                "internalType": "uint256",
                "name": "v24",
                "type": "uint256"
              }
            ],
            "internalType": "struct T2",
            "name": "msg",
            "type": "tuple"
          }
        ],
        "indexed": false,
        "internalType": "struct T3",
        "name": "_a",
        "type": "tuple"
      }
    ],
    "name": "e1",
    "type": "event"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "components": [
          {
            "components": [
              {
                "internalType": "address payable",
                "name": "v23",
                "type": "address"
              },
              {
                "internalType": "uint256",
                "name": "v24",
                "type": "uint256"
              },
              {
                "internalType": "address payable",
                "name": "v31",
                "type": "address"
              },
              {
                "internalType": "uint256",
                "name": "v51",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v72",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v83",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v84",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v85",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v86",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v992",
                "type": "uint256"
              }
            ],
            "internalType": "struct T18",
            "name": "svs",
            "type": "tuple"
          },
          {
            "components": [
              {
                "internalType": "uint256",
                "name": "v820",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v821",
                "type": "uint256"
              }
            ],
            "internalType": "struct T21",
            "name": "msg",
            "type": "tuple"
          }
        ],
        "indexed": false,
        "internalType": "struct T22",
        "name": "_a",
        "type": "tuple"
      }
    ],
    "name": "e10",
    "type": "event"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "components": [
          {
            "components": [
              {
                "internalType": "address payable",
                "name": "v23",
                "type": "address"
              },
              {
                "internalType": "uint256",
                "name": "v24",
                "type": "uint256"
              },
              {
                "internalType": "address payable",
                "name": "v31",
                "type": "address"
              },
              {
                "internalType": "uint256",
                "name": "v51",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v83",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v84",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v85",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v86",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v821",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v824",
                "type": "uint256"
              }
            ],
            "internalType": "struct T20",
            "name": "svs",
            "type": "tuple"
          },
          {
            "components": [
              {
                "internalType": "uint256",
                "name": "v833",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v834",
                "type": "uint256"
              }
            ],
            "internalType": "struct T24",
            "name": "msg",
            "type": "tuple"
          }
        ],
        "indexed": false,
        "internalType": "struct T25",
        "name": "_a",
        "type": "tuple"
      }
    ],
    "name": "e11",
    "type": "event"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "components": [
          {
            "components": [
              {
                "internalType": "address payable",
                "name": "v23",
                "type": "address"
              },
              {
                "internalType": "uint256",
                "name": "v24",
                "type": "uint256"
              },
              {
                "internalType": "address payable",
                "name": "v31",
                "type": "address"
              },
              {
                "internalType": "uint256",
                "name": "v51",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v52",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v72",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v73",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v83",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v84",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v85",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v86",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v89",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v90",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v91",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v92",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v93",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v94",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v992",
                "type": "uint256"
              }
            ],
            "internalType": "struct T17",
            "name": "svs",
            "type": "tuple"
          },
          {
            "components": [
              {
                "internalType": "uint256",
                "name": "v494",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v495",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v496",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v497",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v498",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v499",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v500",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v501",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v502",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v503",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v504",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v505",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v506",
                "type": "uint256"
              }
            ],
            "internalType": "struct T27",
            "name": "msg",
            "type": "tuple"
          }
        ],
        "indexed": false,
        "internalType": "struct T28",
        "name": "_a",
        "type": "tuple"
      }
    ],
    "name": "e12",
    "type": "event"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "components": [
          {
            "components": [
              {
                "internalType": "address payable",
                "name": "v23",
                "type": "address"
              },
              {
                "internalType": "uint256",
                "name": "v24",
                "type": "uint256"
              },
              {
                "internalType": "address payable",
                "name": "v31",
                "type": "address"
              },
              {
                "internalType": "uint256",
                "name": "v51",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v52",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v72",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v73",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v83",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v84",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v85",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v86",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v89",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v90",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v91",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v92",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v93",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v94",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v495",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v496",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v498",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v499",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v501",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v502",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v504",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v509",
                "type": "uint256"
              }
            ],
            "internalType": "struct T26",
            "name": "svs",
            "type": "tuple"
          },
          {
            "components": [
              {
                "internalType": "uint256",
                "name": "v527",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v528",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v529",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v530",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v531",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v532",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v533",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v534",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v535",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v536",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v537",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v538",
                "type": "uint256"
              }
            ],
            "internalType": "struct T30",
            "name": "msg",
            "type": "tuple"
          }
        ],
        "indexed": false,
        "internalType": "struct T31",
        "name": "_a",
        "type": "tuple"
      }
    ],
    "name": "e13",
    "type": "event"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "components": [
          {
            "components": [
              {
                "internalType": "address payable",
                "name": "v23",
                "type": "address"
              },
              {
                "internalType": "uint256",
                "name": "v24",
                "type": "uint256"
              },
              {
                "internalType": "address payable",
                "name": "v31",
                "type": "address"
              },
              {
                "internalType": "uint256",
                "name": "v51",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v52",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v72",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v73",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v83",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v84",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v85",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v86",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v89",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v90",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v91",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v92",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v93",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v94",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v992",
                "type": "uint256"
              }
            ],
            "internalType": "struct T17",
            "name": "svs",
            "type": "tuple"
          },
          {
            "components": [
              {
                "internalType": "uint256",
                "name": "v148",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v149",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v150",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v151",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v152",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v153",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v154",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v155",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v156",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v157",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v158",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v159",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v160",
                "type": "uint256"
              }
            ],
            "internalType": "struct T33",
            "name": "msg",
            "type": "tuple"
          }
        ],
        "indexed": false,
        "internalType": "struct T34",
        "name": "_a",
        "type": "tuple"
      }
    ],
    "name": "e14",
    "type": "event"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "components": [
          {
            "components": [
              {
                "internalType": "address payable",
                "name": "v23",
                "type": "address"
              },
              {
                "internalType": "uint256",
                "name": "v24",
                "type": "uint256"
              },
              {
                "internalType": "address payable",
                "name": "v31",
                "type": "address"
              },
              {
                "internalType": "uint256",
                "name": "v51",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v52",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v72",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v73",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v83",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v84",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v85",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v86",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v89",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v90",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v91",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v92",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v93",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v94",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v149",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v150",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v152",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v153",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v155",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v156",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v158",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v163",
                "type": "uint256"
              }
            ],
            "internalType": "struct T32",
            "name": "svs",
            "type": "tuple"
          },
          {
            "components": [
              {
                "internalType": "uint256",
                "name": "v187",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v188",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v189",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v190",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v191",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v192",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v193",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v194",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v195",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v196",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v197",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v198",
                "type": "uint256"
              }
            ],
            "internalType": "struct T35",
            "name": "msg",
            "type": "tuple"
          }
        ],
        "indexed": false,
        "internalType": "struct T36",
        "name": "_a",
        "type": "tuple"
      }
    ],
    "name": "e15",
    "type": "event"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "components": [
          {
            "components": [
              {
                "internalType": "address payable",
                "name": "v23",
                "type": "address"
              },
              {
                "internalType": "uint256",
                "name": "v24",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v27",
                "type": "uint256"
              }
            ],
            "internalType": "struct T1",
            "name": "svs",
            "type": "tuple"
          },
          {
            "internalType": "bool",
            "name": "msg",
            "type": "bool"
          }
        ],
        "indexed": false,
        "internalType": "struct T6",
        "name": "_a",
        "type": "tuple"
      }
    ],
    "name": "e2",
    "type": "event"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "components": [
          {
            "components": [
              {
                "internalType": "address payable",
                "name": "v23",
                "type": "address"
              },
              {
                "internalType": "uint256",
                "name": "v24",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v27",
                "type": "uint256"
              }
            ],
            "internalType": "struct T1",
            "name": "svs",
            "type": "tuple"
          },
          {
            "internalType": "bool",
            "name": "msg",
            "type": "bool"
          }
        ],
        "indexed": false,
        "internalType": "struct T6",
        "name": "_a",
        "type": "tuple"
      }
    ],
    "name": "e3",
    "type": "event"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "components": [
          {
            "components": [
              {
                "internalType": "address payable",
                "name": "v23",
                "type": "address"
              },
              {
                "internalType": "uint256",
                "name": "v24",
                "type": "uint256"
              },
              {
                "internalType": "address payable",
                "name": "v31",
                "type": "address"
              },
              {
                "internalType": "uint256",
                "name": "v33",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v34",
                "type": "uint256"
              }
            ],
            "internalType": "struct T4",
            "name": "svs",
            "type": "tuple"
          },
          {
            "components": [
              {
                "internalType": "uint256",
                "name": "v51",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v52",
                "type": "uint256"
              }
            ],
            "internalType": "struct T8",
            "name": "msg",
            "type": "tuple"
          }
        ],
        "indexed": false,
        "internalType": "struct T9",
        "name": "_a",
        "type": "tuple"
      }
    ],
    "name": "e4",
    "type": "event"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "components": [
          {
            "components": [
              {
                "internalType": "address payable",
                "name": "v23",
                "type": "address"
              },
              {
                "internalType": "uint256",
                "name": "v24",
                "type": "uint256"
              },
              {
                "internalType": "address payable",
                "name": "v31",
                "type": "address"
              },
              {
                "internalType": "uint256",
                "name": "v33",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v34",
                "type": "uint256"
              }
            ],
            "internalType": "struct T4",
            "name": "svs",
            "type": "tuple"
          },
          {
            "internalType": "bool",
            "name": "msg",
            "type": "bool"
          }
        ],
        "indexed": false,
        "internalType": "struct T10",
        "name": "_a",
        "type": "tuple"
      }
    ],
    "name": "e5",
    "type": "event"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "components": [
          {
            "components": [
              {
                "internalType": "address payable",
                "name": "v23",
                "type": "address"
              },
              {
                "internalType": "uint256",
                "name": "v24",
                "type": "uint256"
              },
              {
                "internalType": "address payable",
                "name": "v31",
                "type": "address"
              },
              {
                "internalType": "uint256",
                "name": "v33",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v51",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v52",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v55",
                "type": "uint256"
              }
            ],
            "internalType": "struct T7",
            "name": "svs",
            "type": "tuple"
          },
          {
            "components": [
              {
                "internalType": "uint256",
                "name": "v72",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v73",
                "type": "uint256"
              }
            ],
            "internalType": "struct T14",
            "name": "msg",
            "type": "tuple"
          }
        ],
        "indexed": false,
        "internalType": "struct T15",
        "name": "_a",
        "type": "tuple"
      }
    ],
    "name": "e6",
    "type": "event"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "components": [
          {
            "components": [
              {
                "internalType": "address payable",
                "name": "v23",
                "type": "address"
              },
              {
                "internalType": "uint256",
                "name": "v24",
                "type": "uint256"
              },
              {
                "internalType": "address payable",
                "name": "v31",
                "type": "address"
              },
              {
                "internalType": "uint256",
                "name": "v33",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v51",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v52",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v55",
                "type": "uint256"
              }
            ],
            "internalType": "struct T7",
            "name": "svs",
            "type": "tuple"
          },
          {
            "internalType": "bool",
            "name": "msg",
            "type": "bool"
          }
        ],
        "indexed": false,
        "internalType": "struct T16",
        "name": "_a",
        "type": "tuple"
      }
    ],
    "name": "e7",
    "type": "event"
  },
  {
    "inputs": [
      {
        "components": [
          {
            "components": [
              {
                "internalType": "uint256",
                "name": "v19",
                "type": "uint256"
              }
            ],
            "internalType": "struct T0",
            "name": "svs",
            "type": "tuple"
          },
          {
            "components": [
              {
                "internalType": "uint256",
                "name": "v24",
                "type": "uint256"
              }
            ],
            "internalType": "struct T2",
            "name": "msg",
            "type": "tuple"
          }
        ],
        "internalType": "struct T3",
        "name": "_a",
        "type": "tuple"
      }
    ],
    "name": "m1",
    "outputs": [],
    "stateMutability": "payable",
    "type": "function"
  },
  {
    "inputs": [
      {
        "components": [
          {
            "components": [
              {
                "internalType": "address payable",
                "name": "v23",
                "type": "address"
              },
              {
                "internalType": "uint256",
                "name": "v24",
                "type": "uint256"
              },
              {
                "internalType": "address payable",
                "name": "v31",
                "type": "address"
              },
              {
                "internalType": "uint256",
                "name": "v51",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v72",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v83",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v84",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v85",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v86",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v992",
                "type": "uint256"
              }
            ],
            "internalType": "struct T18",
            "name": "svs",
            "type": "tuple"
          },
          {
            "components": [
              {
                "internalType": "uint256",
                "name": "v820",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v821",
                "type": "uint256"
              }
            ],
            "internalType": "struct T21",
            "name": "msg",
            "type": "tuple"
          }
        ],
        "internalType": "struct T22",
        "name": "_a",
        "type": "tuple"
      }
    ],
    "name": "m10",
    "outputs": [],
    "stateMutability": "payable",
    "type": "function"
  },
  {
    "inputs": [
      {
        "components": [
          {
            "components": [
              {
                "internalType": "address payable",
                "name": "v23",
                "type": "address"
              },
              {
                "internalType": "uint256",
                "name": "v24",
                "type": "uint256"
              },
              {
                "internalType": "address payable",
                "name": "v31",
                "type": "address"
              },
              {
                "internalType": "uint256",
                "name": "v51",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v83",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v84",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v85",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v86",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v821",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v824",
                "type": "uint256"
              }
            ],
            "internalType": "struct T20",
            "name": "svs",
            "type": "tuple"
          },
          {
            "components": [
              {
                "internalType": "uint256",
                "name": "v833",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v834",
                "type": "uint256"
              }
            ],
            "internalType": "struct T24",
            "name": "msg",
            "type": "tuple"
          }
        ],
        "internalType": "struct T25",
        "name": "_a",
        "type": "tuple"
      }
    ],
    "name": "m11",
    "outputs": [],
    "stateMutability": "payable",
    "type": "function"
  },
  {
    "inputs": [
      {
        "components": [
          {
            "components": [
              {
                "internalType": "address payable",
                "name": "v23",
                "type": "address"
              },
              {
                "internalType": "uint256",
                "name": "v24",
                "type": "uint256"
              },
              {
                "internalType": "address payable",
                "name": "v31",
                "type": "address"
              },
              {
                "internalType": "uint256",
                "name": "v51",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v52",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v72",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v73",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v83",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v84",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v85",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v86",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v89",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v90",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v91",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v92",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v93",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v94",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v992",
                "type": "uint256"
              }
            ],
            "internalType": "struct T17",
            "name": "svs",
            "type": "tuple"
          },
          {
            "components": [
              {
                "internalType": "uint256",
                "name": "v494",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v495",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v496",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v497",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v498",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v499",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v500",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v501",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v502",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v503",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v504",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v505",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v506",
                "type": "uint256"
              }
            ],
            "internalType": "struct T27",
            "name": "msg",
            "type": "tuple"
          }
        ],
        "internalType": "struct T28",
        "name": "_a",
        "type": "tuple"
      }
    ],
    "name": "m12",
    "outputs": [],
    "stateMutability": "payable",
    "type": "function"
  },
  {
    "inputs": [
      {
        "components": [
          {
            "components": [
              {
                "internalType": "address payable",
                "name": "v23",
                "type": "address"
              },
              {
                "internalType": "uint256",
                "name": "v24",
                "type": "uint256"
              },
              {
                "internalType": "address payable",
                "name": "v31",
                "type": "address"
              },
              {
                "internalType": "uint256",
                "name": "v51",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v52",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v72",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v73",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v83",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v84",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v85",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v86",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v89",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v90",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v91",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v92",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v93",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v94",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v495",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v496",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v498",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v499",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v501",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v502",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v504",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v509",
                "type": "uint256"
              }
            ],
            "internalType": "struct T26",
            "name": "svs",
            "type": "tuple"
          },
          {
            "components": [
              {
                "internalType": "uint256",
                "name": "v527",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v528",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v529",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v530",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v531",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v532",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v533",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v534",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v535",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v536",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v537",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v538",
                "type": "uint256"
              }
            ],
            "internalType": "struct T30",
            "name": "msg",
            "type": "tuple"
          }
        ],
        "internalType": "struct T31",
        "name": "_a",
        "type": "tuple"
      }
    ],
    "name": "m13",
    "outputs": [],
    "stateMutability": "payable",
    "type": "function"
  },
  {
    "inputs": [
      {
        "components": [
          {
            "components": [
              {
                "internalType": "address payable",
                "name": "v23",
                "type": "address"
              },
              {
                "internalType": "uint256",
                "name": "v24",
                "type": "uint256"
              },
              {
                "internalType": "address payable",
                "name": "v31",
                "type": "address"
              },
              {
                "internalType": "uint256",
                "name": "v51",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v52",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v72",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v73",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v83",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v84",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v85",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v86",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v89",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v90",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v91",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v92",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v93",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v94",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v992",
                "type": "uint256"
              }
            ],
            "internalType": "struct T17",
            "name": "svs",
            "type": "tuple"
          },
          {
            "components": [
              {
                "internalType": "uint256",
                "name": "v148",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v149",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v150",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v151",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v152",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v153",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v154",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v155",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v156",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v157",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v158",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v159",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v160",
                "type": "uint256"
              }
            ],
            "internalType": "struct T33",
            "name": "msg",
            "type": "tuple"
          }
        ],
        "internalType": "struct T34",
        "name": "_a",
        "type": "tuple"
      }
    ],
    "name": "m14",
    "outputs": [],
    "stateMutability": "payable",
    "type": "function"
  },
  {
    "inputs": [
      {
        "components": [
          {
            "components": [
              {
                "internalType": "address payable",
                "name": "v23",
                "type": "address"
              },
              {
                "internalType": "uint256",
                "name": "v24",
                "type": "uint256"
              },
              {
                "internalType": "address payable",
                "name": "v31",
                "type": "address"
              },
              {
                "internalType": "uint256",
                "name": "v51",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v52",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v72",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v73",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v83",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v84",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v85",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v86",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v89",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v90",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v91",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v92",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v93",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v94",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v149",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v150",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v152",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v153",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v155",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v156",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v158",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v163",
                "type": "uint256"
              }
            ],
            "internalType": "struct T32",
            "name": "svs",
            "type": "tuple"
          },
          {
            "components": [
              {
                "internalType": "uint256",
                "name": "v187",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v188",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v189",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v190",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v191",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v192",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v193",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v194",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v195",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v196",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v197",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v198",
                "type": "uint256"
              }
            ],
            "internalType": "struct T35",
            "name": "msg",
            "type": "tuple"
          }
        ],
        "internalType": "struct T36",
        "name": "_a",
        "type": "tuple"
      }
    ],
    "name": "m15",
    "outputs": [],
    "stateMutability": "payable",
    "type": "function"
  },
  {
    "inputs": [
      {
        "components": [
          {
            "components": [
              {
                "internalType": "address payable",
                "name": "v23",
                "type": "address"
              },
              {
                "internalType": "uint256",
                "name": "v24",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v27",
                "type": "uint256"
              }
            ],
            "internalType": "struct T1",
            "name": "svs",
            "type": "tuple"
          },
          {
            "internalType": "bool",
            "name": "msg",
            "type": "bool"
          }
        ],
        "internalType": "struct T6",
        "name": "_a",
        "type": "tuple"
      }
    ],
    "name": "m2",
    "outputs": [],
    "stateMutability": "payable",
    "type": "function"
  },
  {
    "inputs": [
      {
        "components": [
          {
            "components": [
              {
                "internalType": "address payable",
                "name": "v23",
                "type": "address"
              },
              {
                "internalType": "uint256",
                "name": "v24",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v27",
                "type": "uint256"
              }
            ],
            "internalType": "struct T1",
            "name": "svs",
            "type": "tuple"
          },
          {
            "internalType": "bool",
            "name": "msg",
            "type": "bool"
          }
        ],
        "internalType": "struct T6",
        "name": "_a",
        "type": "tuple"
      }
    ],
    "name": "m3",
    "outputs": [],
    "stateMutability": "payable",
    "type": "function"
  },
  {
    "inputs": [
      {
        "components": [
          {
            "components": [
              {
                "internalType": "address payable",
                "name": "v23",
                "type": "address"
              },
              {
                "internalType": "uint256",
                "name": "v24",
                "type": "uint256"
              },
              {
                "internalType": "address payable",
                "name": "v31",
                "type": "address"
              },
              {
                "internalType": "uint256",
                "name": "v33",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v34",
                "type": "uint256"
              }
            ],
            "internalType": "struct T4",
            "name": "svs",
            "type": "tuple"
          },
          {
            "components": [
              {
                "internalType": "uint256",
                "name": "v51",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v52",
                "type": "uint256"
              }
            ],
            "internalType": "struct T8",
            "name": "msg",
            "type": "tuple"
          }
        ],
        "internalType": "struct T9",
        "name": "_a",
        "type": "tuple"
      }
    ],
    "name": "m4",
    "outputs": [],
    "stateMutability": "payable",
    "type": "function"
  },
  {
    "inputs": [
      {
        "components": [
          {
            "components": [
              {
                "internalType": "address payable",
                "name": "v23",
                "type": "address"
              },
              {
                "internalType": "uint256",
                "name": "v24",
                "type": "uint256"
              },
              {
                "internalType": "address payable",
                "name": "v31",
                "type": "address"
              },
              {
                "internalType": "uint256",
                "name": "v33",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v34",
                "type": "uint256"
              }
            ],
            "internalType": "struct T4",
            "name": "svs",
            "type": "tuple"
          },
          {
            "internalType": "bool",
            "name": "msg",
            "type": "bool"
          }
        ],
        "internalType": "struct T10",
        "name": "_a",
        "type": "tuple"
      }
    ],
    "name": "m5",
    "outputs": [],
    "stateMutability": "payable",
    "type": "function"
  },
  {
    "inputs": [
      {
        "components": [
          {
            "components": [
              {
                "internalType": "address payable",
                "name": "v23",
                "type": "address"
              },
              {
                "internalType": "uint256",
                "name": "v24",
                "type": "uint256"
              },
              {
                "internalType": "address payable",
                "name": "v31",
                "type": "address"
              },
              {
                "internalType": "uint256",
                "name": "v33",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v51",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v52",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v55",
                "type": "uint256"
              }
            ],
            "internalType": "struct T7",
            "name": "svs",
            "type": "tuple"
          },
          {
            "components": [
              {
                "internalType": "uint256",
                "name": "v72",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v73",
                "type": "uint256"
              }
            ],
            "internalType": "struct T14",
            "name": "msg",
            "type": "tuple"
          }
        ],
        "internalType": "struct T15",
        "name": "_a",
        "type": "tuple"
      }
    ],
    "name": "m6",
    "outputs": [],
    "stateMutability": "payable",
    "type": "function"
  },
  {
    "inputs": [
      {
        "components": [
          {
            "components": [
              {
                "internalType": "address payable",
                "name": "v23",
                "type": "address"
              },
              {
                "internalType": "uint256",
                "name": "v24",
                "type": "uint256"
              },
              {
                "internalType": "address payable",
                "name": "v31",
                "type": "address"
              },
              {
                "internalType": "uint256",
                "name": "v33",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v51",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v52",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "v55",
                "type": "uint256"
              }
            ],
            "internalType": "struct T7",
            "name": "svs",
            "type": "tuple"
          },
          {
            "internalType": "bool",
            "name": "msg",
            "type": "bool"
          }
        ],
        "internalType": "struct T16",
        "name": "_a",
        "type": "tuple"
      }
    ],
    "name": "m7",
    "outputs": [],
    "stateMutability": "payable",
    "type": "function"
  },
  {
    "stateMutability": "payable",
    "type": "receive"
  }
]`,
  Bytecode: `0x608060408190527f49ff028a829527a47ec6839c7147b484eccf5a2a94853eddac09cef44d9d4e9e90600090a16040805160208082018352438252825180820184526000808252925181528351808301849052905181850152835180820385018152606090910190935282519201919091209055614363806100826000396000f3fe6080604052600436106100c55760003560e01c8063900ff5b11161007f578063d3218fd711610059578063d3218fd71461017e578063dc9c8a8f14610191578063e163d7c4146101a4578063ef3d7a3a146101b757600080fd5b8063900ff5b1146101455780639532ef01146101585780639c669a391461016b57600080fd5b8062c0492e146100d15780630d9c4115146100e65780631a3de4b8146100f95780631c2947e31461010c5780632438df701461011f57806371acb0f01461013257600080fd5b366100cc57005b600080fd5b6100e46100df366004613958565b6101ca565b005b6100e46100f4366004613975565b6104aa565b6100e4610107366004613928565b611084565b6100e461011a366004613915565b61119b565b6100e461012d3660046139a4565b611356565b6100e4610140366004613958565b6114f0565b6100e46101533660046139b6565b6117b4565b6100e4610166366004613992565b61198d565b6100e461017936600461393b565b611a92565b6100e461018c366004613975565b612018565b6100e461019f36600461393b565b612ba0565b6100e46101b23660046139a4565b612d99565b6100e46101c5366004613903565b612e9b565b604051610206906101e2906012908490602001614100565b6040516020818303038152906040528051906020012060001c600054146030612f9d565b600080556102163415602e612f9d565b6102383361022760208401846138e1565b6001600160a01b031614602f612f9d565b7f78a6beaf944c222f7f6a266b9b82e54c2c94d54a415a883c18707dbf95be4562816040516102679190613f6d565b60405180910390a161034060405180610320016040528060006001600160a01b031681526020016000815260200160006001600160a01b03168152602001600081526020016000815260200160008152602001600081526020016000815260200160008152602001600081526020016000815260200160008152602001600081526020016000815260200160008152602001600081526020016000815260200160008152602001600081526020016000815260200160008152602001600081526020016000815260200160008152602001600081525090565b61034d60208301836138e1565b6001600160a01b031681526020808301359082015261037260608301604084016138e1565b6001600160a01b0316604080830191909152606080840135908301526080808401359083015260a0808401359083015260c0808401359083015260e08084013590830152610100808401359083015261012080840135908301526101408084013590830152610160808401359083015261018080840135908301526101a080840135908301526101c080840135908301526101e08084013590830152610200808401359083015261026080840135610220840152610280808501356102408501526102c080860135928501929092526102e080860135918501919091526103208501356102a08501526103408501359184019190915261038084013590830152436103008301525161048b906013908390602001614269565b60408051601f1981840301815291905280516020909101206000555050565b6040516104e6906104c2906013908490602001614254565b6040516020818303038152906040528051906020012060001c600054146033612f9d565b600080556104f2613457565b6104fe34156031612f9d565b6105233361051260608501604086016138e1565b6001600160a01b0316146032612f9d565b610220820135610534576000610542565b610200820135610220830135105b15610587576020808201805160e085013590528051610100850135920191909152805161012084013560409091015280516101408401356060909101525181526107f0565b61022082013561034083013514156105d9576040808201805160e085013590528051610100850135602090910152805161012085013592019190915280516101408401356060909101525181526107f0565b610240820135156106ef576101408201351561063e5760608101805160e084013590525161010083013560209091015261061d6102208301356101208401356142a7565b606080830180516040019290925281516101408501359101525181526107f0565b61022082013561012083013511156106a15760808101805160e084013590525161010083013560209091015261067e6102208301356101208401356142de565b6080820180516040019190915280516101408401356060909101525181526107f0565b60a08101805160e08401359052516101008301356020909101526106cf6101208301356102208401356142de565b60a0820180516040019190915280516001606091909101525181526107f0565b610100820135156107a55761022082013560e0830135111561075a5761071e61022083013560e08401356142de565b60c082018051919091528051610100840135602090910152805161012084013560409091015280516101408401356060909101525181526107f0565b61076d60e08301356102208401356142de565b60e0820180519190915280516000602090910152805161012084013560409091015280516101408401356060909101525181526107f0565b6107b861022083013560e08401356142a7565b610100808301805192909252815190840135602090910152805161012084013560409091015280516101408401356060909101525181525b61026082013561080157600061080f565b610200820135610260830135105b1561085857805151610140820180519190915281516020908101518251909101528151604090810151825190910152815160609081015182519091015251610120820152610ae6565b6102608201356103a083013514156108ae57805151610160820180519190915281516020908101518251909101528151604090810151825190910152815160609081015182519091015251610120820152610ae6565b610280820135156109d9578051606001511561091c5780515161018082018051919091528151602090810151915101528051604001516108f490610260840135906142a7565b6101808201805160400191909152815160609081015182519091015251610120820152610ae6565b8051604001516102608301351015610986578051516101a0820180519190915281516020908101519151015280516040015161095e90610260840135906142de565b6101a08201805160400191909152815160609081015182519091015251610120820152610ae6565b8051516101c082018051919091528151602090810151915101528051604001516109b5906102608401356142de565b6101c082018051604001919091528051600160609091015251610120820152610ae6565b80516020015115610a96578051516102608301351015610a4757805151610a0690610260840135906142de565b6101e0820180519190915281516020908101518251909101528151604090810151825190910152815160609081015182519091015251610120820152610ae6565b805151610a59906102608401356142de565b6102008201805191909152805160006020909101528151604090810151825190910152815160609081015182519091015251610120820152610ae6565b805151610aa990610260840135906142a7565b6102208201805191909152815160209081015182519091015281516040908101518251909101528151606090810151825190910152516101208201525b6102a0820135610af7576000610b05565b6102008201356102a0830135105b15610b53576101208101805151610260830180519190915281516020908101518251909101528151604090810151825190910152905160609081015182519091015251610240820152610e24565b6102a08201356104008301351415610bae576101208101805151610280830180519190915281516020908101518251909101528151604090810151825190910152905160609081015182519091015251610240820152610e24565b6102c082013515610cf7576101208101516060015115610c2a5761012081018051516102a0808401805192909252825160209081015192510191909152905160400151610bfe91840135906142a7565b6102a0820180516040019190915261012082015160609081015182519091015251610240820152610e24565b610120810151604001516102a08301351015610ca05761012081018051516102c083018051919091528151602090810151915101525160400151610c74906102a0840135906142de565b6102c0820180516040019190915261012082015160609081015182519091015251610240820152610e24565b61012081018051516102e083018051919091528151602090810151915101525160400151610cd3906102a08401356142de565b6102e082018051604001919091528051600160609091015251610240820152610e24565b6101208101516020015115610dcc57610120810151516102a08301351015610d755761012081015151610d30906102a0840135906142de565b61030082018051919091526101208201805160209081015183519091015280516040908101518351909101525160609081015182519091015251610240820152610e24565b61012081015151610d8b906102a08401356142de565b610320820180519190915280516000602090910152610120820180516040908101518351909101525160609081015182519091015251610240820152610e24565b61012081015151610de3906102a0840135906142a7565b610340820180519190915261012082018051602090810151835190910152805160409081015183519091015251606090810151825190910152516102408201525b6102e082013515610ec057610e436104608301356102e08401356142a7565b61038082015260c08201356101608301351115610e9457610380810151610e7360c08401356101608501356142de565b610e7d91906142a7565b6103a0820180519190915251610360820152610f12565b610380810151610ea9906101608401356142a7565b6103c0820180519190915251610360820152610f12565b60c08201356101608301351115610efb57610ee460c08301356101608401356142de565b6103e0820180519190915251610360820152610f12565b610400810180516101608401359052516103608201525b7f5608678933c261c83e77dc6d0ace78ea7db075984db24ad53f3b1747035e7f5d82604051610f419190614010565b60405180910390a1610f51613650565b610f5e60208401846138e1565b81516001600160a01b0390911690528051602080850135910152610f8860608401604085016138e1565b81516001600160a01b03909116604091820152815160608086013591810191909152825160808087013591810191909152835160a08088013591810191909152845160c08089013591015261024086018051516020808801805192909252825181015182519091015281518601518151909601959095525183015184519093019290925261036085015151835190910152905161018085013591015261103460016101a08501356142a7565b60208201805160c0019190915280516101c085013560e09091015280516101e08501356101009091015280516102008501356101209091015251436101409091015261107f81612fc2565b505050565b6040516110c09061109c906005908490602001614292565b6040516020818303038152906040528051906020012060001c60005414601e612f9d565b600080556110e06110d6600560c08401356142a7565b431015601f612f9d565b6110ec3415601c612f9d565b6111113361110060608401604085016138e1565b6001600160a01b031614601d612f9d565b61112160608201604083016138e1565b6040516001600160a01b039190911690606083013580156108fc02916000818181858888f1935050505015801561115c573d6000803e3d6000fd5b507f0b3f4ceba20fb33184a7821735173dd8ab832c32e0ab37a206103333aced56688160405161118c9190613f0f565b60405180910390a16000805533ff5b6040516111d7906111b3906005908490602001614292565b6040516020818303038152906040528051906020012060001c60005414601a612f9d565b600080556111f66111ed600560c08401356142a7565b4310601b612f9d565b61120234156018612f9d565b6112243361121360208401846138e1565b6001600160a01b0316146019612f9d565b7ff9573e675c9bc3a16b419c409b77495ceab1a0967da38c45725a0e3a3d710d25816040516112539190613ee0565b60405180910390a1611263613650565b61127060208301836138e1565b81516001600160a01b039091169052805160208084013591015261129a60608301604084016138e1565b81516001600160a01b039091166040918201528151608080850135606092830152835160a08087013591830191909152845160e0808801359183019190915285516101008089013560c0928301526020808901805160009081905281519092018290528051909801819052875190960186905286519094018590528551909201939093528351600191018190528351600293019290925282510152805160326101209091015251436101409091015261135281612fc2565b5050565b6040516113929061136e906001908490602001614240565b6040516020818303038152906040528051906020012060001c60005414600a612f9d565b600080805560408051602081019091529081526113c06113b7600a60408501356142a7565b4310600b612f9d565b6113ce6020830135806142a7565b81526113e1346020840135146009612f9d565b7f1ca594b20641191c893d80895212a20239e99e17b7304a35c096140ec34f22dd8260405161141091906140a8565b60405180910390a161145c6040518060a0016040528060006001600160a01b031681526020016000815260200160006001600160a01b0316815260200160008152602001600081525090565b61146960208401846138e1565b6001600160a01b0390811682526020938401358483019081523360408085019182529451606080860191825243608080880191825288516003818c015297518716888a0152945191870191909152915190931691840191909152905160a08301525160c0808301919091528251808303909101815260e09091019091528051910120600055565b60405161152c90611508906010908490602001614100565b6040516020818303038152906040528051906020012060001c60005414602a612f9d565b6000805561153c34156028612f9d565b6115613361155060608401604085016138e1565b6001600160a01b0316146029612f9d565b7f1bd227069db4ad213b5bf4545e4c63af90dad0b82702a901be423735dcad6d8f816040516115909190613f6d565b60405180910390a161166960405180610320016040528060006001600160a01b031681526020016000815260200160006001600160a01b03168152602001600081526020016000815260200160008152602001600081526020016000815260200160008152602001600081526020016000815260200160008152602001600081526020016000815260200160008152602001600081526020016000815260200160008152602001600081526020016000815260200160008152602001600081526020016000815260200160008152602001600081525090565b61167660208301836138e1565b6001600160a01b031681526020808301359082015261169b60608301604084016138e1565b6001600160a01b0316604080830191909152606080840135908301526080808401359083015260a0808401359083015260c0808401359083015260e08084013590830152610100808401359083015261012080840135908301526101408084013590830152610160808401359083015261018080840135908301526101a080840135908301526101c080840135908301526101e08084013590830152610200808401359083015261026080840135610220840152610280808501356102408501526102c080860135928501929092526102e080860135918501919091526103208501356102a08501526103408501359184019190915261038084013590830152436103008301525161048b906011908390602001614269565b6040516117f0906117cc90600390849060200161427e565b6040516020818303038152906040528051906020012060001c600054146012612f9d565b6000805561180f611806600560808401356142a7565b43106013612f9d565b61181b34156010612f9d565b6118403361182f60608401604085016138e1565b6001600160a01b0316146011612f9d565b7f0e1ba93252ec75210543b01f81f77649ecdc7a91bb28dce8b23f6139007b4dec8160405161186f91906140d8565b60405180910390a16118c96040518060e0016040528060006001600160a01b031681526020016000815260200160006001600160a01b03168152602001600081526020016000815260200160008152602001600081525090565b6118d660208301836138e1565b6001600160a01b03168152602080830135908201526118fb60608301604084016138e1565b6001600160a01b03908116604083810191825260608581013581860190815260a080880135608080890191825260c0808b0135848b0190815243828c01908152885160056020808301919091528d518d169a82019a909a52988c0151978901979097529751909816908601529151908401525193820193909352905160e082015290516101008201526101200161048b565b60408051600060208201528235918101919091526119cc906060016040516020818303038152906040528051906020012060001c600054146008612f9d565b600080556119e1346020830135146007612f9d565b6040805182358152602080840135908201527ff2c62eba998811305a23599b2e6d212befbd7ded3a73f4c08bfb9aefe08dc166910160405180910390a1611a4b604051806060016040528060006001600160a01b0316815260200160008152602001600081525090565b338152602082810135818301908152436040808501918252805160019481019490945284516001600160a01b0316908401529051606083015251608082015260a00161048b565b604051611ace90611aaa90600a908490602001614216565b6040516020818303038152906040528051906020012060001c600054146027612f9d565b60008055611ada61370f565b611ae634156024612f9d565b611b0b33611afa60608501604086016138e1565b6001600160a01b0316146025612f9d565b60408051610140840135602082015261016084013591810191909152611b539060600160408051601f1981840301815291905280516020909101206060840135146026612f9d565b60a082013560e08301351415611d5b5760a082013515611d015760808201356101008301351115611c3f57611b9160808301356101008401356142de565b606082015260c08201356101608301351115611bf35760608101516101208201805191909152516000602090910152611bd360c08301356101608401356142de565b610120820180516040019190915280516000606090910152518152611ed2565b60608101516101408201805191909152516000602090910152611c1f61016083013560c08401356142de565b610140820180516040019190915280516001606090910152518152611ed2565b611c5261010083013560808401356142de565b608082015260c08201356101608301351115611cb557608081015161016080830180519290925290516001602090910152611c959060c0840135908401356142de565b610160820180516040019190915280516000606090910152518152611ed2565b60808101516101808201805191909152516001602090910152611ce161016083013560c08401356142de565b610180820180516040019190915280516001606090910152518152611ed2565b611d1461010083013560808401356142a7565b6101a08201805191909152516000602090910152611d3b61016083013560c08401356142a7565b6101a0820180516040019190915280516000606090910152518152611ed2565b60e082013560a08301351115611e1757611d7e60c08301356101608401356142a7565b602082015260808201356101008301351115611dd557611da760808301356101008401356142de565b60a0820180519190915280516000602091820181905290830151825160400152815160600152518152611ed2565b611de861010083013560808401356142de565b60c082018051919091528051600160209182015282015181516040015280516000606090910152518152611ed2565b611e2a60808301356101008401356142a7565b604082015260c08201356101608301351115611e8a57604081015160e08201805191909152516000602090910152611e6b60c08301356101608401356142de565b60e0820180516040019190915280516000606090910152518152611ed2565b60408101516101008201805191909152516000602090910152611eb661016083013560c08401356142de565b6101008201805160400191909152805160016060909101525181525b805160608101516020820151146101c08301526040810151905114611ef8576000611eff565b806101c001515b15611f8457611f1160208301836138e1565b6001600160a01b03166108fc611f2c602085013560026142bf565b6040518115909202916000818181858888f19350505050158015611f54573d6000803e3d6000fd5b507f4ed365722ea5fc41279978e41f3661307e31c19d82449d9ac38f58a8de976e498260405161118c9190613f40565b805160608101516020909101511115611fa757611f1160608301604084016138e1565b806101c001511561200b5780516020015115611fed5780516040810151905111611fdd57611fd860208301836138e1565b611f11565b611f1160608301604084016138e1565b8051604081015190511161200b57611fd860608301604084016138e1565b611f1160208301836138e1565b60405161205490612030906011908490602001614254565b6040516020818303038152906040528051906020012060001c60005414602d612f9d565b60008055612060613457565b61206c3415602b612f9d565b61208e3361207d60208501856138e1565b6001600160a01b031614602c612f9d565b61022082013561209f5760006120ad565b610200820135610220830135105b156120f2576020808201805161012085013590528051610140850135920191909152805160e0840135604090910152805161010084013560609091015251815261235b565b6102208201356103408301351415612144576040808201805161012085013590528051610140850135602090910152805160e0850135920191909152805161010084013560609091015251815261235b565b6102408201351561225957610100820135156121a95760608101805161012084013590525161014083013560209091015261218861022083013560e08401356142a7565b6060808301805160400192909252815161010085013591015251815261235b565b61022082013560e0830135111561220b576080810180516101208401359052516101408301356020909101526121e861022083013560e08401356142de565b60808201805160400191909152805161010084013560609091015251815261235b565b60a08101805161012084013590525161014083013560209091015261223960e08301356102208401356142de565b60a08201805160400191909152805160016060919091015251815261235b565b610140820135156123105761022082013561012083013511156122c55761228a6102208301356101208401356142de565b60c082018051919091528051610140840135602090910152805160e0840135604090910152805161010084013560609091015251815261235b565b6122d96101208301356102208401356142de565b60e080830180519290925281516000602090910152815190840135604090910152805161010084013560609091015251815261235b565b6123246102208301356101208401356142a7565b6101008083018051929092528151610140850135602090910152815160e08501356040909101528151908401356060909101525181525b61026082013561236c57600061237a565b610200820135610260830135105b156123c357805151610140820180519190915281516020908101518251909101528151604090810151825190910152815160609081015182519091015251610120820152612651565b6102608201356103a0830135141561241957805151610160820180519190915281516020908101518251909101528151604090810151825190910152815160609081015182519091015251610120820152612651565b61028082013515612544578051606001511561248757805151610180820180519190915281516020908101519151015280516040015161245f90610260840135906142a7565b6101808201805160400191909152815160609081015182519091015251610120820152612651565b80516040015161026083013510156124f1578051516101a082018051919091528151602090810151915101528051604001516124c990610260840135906142de565b6101a08201805160400191909152815160609081015182519091015251610120820152612651565b8051516101c08201805191909152815160209081015191510152805160400151612520906102608401356142de565b6101c082018051604001919091528051600160609091015251610120820152612651565b805160200151156126015780515161026083013510156125b25780515161257190610260840135906142de565b6101e0820180519190915281516020908101518251909101528151604090810151825190910152815160609081015182519091015251610120820152612651565b8051516125c4906102608401356142de565b6102008201805191909152805160006020909101528151604090810151825190910152815160609081015182519091015251610120820152612651565b80515161261490610260840135906142a7565b6102208201805191909152815160209081015182519091015281516040908101518251909101528151606090810151825190910152516101208201525b6102a0820135612662576000612670565b6102008201356102a0830135105b156126be57610120810180515161026083018051919091528151602090810151825190910152815160409081015182519091015290516060908101518251909101525161024082015261298f565b6102a0820135610400830135141561271957610120810180515161028083018051919091528151602090810151825190910152815160409081015182519091015290516060908101518251909101525161024082015261298f565b6102c0820135156128625761012081015160600151156127955761012081018051516102a080840180519290925282516020908101519251019190915290516040015161276991840135906142a7565b6102a082018051604001919091526101208201516060908101518251909101525161024082015261298f565b610120810151604001516102a0830135101561280b5761012081018051516102c0830180519190915281516020908101519151015251604001516127df906102a0840135906142de565b6102c082018051604001919091526101208201516060908101518251909101525161024082015261298f565b61012081018051516102e08301805191909152815160209081015191510152516040015161283e906102a08401356142de565b6102e08201805160400191909152805160016060909101525161024082015261298f565b610120810151602001511561293757610120810151516102a083013510156128e0576101208101515161289b906102a0840135906142de565b6103008201805191909152610120820180516020908101518351909101528051604090810151835190910152516060908101518251909101525161024082015261298f565b610120810151516128f6906102a08401356142de565b61032082018051919091528051600060209091015261012082018051604090810151835190910152516060908101518251909101525161024082015261298f565b6101208101515161294e906102a0840135906142a7565b610340820180519190915261012082018051602090810151835190910152805160409081015183519091015251606090810151825190910152516102408201525b6102e082013515612a2b576129ae6104608301356102e08401356142a7565b610380820152608082013561016083013511156129ff576103808101516129de60808401356101608501356142de565b6129e891906142a7565b6103a0820180519190915251610360820152612a7d565b610380810151612a14906101608401356142a7565b6103c0820180519190915251610360820152612a7d565b60808201356101608301351115612a6657612a4f60808301356101608401356142de565b6103e0820180519190915251610360820152612a7d565b610400810180516101608401359052516103608201525b7fc06f3d6233c7b1f845c0c779ebae8a579238ffcb248a38316a0d0c7c746d685382604051612aac9190614010565b60405180910390a1612abc613650565b612ac960208401846138e1565b81516001600160a01b0390911690528051602080850135910152612af360608401604085016138e1565b81516001600160a01b03909116604091820152815160608086013591810191909152825160808087013591810191909152835160a08088013591810191909152845160c0808901359101526102408601805185015160208088018051929092528251860151825182015282515182519097019690965290519094015184519093019290925261036085015151835190910152905161018085013591015261103460016101a08501356142a7565b604051612bdc90612bb8906009908490602001614216565b6040516020818303038152906040528051906020012060001c600054146023612f9d565b60008055612bec34156020612f9d565b612c0e33612bfd60208401846138e1565b6001600160a01b0316146021612f9d565b60408051610140830135602082015261016083013591810191909152612c569060600160408051601f1981840301815291905280516020909101206080830135146022612f9d565b7f3d55f848098ca7b939dca12f26d8b2412fa0fac57cdd034a8460a91b49d4d39181604051612c859190613f40565b60405180910390a1612cf560405180610140016040528060006001600160a01b031681526020016000815260200160006001600160a01b03168152602001600081526020016000815260200160008152602001600081526020016000815260200160008152602001600081525090565b612d0260208301836138e1565b6001600160a01b0316815260208083013590820152612d2760608301604084016138e1565b6001600160a01b03166040808301919091526060808401359083015260a080840135608084015260c0808501359184019190915260e08085013591840191909152610100808501359184019190915261016084013590830152436101208301525161048b90600a90839060200161422b565b604051612dd590612db1906001908490602001614240565b6040516020818303038152906040528051906020012060001c60005414600e612f9d565b60008055612df5612deb600a60408401356142a7565b431015600f612f9d565b612e013415600c612f9d565b612e2333612e1260208401846138e1565b6001600160a01b031614600d612f9d565b612e3060208201826138e1565b6040516001600160a01b039190911690602083013580156108fc02916000818181858888f19350505050158015612e6b573d6000803e3d6000fd5b507fc3d6ba703f6ce931b1dd0e05e983d8be7c8ccc7f15219d844425151d856230138160405161118c91906140a8565b604051612ed790612eb390600390849060200161427e565b6040516020818303038152906040528051906020012060001c600054146016612f9d565b60008055612ef7612eed600560808401356142a7565b4310156017612f9d565b612f0334156014612f9d565b612f2533612f1460208401846138e1565b6001600160a01b0316146015612f9d565b612f3260208201826138e1565b6040516001600160a01b039190911690606083013580156108fc02916000818181858888f19350505050158015612f6d573d6000803e3d6000fd5b507f7206b35298c8dc27a88d0202316b60bd31564f2bc4c02d3f4ff85b4f5102e0e68160405161118c9190613eb0565b816113525760405163100960cb60e01b81526004810182905260240160405180910390fd5b60008160200151610100015111612fda576000612ff8565b805160c0015160208201516080015114612ff5576001612ff8565b60005b613003576000613024565b80600001516080015181602001516080015114613021576001613024565b60005b1561325557602081015160a081015160e082015160c0909201519091613049916142f5565b1415613152576130576137c1565b8151516001600160a01b039081168252825160209081015181840152835160409081015190921682840152835160609081015181850152845160809081015181860152855160a09081015181870152865160c0908101518188015284880180515160e0808a01919091528151870151610100808b01919091528251890151610120808c0191909152835190970151610140808c01919091528351909601516101608b01528251909401516101808a01528151909201516101a08901528051909101516101c08801528051909101516101e087015280519092015161020086015290510151610220840152905161048b91601091849101614115565b61315a6137c1565b8151516001600160a01b039081168252825160209081015181840152835160409081015190921682840152835160609081015181850152845160809081015181860152855160a09081015181870152865160c0908101518188015284880180515160e0808a01919091528151870151610100808b01919091528251890151610120808c0191909152835190970151610140808c01919091528351909601516101608b01528251909401516101808a01528151909201516101a08901528051909101516101c08801528051909101516101e087015280519092015161020086015290510151610220840152905161048b91601291849101614115565b6132cb60405180604001604052806137b460405180610140016040528060006001600160a01b031681526020016000815260200160006001600160a01b03168152602001600081526020016000815260200160008152602001600081526020016000815260200160008152602001600081525090565b81515181516001600160a01b039182169052825160209081015183518201528351604090810151845193169281019290925283516060908101518451820152845160a090810151855160800152828601805151865190920191909152805190920151845160c00152815190920151835160e001528051909101518251610100015251610140015181516101200152611352816133c560405180610140016040528060006001600160a01b031681526020016000815260200160006001600160a01b03168152602001600081526020016000815260200160008152602001600081526020016000815260200160008152602001600081525090565b8151516001600160a01b039081168252825160209081015181840152835160409081015190921682840152835160609081015190840152835160809081015190840152835160a09081015190840152835160c09081015190840152835160e090810151908401528351610100908101519084015283516101209081015190840152905161048b9160099184910161422b565b60405180610420016040528061346b61385e565b815260200161347861385e565b815260200161348561385e565b815260200161349261385e565b815260200161349f61385e565b81526020016134ac61385e565b81526020016134b961385e565b81526020016134c661385e565b81526020016134d361385e565b81526020016134e061385e565b81526020016134ed61385e565b81526020016134fa61385e565b815260200161350761385e565b815260200161351461385e565b815260200161352161385e565b815260200161352e61385e565b815260200161353b61385e565b815260200161354861385e565b815260200161355561385e565b815260200161356261385e565b815260200161356f61385e565b815260200161357c61385e565b815260200161358961385e565b815260200161359661385e565b81526020016135a361385e565b81526020016135b061385e565b81526020016135bd61385e565b81526020016135d86040518060200160405280600081525090565b8152602001600081526020016135fa6040518060200160405280600081525090565b81526020016136156040518060200160405280600081525090565b81526020016136306040518060200160405280600081525090565b815260200161364b6040518060200160405280600081525090565b905290565b60405180604001604052806136ad6040518060e0016040528060006001600160a01b031681526020016000815260200160006001600160a01b03168152602001600081526020016000815260200160008152602001600081525090565b815260200161364b60405180610160016040528060008152602001600081526020016000815260200160008152602001600081526020016000815260200160008152602001600081526020016000815260200160008152602001600081525090565b604051806101e0016040528061372361385e565b81526020016000815260200160008152602001600081526020016000815260200161374c61385e565b815260200161375961385e565b815260200161376661385e565b815260200161377361385e565b815260200161378061385e565b815260200161378d61385e565b815260200161379a61385e565b81526020016137a761385e565b81526020016137b461385e565b8152600060209091015290565b60405180610240016040528060006001600160a01b031681526020016000815260200160006001600160a01b031681526020016000815260200160008152602001600081526020016000815260200160008152602001600081526020016000815260200160008152602001600081526020016000815260200160008152602001600081526020016000815260200160008152602001600081525090565b6040518060800160405280600081526020016000815260200160008152602001600081525090565b80356001600160a01b038116811461389d57600080fd5b919050565b600061018082840312156138b557600080fd5b50919050565b60006103e082840312156138b557600080fd5b60006104a082840312156138b557600080fd5b6000602082840312156138f357600080fd5b6138fc82613886565b9392505050565b600060c082840312156138b557600080fd5b600061012082840312156138b557600080fd5b600061010082840312156138b557600080fd5b6000610180828403121561394e57600080fd5b6138fc83836138a2565b60006103e0828403121561396b57600080fd5b6138fc83836138bb565b60006104a0828403121561398857600080fd5b6138fc83836138ce565b6000604082840312156138b557600080fd5b6000608082840312156138b557600080fd5b600060e082840312156138b557600080fd5b6139e2826139d583613886565b6001600160a01b03169052565b602081013560208301526139f860408201613886565b6001600160a01b03166040830152606081810135908301526080808201359083015260a0808201359083015260c0808201359083015260e08082013590830152610100808201359083015261012080820135908301526101408082013590830152610160808201359083015261018080820135908301526101a080820135908301526101c080820135908301526101e08082013590830152610200808201359083015261022090810135910152565b613ab4826139d583613886565b60208101356020830152613aca60408201613886565b6001600160a01b03166040830152606081810135908301526080808201359083015260a0808201359083015260c0808201359083015260e08082013590830152610100808201359083015261012090810135910152565b80516001600160a01b03168252602081015160208301526040810151613b5260408401826001600160a01b03169052565b50606081015160608301526080810151608083015260a081015160a083015260c081015160c083015260e081015160e08301526101008082015181840152506101208082015181840152505050565b6001600160a01b03613bb282613886565b16825260208181013590830152604090810135910152565b613bd7826139d583613886565b60208101356020830152613bed60408201613886565b6001600160a01b03166040830152606081810135908301526080808201359083015260a0808201359083015260c0808201359083015260e08082013590830152610100808201359083015261012080820135908301526101408082013590830152610160808201359083015261018080820135908301526101a080820135908301526101c080820135908301526101e08082013590830152610200808201359083015261022080820135908301526102408082013590830152610260808201359083015261028080820135908301526102a080820135908301526102c080820135908301526102e0808201359083015261030090810135910152565b80516001600160a01b03168252602081015160208301526040810151613d1a60408401826001600160a01b03169052565b50606081810151908301526080808201519083015260a0808201519083015260c0808201519083015260e08082015190830152610100808201519083015261012080820151908301526101408082015190830152610160808201519083015261018080820151908301526101a080820151908301526101c080820151908301526101e08082015190830152610200808201519083015261022080820151908301526102408082015190830152610260808201519083015261028080820151908301526102a080820151908301526102c080820151908301526102e0808201519083015261030090810151910152565b6001600160a01b0380613e1b83613886565b1683526020820135602084015280613e3560408401613886565b1660408401525060608181013590830152608090810135910152565b6001600160a01b0380613e6383613886565b1683526020820135602084015280613e7d60408401613886565b16604084015250606081013560608301526080810135608083015260a081013560a083015260c081013560c08301525050565b60c08101613ebe8284613e09565b60a0830135801515808214613ed257600080fd5b8060a0850152505092915050565b6101208101613eef8284613e51565b613f0960e0830160e0850180358252602090810135910152565b92915050565b6101008101613f1e8284613e51565b60e0830135801515808214613f3257600080fd5b8060e0850152505092915050565b6101808101613f4f8284613aa7565b610140838101358382015261016080850135908401525b5092915050565b6103e08101613f7c82846139c8565b6102408381013583820152610260808501359084015261028080850135908401526102a080850135908401526102c080850135908401526102e08085013590840152610300808501359084015261032080850135908401526103408085013590840152610360808501359084015261038080850135908401526103a080850135908401526103c08085013590840152613f66565b6104a0810161401f8284613bca565b61032083810135838201526103408085013590840152610360808501359084015261038080850135908401526103a080850135908401526103c080850135908401526103e0808501359084015261040080850135908401526104208085013590840152610440808501359084015261046080850135908401526104808085013590840152613f66565b608081016140b68284613ba1565b60608301358015158082146140ca57600080fd5b806060850152505092915050565b60e081016140e68284613e09565b613f0960a0830160a0850180358252602090810135910152565b82815261026081016138fc60208301846139c8565b82815281516001600160a01b03166020820152610260810160208301516040830152604083015161415160608401826001600160a01b03169052565b50606083015160808381019190915283015160a08084019190915283015160c08084019190915283015160e08084019190915283015161010080840191909152830151610120808401919091528301516101408084019190915283015161016080840191909152830151610180808401919091528301516101a0808401919091528301516101c0808401919091528301516101e08084019190915283015161020080840191909152830151610220808401919091529092015161024090910152919050565b82815261016081016138fc6020830184613aa7565b82815261016081016138fc6020830184613b21565b828152608081016138fc6020830184613ba1565b82815261034081016138fc6020830184613bca565b82815261034081016138fc6020830184613ce9565b82815260c081016138fc6020830184613e09565b82815261010081016138fc6020830184613e51565b600082198211156142ba576142ba614317565b500190565b60008160001904831182151516156142d9576142d9614317565b500290565b6000828210156142f0576142f0614317565b500390565b60008261431257634e487b7160e01b600052601260045260246000fd5b500690565b634e487b7160e01b600052601160045260246000fdfea2646970667358221220fa45df99bdea458f20e9cc606239fcb83d2b0b98d95af1106f3cbb85edd8d8bf64736f6c63430008050033`,
  BytecodeLen: 17381,
  Which: `oD`,
  deployMode: `DM_constructor`,
  views: {
    }
  };

export const _Connectors = {
  ALGO: _ALGO,
  ETH: _ETH
  };

