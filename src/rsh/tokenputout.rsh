'reach 0.1';
// 'use strict';

// import { digestEq } from "@reach-sh/stdlib/dist/types/shared_backend";

// const optionConfig = {
//     // //决定合约是单独部署(constructor,默认)，首个发布（'firstMsg'）的一部分部署
//     // deployMode:'constructor',
//     // //决定算术运算是否自动引入(.UIntmax),默认false,开发建议关闭，部署打开。消耗性能
//     // verifyArithmetic:false,
//     // //决定是对每个连接器进行验证，还是对常规连接器进行一次验证。默认false,选择true,消耗性能。.UIntmax将被实例化为具体数字。
//     // verifyPerConnector:false,
//     // //连接器元组，应用程序需要被编译为该元组内的连接器，默认[ETH, ALGO]
//     // connectors:[ETH, ALGO]
// }

/**
 *Participant(participantName, participantInteractInterface)
 *participantName 是一个字符串，表示生成的后端代码中参与者函数的名称。每个 participantName 都必须是唯一的。
 *participantInteractInterface 是参与者交互接口，它是一个对象，其字段均是函数或值的类型，且必须由前端提供给后端，以便与参与者交互
 *本例子定义两个身份，一个发放空投代币者身份，一个领取空投代币参与者身份
 */
// const participantArr = [Participant('Airdropper', { amt: UInt, pass: UInt }), Participant('Recipient', { getPass: Function([], UInt) })],

/**
 * 执行步骤处理的函数
 * 参数数量与顺序与participantDefinitions保持一致
 */
// const programFunHandle = (Airdropper, Recipient) => {
//     Airdropper.only(() => {
//         const _pass = interact.pass;
//         const [amt, passDigest] = declassify([interact.amt, digest(_pass)])
//     })
//     Airdropper.publish(passDigest, amt).pay(amt)
//     commit()
//     unknowable(Recipient, Airdropper(_pass))
//     Recipient.only(() => {
//         const pass = declassify(interact.getPass());
//         assume(passDigest == digest(pass))
//     })
//     Recipient.publish(pass)
//     require(passDigest == digest(pass))
//     transfer(amt).to(Recipient)
//     commit()
//     exit()
// }

// export const main = Reach.App(
//     optionConfig,
//     participantArr,
//     programFunHandle);

//UInt可理解成一个hash结构{_hex: "0xeaeb63d4226277f1d1954b5c0ad2e7443bebf8cd090c257d2faa598a2d9fd6f3",isBigNumber: true}
    
export const main = Reach.App(
    {},
    [Participant('Airdropper', { amt: UInt, pass: UInt }), Participant('Recipient', { getPass: Fun([], UInt) })],
    (Airdropper, Recipient) => {
        Airdropper.only(() => {
            const _pass = interact.pass;
            const [amt, passDigest] = declassify([interact.amt, digest(_pass)])
        })
        Airdropper.publish(passDigest, amt).pay(amt)
        commit()
        unknowable(Recipient, Airdropper(_pass))
        Recipient.only(() => {
            const pass = declassify(interact.getPass());
            assume(passDigest == digest(pass))
        })
        Recipient.publish(pass)
        require(passDigest == digest(pass))
        transfer(amt).to(Recipient)
        commit()
        exit()
    });