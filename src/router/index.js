
import Home from '../view/home/home'
import Roadmap from '../view/roadmap/roadmap'
import Solution from '../view/solution/solution';
import Document from '../view/document/document';
import Activity from '../view/activity/activity';

export default [
    {
        //页面名字（可用于导航栏）
        routerName: 'Home',
        //路由路径
        path: '/home',
        //遍历值
        key: '1',
        //渲染组件
        component:Home,
        //是否展示页脚
        showFooter:false
    },
    {
        routerName: 'Roadmap',
        path: '/roadmap',
        key: '2',
        component:Roadmap,
        showFooter:true
    },
    // {
    //     routerName: 'Solution',
    //     path: '/solution',
    //     key: '3',
    //     component:Solution,
    //     showFooter:true
    // },
    // {
    //     routerName: 'Document',
    //     path: '/document',
    //     key: '4',
    //     component:Document,
    //     showFooter:true
    // },
    // {
    //     routerName: 'Activity',
    //     path: '/activity',
    //     key: '5',
    //     component:Activity,
    //     showFooter:true
    // },
]