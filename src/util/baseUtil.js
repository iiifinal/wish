import { loadStdlib } from '@reach-sh/stdlib';

const Reach = loadStdlib(process.env);


/**
 * 获取区块链地址（可中间省略号）
 * @param {*} acc 一个加密钱包类型的对象
 * @param {*} open 布尔值，是否缩写地址
 * @returns 
 */
export function GetFormatAddress(acc,open){
 if(Object.keys(acc).length){
  let str=Reach.formatAddress(acc)
  if(open){
      return str.substring(0,4)+'...'+str.substring(str.length-4,str.length) 
  }
  return str
 }else{
   return ''
 }
 
}
/**
 * 随机打乱一组数组（可用于随机洗牌）
 * @param {*} arr  
 * @returns 
 */
export function RandomArrayHandle(arr){
     let copyArr=JSON.parse(JSON.stringify(arr))
     return copyArr.sort((a,b)=>Math.random() - 0.5)
}

/**
 * 随机生成一个开局随机数
 */
 export function RandomStartRound(){
   //  Reach.formatCurrency(num)：31013984424972677190264020376364930683671365284862893157840.984195377454376207
   return Reach.formatCurrency(Reach.hasRandom.random()).split('.')[0].split('')[0]%2
}



export function Ascii2native(asciicode) {
      asciicode = asciicode.split("\\u");
      var nativeValue = asciicode[0];
      for (var i = 1; i < asciicode.length; i++) {
      var code = asciicode[i];
      nativeValue += String.fromCharCode(parseInt("0x" + code.substring(0, 4)));
      if (code.length > 4) {
      nativeValue += code.substring(4, code.length);
      }
      }
      return nativeValue;
  }
  