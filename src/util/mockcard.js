
//每张卡的数据格式说明
//注：如果id为0 ，则说明该项的没有效果或者表示通用型
export const handcardArr=[
    {   
        //种类(分类role1:角色卡,limit2:限制卡，site3:场地卡，energy4：效果卡,equipment5:装备卡)
        type:'role',
        //卡的编号uuid，是有规律的。(种类-世界-等级-卡本身id-效果系列id)
        uuid:'1-1-1-1-1',
        //世界序列id，如果为0，则表示是通用卡，不区分世界
        word_id:'1',
        //种类id(角色？限制卡？场地卡？效果卡？装备卡？)
        type_id:'1',
        //层级id（卡的珍贵层度）
        level_id:'1',
        //卡本身编号id
        id:'1',
        //效果说明id
        effect_id:'1',
        //卡的绘画链接
        url:'',
        //nft的token
        token_id:'',
    },
    {   
        type:'limit',
        uuid:'2-0-1-1-1',
        word_id:'0',
        type_id:'2',
        level_id:'1',
        id:'1',
        effect_id:'1',
        url:'',
        token_id:'',
    },

    {   
        type:'site',
        uuid:'3-1-1-1-1',
        word_id:'1',
        type_id:'3',
        level_id:'1',
        id:'1',
        effect_id:'1',
        url:'',
        token_id:'',
    },
    
    {   
        type:'energy',
        uuid:'4-0-1-1-1',
        word_id:'0',
        type_id:'4',
        level_id:'1',
        id:'1',
        effect_id:'1',
        url:'',
        token_id:'',
    },
    {   
        type:'equipment',
        uuid:'5-1-1-1-1',
        word_id:'1',
        type_id:'5',
        level_id:'1',
        id:'1',
        effect_id:'1',
        url:"",
        token_id:'',
    }
]


