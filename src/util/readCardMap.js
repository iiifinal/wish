//种类(分类role1:角色卡,limit2:限制卡，site3:场地卡，energy4：效果卡,equipment5:装备卡)
export function ReadTypeCardInfo(type, uuid) {
    switch (type) {
        case 'role':
            return MapRoleCardInfo(uuid)
        case 'energy':
            return MapEffectCardInfo(uuid)
        case 'limit':
            return MapLimitCardInfo(uuid)
        case 'equipment':
            return MapEquipmentCardInfo(uuid)
        default:
            return 0
    }
}



//读取角色卡能力的计算
export function MapRoleCardInfo(uuid) {
    switch (uuid) {
        case '1-0-1-1-0':
            return 1000
        case '1-0-2-2-0':
            return 1200
        case '1-0-3-3-0':
            return 1500
        case '1-0-4-4-0':
            return 2000
        case '1-0-5-5-0':
            return 2500
        default:
            return 0;
    }
}

//读取效果卡的计算（暂时设定1是增益，2是减益）
export function MapEffectCardInfo(uuid) {
    switch (uuid) {
        case '4-0-1-1-1':
            return 100
        case '4-0-2-2-1':
            return 300
        case '4-0-3-3-1':
            return 500
        case '4-0-3-3-2':
            return -500
        case '4-0-4-4-1':
            return 800
        case '4-0-4-4-2':
            return -800
        case '4-0-5-5-1':
            return 1000
        case '4-0-5-5-2':
            return -1000
        default:
            return 0;
    }
}

//读取装备卡的计算（暂时设定1是增益，2是减益）
export function MapEquipmentCardInfo(uuid) {
    switch (uuid) {
        case '5-0-1-1-1':
            return 300
        case '5-0-2-2-1':
            return 500
        case '5-0-2-2-2':
            return -500
        case '5-0-3-3-1':
            return 800
        case '5-0-3-3-2':
            return -800
        default:
            return 0;
    }
}

//读取限制卡的计算（暂时设定1是增益，2是减益）
export function MapLimitCardInfo(uuid) {
    switch (uuid) {
        case '2-0-1-1-1':
            return 5
        case '2-0-1-1-2':
            return 4
        case '2-0-2-2-1':
            return 3
        case '2-0-2-2-2':
            return 2
        case '2-0-2-3-1':
            return 1
        default:
            return 0;
    }
}